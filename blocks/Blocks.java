package blocks;

import com.eclipsesource.json.JsonObject;

import db.DBConnection;
import db.Rows;
import db.Select;
import db.column.FileColumn;
import ui.RichText;
import ui.Table;
import web.HTMLWriter;
import web.JS;

public class Blocks {
	private final boolean		m_editable;
	private Rows				m_rows;
	private Table				m_table;
	private final HTMLWriter	m_w;

	//--------------------------------------------------------------------------

	public
	Blocks(boolean editable, HTMLWriter w) {
		m_editable = editable;
		m_w = w;
	}

	//--------------------------------------------------------------------------
	// only need to call if lookup() is called

	public final void
	close() {
		if (m_rows != null)
			m_rows.close();
	}

	//--------------------------------------------------------------------------

	private String
	getString(String template) {
		int i = template.indexOf("${");
		while (i != -1) {
			int j = template.indexOf('}', i + 2);
			String column = template.substring(i + 2, j);
			String value = m_rows.getString(column);
			if (value != null)
				template = template.substring(0, i) + value + template.substring(j + 1);
			else
				template = template.substring(0, i) + template.substring(j + 1);
			i = template.indexOf("${", i);
		}
		return template;
	}

	//--------------------------------------------------------------------------

	public final Blocks
	lookup(Select select, DBConnection db) {
		m_rows = new Rows(select, db);
		m_rows.next();
		return this;
	}

	//--------------------------------------------------------------------------
	// return true if handled

	public final boolean
	write(JsonObject block) {
		if (!m_editable && block.getBoolean("edit_only", false))
			return true;
		String type = block.getString("type", null);
		if (!"text".equals(type) && m_table != null) {
			m_table.close();
			m_table = null;
		}
		switch(type) {
		case "file":
			writeFile(block);
			return true;
		case "html":
			writeHTML(block, m_rows.getString(block.getString("column", null)));
			return true;
		case "icon":
			writeIcon(block);
			return true;
		case "text":
			writeText(block, m_rows.getString(block.getString("column", null)));
			return true;
		}
		return false;
	}

	//--------------------------------------------------------------------------

	private void
	writeFile(JsonObject block) {
		String filename = m_rows.getString(block.getString("column", null));
		if (filename != null)
			FileColumn.writeLink(block.getString("text", filename), getString(block.getString("url", null)), false, null, 0, m_w);
	}

	//--------------------------------------------------------------------------

	private void
	writeHTML(JsonObject block, String html) {
		if (!m_editable && html == null)
			return;
		String title = block.getString("title", null);
		if (title != null)
			m_w.addClass("text-muted").h4(title);
		m_w.write("<div class=\"mb-3\" style=\"max-width:1024px\">");
		if (m_editable) {
			String column = block.getString("column", null);
			new RichText()
				.setLabel(block.getString("label", null))
				.setOnSave("function(el){net.post(context+'/Views/" + block.getString("view", null) + "/update','db_key_value='+Dialog.top().options.id+'&" + column + "='+encodeURIComponent(el.innerHTML))}")
				.write(column, html, "div", false, null, m_w);
		} else
			m_w.write(html);
		m_w.write("</div>");
	}

	//--------------------------------------------------------------------------

	private void
	writeIcon(JsonObject block) {
		if (block.getBoolean("edit_only", false) && !m_editable)
			return;
		String icon = block.getString("icon", null);
		if (icon != null)
			m_w.ui.icon(icon).nbsp();
	}

	//--------------------------------------------------------------------------

	public final void
	writeImg(JsonObject block, String src) {
		if (block.getBoolean("edit_only", false) && !m_editable)
			return;
		String style = block.getString("style", null);
		if (style != null)
			m_w.addStyle(style);
		if (m_editable) {
			String on_click = "Img.edit_menu(this";
			String label = block.getString("label", null);
			if (label != null)
				on_click += "," + JS.string(label);
			on_click += ")";
			m_w.addStyle("cursor:pointer;min-height:40px;min-width:40px")
				.setAttribute("onclick", on_click)
				.setAttribute("onerror", "this.removeAttribute('src')")
				.setAttribute("onmouseleave", "this.style.outline=''")
				.setAttribute("onmouseenter", "this.style.outline='solid 1px'")
				.setAttribute("title", "click to edit");
		}
		m_w.img(src);
	}

	//--------------------------------------------------------------------------

	private void
	writeText(JsonObject block, String text) {
		String column = block.getString("column", null);
		if (!m_editable && "email".equals(column) && m_rows.hasColumn("hide_email_on_site") && m_rows.getBoolean("hide_email_on_site"))
			return;
		if (m_editable || text != null) {
			block.getBoolean("inline", false);
			if (m_editable) {
				if (m_table == null)
					m_table = m_w.ui.table().addClass("table").addClass("table-borderless");
				m_table.tr();
				m_w.addClass("ps-0 text-muted");
				m_table.td();
				String label = block.getString("label", null);
				m_w.write(label != null ? label : column);
				m_table.td();
				new RichText()
					.setOneAtATime(true)
					.setLabel(block.getString("label", null))
					.setOnSave("function(el){net.post(context+'/Views/" + block.getString("view", null) + "/update','db_key_value='+Dialog.top().options.id+'&" + column + "='+encodeURIComponent(el.innerHTML))}")
					.write(column, text, "div", false, null, m_w);
			} else {
				String icon = block.getString("icon", null);
				if (icon != null)
					m_w.ui.iconText(icon, text);
				else
					m_w.p(text);
			}
		}
	}
}
