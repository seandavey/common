package sitemenu;

import java.util.Collection;
import java.util.List;

import app.Modules;
import app.Request;
import db.NameValuePairs;
import db.Reorderable;
import db.SelectRenderer;
import db.View;
import db.ViewDef;
import db.ViewState;
import db.column.Column;
import db.object.DBObjects;
import pages.Page;

class MenuItemsViewDef extends ViewDef {
	public
	MenuItemsViewDef(Collection<Page> pages) {
		super("menu_items");
		setRecordName("Tab", true);
		setReorderable(new Reorderable().setOnUpdate("app.refresh_top_menu()"));
//		setShowColumnHeads(false);
		setShowNumRecords(false);
		setColumnNamesForm(new String[] { "people_id", "name" });
		setColumnNamesTable(new String[] { "name" });
		setColumn(new Column("people_id").setIsHidden(true).setDefaultToUserId());
		setColumn(new Column("name").setDisplayName("").setInputRenderer(new SelectRenderer(pages)/*MenuItemsInputRenderer(all_pages)*/));
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	afterList(View v, Request r) {
		r.w.ui.buttonOutlineOnClick("Reset to default menu", "net.post(context+'/SiteMenu/reset',null,function(){var p=_.c(this).parentNode;_.dom.empty(p);p.style.display='none';p.previousSibling.style.display='';app.refresh_top_menu()}.bind(this))");
	}

	//--------------------------------------------------------------------------

	@Override
	public View
	newView(Request r) {
		SiteMenu site_menu = (SiteMenu)Modules.get("SiteMenu");
		DBObjects<MenuItem> user_menu_items = site_menu.getUserPages(r);
		if (user_menu_items != null) {
			List<MenuItem> menu_items = user_menu_items.getObjects();
			if (menu_items.isEmpty()) {
				user_menu_items.copyObjects(site_menu.getMenu());
				menu_items = user_menu_items.getObjects();
				NameValuePairs nvp = new NameValuePairs();
				nvp.set("people_id", r.getUser().getId());
				for (int i=0; i<menu_items.size(); i++) {
					nvp.set("_order_", i + 1);
					nvp.set("name", menu_items.get(i).name);
					r.db.insert("menu_items", nvp);
				}
			}
		}
		ViewState.setBaseFilter("menu_items", "people_id=" + r.getUser().getId(), r);
		return super.newView(r);
	}
}
