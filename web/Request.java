package web;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.net.URLDecoder;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import app.Site;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.Part;

public class Request {
	public final HttpServletRequest		request;
	public final Response				response;
	public final HTMLWriter				w;

	private Map<String,Object>	m_data;
	private Map<String,String>	m_parameters;
	private final String[]		m_path_segments;

	//--------------------------------------------------------------------------

	public
	Request(HttpServletRequest request, String[] path_segments, HttpServletResponse response, Writer writer) {
		this.request = request;
		m_path_segments = path_segments != null ? path_segments : getPathSegments(request);
		if (response != null) {
			response.setHeader("Cache-Control","no-cache, no-store, must-revalidate");
			response.setHeader("Pragma","no-cache");
			response.setDateHeader("Expires", 0);
		}
		if (response != null) {
			this.response = new Response(response);
			w = writer != null ? new HTMLWriter(writer) : null;
		} else {
			this.response = null;
			w = null;
		}
	}

	//--------------------------------------------------------------------------

	public void
	close() {
		w.tagsCloseAll();
	}

	//--------------------------------------------------------------------------

//	public final void
//	deleteCookie(String name) {
//		Cookie cookie = new Cookie(name, null);
//		cookie.setMaxAge(0);
//		response.addCookie(cookie);
//	}

	//--------------------------------------------------------------------------

	public final boolean
	getBoolean(String name) {
		String value = getParameter(name);
		return "on".equals(value) || "true".equals(value);
	}

	//--------------------------------------------------------------------------

	public final String
	getCookie(String name) {
		name = name.replace(' ', '_');
		Cookie[] cookies = request.getCookies();
		if (cookies != null)
			for (Cookie cookie : cookies)
				if (name.equals(cookie.getName()))
					return cookie.getValue();
		return null;
	}

	//--------------------------------------------------------------------------

	public final Object
	getData(String name) {
		if (m_data == null)
			return null;
		return m_data.get(name);
	}

	//--------------------------------------------------------------------------
	/**
	 * @param name name of request parameter to return
	 * @param default_value value to return if parameter is not found in request
	 * @return double value of parameter. if value is of form name=value then nested value returned
	 */

	public final double
	getDouble(String name, double default_value) {
		String value = getParameter(name);

		if (value != null && value.length() > 0) {
			int equals = value.indexOf('=');
			if (equals != -1)
				if (value.charAt(equals + 1) == '\'')
					value = value.substring(equals + 2, value.length() - 1);
				else
					value = value.substring(equals + 1);
			try {
				return Double.parseDouble(value);
			} catch (NumberFormatException e) {
			}
		}

		return default_value;
	}

	//--------------------------------------------------------------------------

	public final <E extends Enum<E>> E
	getEnum(String name, Class<E> enum_class) {
		String value = getParameter(name);
		if (value != null)
			try {
				return Enum.valueOf(enum_class, value);
			} catch (Exception e) {
				throw new RuntimeException("invalid value for " + name);
			}
		return null;
	}

	//--------------------------------------------------------------------------
	/**
	 * @param name name of request parameter to return
	 * @param default_value value to return if parameter is not found in request
	 * @return float value of parameter. if value is of form name=value then nested value returned
	 */

//	public final float
//	getFloat(String name, float default_value) {
//		String value = getParameter(name);
//
//		if (value != null && value.length() > 0) {
//			int equals = value.indexOf('=');
//			if (equals != -1)
//				if (value.charAt(equals + 1) == '\'')
//					value = value.substring(equals + 2, value.length() - 1);
//				else
//					value = value.substring(equals + 1);
//			try {
//				return Float.parseFloat(value);
//			} catch (NumberFormatException e) {
//			}
//		}
//
//		return default_value;
//	}

	//--------------------------------------------------------------------------
	/**
	 * @param name name of request parameter to return
	 * @param default_value value to return if parameter is not found in request
	 * @return int value of parameter. if value is of form name=value then nested value returned
	 */

	public final int
	getInt(String name, int default_value) {
		String value = getParameter(name);

		if (value == null || value.length() <= 0)
			return default_value;

		int equals = value.indexOf('=');
		if (equals != -1)
			if (value.charAt(equals + 1) == '\'')
				value = value.substring(equals + 2, value.length() - 1);
			else
				value = value.substring(equals + 1);
		value = value.trim();
		try {
			return Integer.parseInt(value);
		} catch(NumberFormatException e) {
			log("getInt: invalid " + name + ": " + value, true);
			throw new RuntimeException("invalid value for " + name);
		}
	}

	//--------------------------------------------------------------------

	public static String[]
	getPathSegments(HttpServletRequest http_request) {
		String path;
		try {
			path = URLDecoder.decode(http_request.getServletPath(), "UTF-8");
			return path.substring(1).split("/");
		} catch (UnsupportedEncodingException e) {
		}
		return null;
	}

	//--------------------------------------------------------------------------

	public final String
	getParameter(String name) {
		String value = request.getParameter(name);
		if (value == null && m_parameters != null)
			value = m_parameters.get(name);
		if (value != null)
			value = value.trim();
		return value;
	}

	//--------------------------------------------------------------------------

	public final String[]
	getParameterValues(String name) {
		return request.getParameterValues(name);
	}

	//--------------------------------------------------------------------------

	public final Part
	getPart(String name) {
		try {
			return request.getPart(name);
		} catch (IOException | ServletException e) {
			log(e);
			throw new RuntimeException(e);
		}
	}

	//--------------------------------------------------------------------------

	public Collection<Part> getParts() {
		try {
			return request.getParts();
		} catch (IOException | ServletException e) {
			log(e);
			throw new RuntimeException(e);
		}
	}

	//--------------------------------------------------------------------------
	// negative indexes count from end

	public final String
	getPathSegment(int index) {
		if (index < 0)
			index = m_path_segments.length + index;
		if (index < 0 || index >= m_path_segments.length)
			return null;
		return m_path_segments[index];
	}

	//--------------------------------------------------------------------------

	public final <E extends Enum<E>> E
	getPathSegmentEnum(int index, Class<E> enum_class) {
		String value = getPathSegment(index);
		if (value == null)
			return null;
		try {
			return Enum.valueOf(enum_class, value);
		} catch (Exception e) {
			return null;
		}
	}

	//--------------------------------------------------------------------------

	public final int
	getPathSegmentInt(int index) {
		String path_segment = getPathSegment(index);
		if (path_segment == null)
			return 0;
		try {
			return Integer.parseInt(path_segment);
		} catch (NumberFormatException e) {
			log("getPathSegmentInt: " + path_segment + " is not an integer", true);
			throw e;
		}
	}

	//--------------------------------------------------------------------------

	public final Object
	getSessionAttribute(String name) {
		return request.getSession().getAttribute(name);
	}

	//--------------------------------------------------------------------------

	public final int
	getSessionInt(String name, int default_value) {
		Integer value = (Integer)request.getSession().getAttribute(name);
		if (value == null)
			return default_value;
		return value.intValue();
	}

	//--------------------------------------------------------------------------

	public final Object
	getState(Class<?> state_class, String name) {
		String class_name = state_class.getName();
		int last_dot = class_name.lastIndexOf('.');
		if (last_dot != -1)
			class_name = class_name.substring(last_dot + 1);
		return getSessionAttribute(class_name + ":" + name);
	}

	//--------------------------------------------------------------------------
	/**
	 * @param name name of request parameter to return
	 * @param default_value value to return if parameter is not found in request
	 * @return String value of parameter. if value is of form name=value then nested value returned
	 */

	public final String
	getString(String name, String default_value) {
		String value = getParameter(name);
		if (value != null && value.length() > 0) {
			int equals = value.indexOf('=');
			if (equals != -1)
				if (value.charAt(equals + 1) == '\'')
					value = value.substring(equals + 2, value.length() - 1);
				else
					value = value.substring(equals + 1);
			return value;
		}

		return default_value;
	}

	//--------------------------------------------------------------------------

	protected void
	log() {
		StringBuffer url = request.getRequestURL();
		String query_string = request.getQueryString();
		if (query_string != null)
			url.append('?').append(query_string);
		System.out.println(url.toString());
		System.out.println(LocalDateTime.now().toString() + " - " + Site.context);
	}

	//--------------------------------------------------------------------------

	public final void
	log(String mesesage, boolean print_stack_trace) {
		log();
		System.out.println(mesesage);
		if (print_stack_trace)
			Thread.dumpStack();
	}

	//--------------------------------------------------------------------------

	public final void
	log(Error e) {
		if (e != null) {
			log(e.toString(), false);
			e.printStackTrace(System.out);
		}
	}

	//--------------------------------------------------------------------------

	public final void
	log(Exception e) {
		if (e != null) {
			log(e.toString(), false);
			e.printStackTrace(System.out);
		}
	}

	//--------------------------------------------------------------------------

	public final void
	removeSessionAttribute(String name) {
		request.getSession().removeAttribute(name);
	}

	//--------------------------------------------------------------------------

//	public final Request
//	setCookie(String name, String value, boolean keep) {
//		Cookie cookie = new Cookie(name.replace(' ', '_'), value);
//		cookie.setMaxAge(keep ? 365*24*60*60*1000 : -1);
//		cookie.setPath(m_context.length() == 0 ? "/" : m_context);
//		response.addCookie(cookie);
//		return this;
//	}

	//--------------------------------------------------------------------------

	public final Request
	setData(String name, Object value) {
		if (m_data == null)
			m_data = new HashMap<>();
		m_data.put(name, value);
		return this;
	}

	//--------------------------------------------------------------------------

	public final Request
	setParameter(String name, String value) {
		if (m_parameters == null)
			m_parameters = new HashMap<>();
		m_parameters.put(name, value);
		return this;
	}

	//--------------------------------------------------------------------------

	public final Request
	setParameter(String name, int value) {
		return setParameter(name, Integer.toString(value));
	}

	//--------------------------------------------------------------------------

	public final Request
	setSessionAttribute(String name, Object value) {
		request.getSession().setAttribute(name, value);
		return this;
	}

	//--------------------------------------------------------------------------

	public final void
	setSessionFlag(String name, boolean value) {
		request.getSession().setAttribute(name, value);
	}

	//--------------------------------------------------------------------------

	public final Request
	setSessionInt(String name, int value) {
		request.getSession().setAttribute(name, value);
		return this;
	}

	//--------------------------------------------------------------------------

	public final boolean
	testSessionFlag(String name) {
		Boolean b = (Boolean)request.getSession().getAttribute(name);
		return b != null && b.booleanValue();
	}

	//--------------------------------------------------------------------------

	public static String
	URIDecode(String s) {
		return s.replaceAll("-slash-", "/");
	}

	//--------------------------------------------------------------------------

	public boolean
	userHasRole(String role) {
		if (role.equals("administrator"))
			return request.isUserInRole("administrator");
		if (role.equals("guest"))
			return request.isUserInRole("guest");
		return request.isUserInRole(role) || request.isUserInRole("admin") || request.isUserInRole("administrator");
	}

	//--------------------------------------------------------------------------

	public final boolean
	userHasRole(String[] roles) {
		for (String role : roles)
			if (userHasRole(role))
				return true;
		return false;
	}
}