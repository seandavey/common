package web;

import app.Site;

public class Head {
	private final String		m_context;
	private final HTMLWriter	m_w;

	//--------------------------------------------------------------------------

	public
	Head(String title, Request r) {
		m_context = Site.context;
		m_w = r.w;
		if (m_w != null) {
			m_w.write("<!DOCTYPE html>");
			m_w.addStyle("height:100.1%;overscroll-behavior-y:none").tagOpen("html"); // 100.1% is to force disappearing scroll bars
			m_w.write("<head><meta charset=\"utf-8\"><meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">");
			if (title != null)
				m_w.tag("title", title);
		}
	}

	//--------------------------------------------------------------------------

	public final void
	close() {
		m_w.write("</head>");
		m_w.addStyle("display:flex;flex-direction:column;height:100%;min-height:100%;width:100%").tagOpen("body");
		m_w.js("var context='" + m_context + "'");
	}

	//--------------------------------------------------------------------------

	public final Head
	favIcon() {
		m_w.write("<link rel=\"icon\" type=\"image/png\" href=\"").write(m_context).write("/favicon.png\" />")
			.write("<link rel=\"apple-touch-icon\" type=\"image/png\" href=\"").write(m_context).write("/apple_touch_120.png\" />")
			.write("<link rel=\"icon\" sizes=\"192x192\" href=\"").write(m_context).write("/android-icon.png\">");
		return this;
	}

	//--------------------------------------------------------------------------

	public final Head
	script(String url) {
		this.script(url, true);
		return this;
	}

	//--------------------------------------------------------------------------

	public final Head
	script(String url, boolean version) {
		m_w.write("<script src=\"");
		if (url.indexOf('/') == -1) {
			m_w.write(m_context).write("/js/");
			if (version)
				url = Site.site.getResources().get("js", url);
		}
		m_w.write(url)
			.write(".js\"></script>");
		return this;
	}

	//--------------------------------------------------------------------------

	public final Head
	style(String style) {
		m_w.style(style);
		return this;
	}

	//--------------------------------------------------------------------------

	public final Head
	styleSheet(String style_sheet) {
		m_w.styleSheet(style_sheet, true);
		return this;
	}

	//--------------------------------------------------------------------------

	public final Head
	styleSheet(String style_sheet, boolean version) {
		m_w.styleSheet(style_sheet, version);
		return this;
	}
}
