package web;

import java.io.File;
import java.io.FilenameFilter;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

public class Resources {
	private final String							m_base_path;
	private final boolean							m_dev_mode;
	private final Map<String,Map<String,String>>	m_maps = new HashMap<>();

	//--------------------------------------------------------------------------

	public
	Resources(String base_path, boolean dev_mode) {
		m_base_path = base_path;
		m_dev_mode = dev_mode;
	}

	//--------------------------------------------------------------------------

	private synchronized String
	add(String type, String name) {
		Map<String,String> map = getTypeMap(type);
		String new_name = map.get(name);
		if (new_name != null)
			return new_name;
		File[] files = getFiles(type, name);
		int index = -1;
		int max = 0;
		int max_index = -1;
		for (int i=0; i<files.length; i++) {
			String filename = files[i].getName();
			int dot = filename.lastIndexOf('.');
			if (dot == name.length())
				index = i;
			else {
				int num = Integer.parseInt(filename.substring(name.length(), dot));
				if (num > max) {
					max = num;
					max_index = i;
				}
			}
		}
		if (index != -1) {
			new_name = name + (max + 1);
			for (int i=0; i<files.length; i++)
				if (i != index)
					files[i].delete();
				else
					files[i].renameTo(Paths.get(m_base_path, type, new_name + "." + type).toFile());
		} else {
			new_name = name + max;
			if (max_index != -1)
				for (int i=0; i<files.length; i++)
					if (i != max_index)
						files[i].delete();
		}
		map.put(name, new_name);
		return new_name;
	}

	//--------------------------------------------------------------------------

	public final String
	get(String type, String name) {
		if (m_dev_mode)
			return name;
		String new_name = getTypeMap(type).get(name);
		if (new_name != null)
			return new_name;
		return add(type, name);
	}

	//--------------------------------------------------------------------------

	private File[]
	getFiles(String type, String name) {
		File[] files = Paths.get(m_base_path, type).toFile().listFiles(new FilenameFilter() {
			@Override
			public boolean
			accept(File dir, String filename) {
				return filename.startsWith(name) && filename.endsWith(type);
			}
		});
		if (files == null) {
			System.out.println("no resources starting with " + name + " found");
			return null;
		}
		return files;
	}

	//--------------------------------------------------------------------------

	private Map<String,String>
	getTypeMap(String type) {
		Map<String,String> map = m_maps.get(type);
		if (map != null)
			return map;
		map = new HashMap<>();
		m_maps.put(type, map);
		return map;
	}
}
