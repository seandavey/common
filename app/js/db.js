import { Dialog } from './dialog'
export const db = {
	delete(el, text, view_def, params, dialog, reload_on_delete) {
		let view = app.get('view:' + view_def)
		let record_name = params.startsWith('id=') ? _.db.record_name(view_def, params.substring(3)) : view.record_name.toLowerCase()
		let q = text + ' this ' + record_name + '?'
		Dialog.confirm(view.delete_text ? q : null, view.delete_text ? view.delete_text : q, text, function() {
			if (view.many_many_table)
				params += '&table=' + view.many_many_table
			net.delete(context + '/Views/' + view_def + '?' + params, function() {
				if (dialog) {
					var d = Dialog.top()
					if (d) {
						if (d.options.owner)
							net.replace(_.$(d.options.owner))
						d.close();
					}
				} else {
					if (!reload_on_delete && view.one_name && _.form.replace_row(view.one_name, view_def))
						return
					net.replace(_.c(el), null, {top:window.scrollY})
				}
			})
		})
	},
	record_name(view_def, id) {
		let f = app.get(view_def + ':record name')
		if (f)
			return f(id)
		return app.get('view:' + view_def).record_name.toLowerCase()
	},
	relationship_add(el, view_def, one_name, hide_ok, owner) {
		let form = el.closest('form')
		let f = function() {
			let one_view = app.get('view:' + one_name)
			let one_id = one_view.id
			if (form)
				form.dataset.id = one_id
			let rows = _.c(el).querySelectorAll('[data-one_id]')
			for (let i=0; i<rows.length; i++)
				rows[i].dataset.one_id = one_id
			let view = app.get('view:' + view_def)
			let url = context + '/Views/' + view.one_name + '?db_mode=ADD_FORM&db_relationship=' + view_def + '&one_id=' + one_id
			new Dialog({button:el, cancel:true, component:true, ok:{hide:hide_ok, text:view.add_text},
				on_complete: function(){ _.form.replace_row(view.one_name, view_def) },
				owner:owner, title:view.add_title?view.add_title:view.add_text+' '+view.record_name, url:url})
		}
		if (form)
			_.form.autosave(form, f)
		else
			f()
	}
}