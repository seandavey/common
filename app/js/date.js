export const date = {
	month_date_year(d) {
		return d.toLocaleString('default', { month: 'long' }) + ' ' + d.getDate() + ', ' + d.getFullYear()
	},
	month_year(d) {
		return d.toLocaleString('default', { month: 'long' }) + ' ' + d.getFullYear()
	},
	offset_date(d, n) {
		d.setDate(d.getDate() + n)
		return d
	},
	parse(s) {
		let a = s.split('-')
		return new Date(a[0], a[1] - 1, a[2])
	},
	to_string(d) {
		return d.toISOString().split('T')[0]
	}
}
