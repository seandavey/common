export const comments = {
	dropdown(e,t,id,ce,cd) {
		let menu = new Dropdown()
		if (ce)
			menu.add('Edit Comment', function() {
				app.hide_popups()
				new Dialog({url:context+'/'+t+'/'+id+'/edit', title:'Edit Comment', dialog_class:'news_item_dialog', e:e, cancel:'Cancel'})
			})
		if (cd)
			menu.add('Delete Comment', function() {
				app.hide_popups()
				Dialog.confirm(null, 'Delete this comment?', _.text.delete, function() {
					net.post(context+'/'+t+'/'+id+'/delete', null, function(){e.closest('div.comment').remove()})
				})
			})
		menu.show(e)
	},
	focus(el) {
		let td = el.parentNode.parentNode
		if (td.lastChild.tagName === 'BUTTON')
			return
		let b = $button({classes:['btn', 'btn-primary', 'btn-sm'], events:{mousedown:function(event){
			if (!el.value) { 
				event.stopPropagation()
				event.preventDefault()
			} else
				_.comments.post(el)
		}}, styles:{float:'right', marginTop:8}}, 'Post')
		if (td.previousSibling)
			td.previousSibling.style['vertical-align'] = 'top'
		td.appendChild(b)
		let d = td.closest('div').parentNode
		d.scrollTop = d.scrollHeight
		el.addEventListener('blur', function(){b.remove()}, {once:true})
	},
	post(el) {
		_.ui.ok_cancel_remove()
		if (el.innerHTML)
			net.post(context + '/' + el.dataset.table + '_comments/add','item_id=' + el.dataset.item + '&comment=' + encodeURIComponent(el.innerHTML),
				function(){
					net.replace(el.closest('.component'),null,{ignore:true})
				}
			)
	}
}
