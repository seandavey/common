export const file_column = {
	adjust_title(cb) {
		let t = cb.form.title
		if (cb.checked) {
			t.parentNode.append(_.ui.alert('info', "The titles for the unzipped files will be set to the files' names."))
			_.dom.hide(t)
		} else {
			t.nextElementSibling.remove()
			_.dom.show(t)
		}		
	},
	changed(el) {
		let f = el.files
		let has_zip = false
		for (let i=0; i<f.length; i++)
			if (f[i].name.endsWith(".zip")) {
				has_zip = true
				break
			}
		if (has_zip) {
			if (!el.nextSibling) {
				let cb = _.ui.checkbox('db_' + el.name + '_unzip', 'unzip the zip file and add multiple documents?', {checked: true, events:{change:function(e){_.file_column.adjust_title(e.target)}}, parent: el.parentNode})
				_.file_column.adjust_title(cb.querySelector('input'))
			}
		} else
			el.nextSibling?.remove()
	},
	replace_clicked(a, name, required) {
		let p = a.parentNode
		_.dom.empty(p)
		let properties = {name:name, parent:p, type:'file'}
		if (required) {
			properties['required'] = 'required'
			properties['data-required-msg'] = 'Please select a file to upload'
		}
		$input(properties)
		$input({type:'hidden', name:name+'_delete', parent:p, value:true})
	}
}
export const form = {
	adjust_meridian(s) {
		if (s.value == 'PM')
			_.$('#end_time3').selectedIndex = 1
	},
	autosave(f, on_complete) {
		if (f.dataset.mode == 'READ_ONLY_FORM') {
			on_complete()
			return
		}
		let c = _.c(f)
		if (c && f.dataset.mode == 'ADD_FORM') {
			f.dataset.mode == 'EDIT_FORM'
			c.dataset.url = c.dataset.url.replace('db_mode=ADD_FORM','db_mode=EDIT_FORM')
			Dialog.top().was_add = true
		}
		if (!f.db_submit_id.value)
			f.db_submit_id.value = _.c(f).id
		_.form.submit(f, {on_complete:on_complete, validate:true})
	},
	changed(f) {
		if (!f.form_data)
			return true
		let e = new FormData(f).entries()
		for (let [key, value] of e)
			if (f.form_data.get(key) !== value)
				return true
		return false
	},
	fill_choice(input, ul) {
		let l = input.value.length
		if (l == 0)
			return
		let lc = input.value.toLowerCase()
		for (let i=0; i<ul.children.length; i++) {
			let v = ul.children[i].innerText
			if (v.substring(0,l).toLowerCase() === lc) {
				if (v.length > l) {
					input.value = v
					if (input.setSelectionRange) {
						input.focus()
						input.setSelectionRange(l, v.length)
					} else if (input.createTextRange) {
						let range = input.createTextRange()
						range.collapse(true)
						range.moveEnd('character', v.length)
						range.moveStart('character', l)
						range.select()
					}
				}
				break
			}
		}
	},
	new_date_input(i, on_change, check_not_same, short) {
		i = _.$(i)
		var config = { altFormat: short ? 'M j, Y' : 'l F j, Y', altInput: true, onChange: on_change }
		if (i.name == 'date' && i.form['end_date'])
			config.onOpen = function(_selectedDates, _dateStr, instance) {
				if (_.c(i).querySelector('#end_date_row').style.display=='none') {
					instance.config.maxDate = null
					return
				}
				let m = i.form['end_date'].value
				if (check_not_same && m) {
					 let d = _.date.parse(m)
					 d.setDate(d.getDate() - 1)
					 m = _.date.to_string(d)
				}
				instance.config.maxDate = m
			}
		else if (i.name == 'end_date')
			config.onOpen = function(_selectedDates, _dateStr, instance) {
				let m = i.form.date.value
				if (check_not_same && m) {
					 let d = _.date.parse(m)
					 d.setDate(d.getDate() + 1)
					 m = _.date.to_string(d)
				}
				instance.config.minDate = m
			}
		JS.get('flatpickr.min', function(){flatpickr(i, config)})
	},
	new_time(t) {
		let d = new Date()
		if (t == '12:00 AM') {
			d.setHours(23)
			d.setMinutes(59)
			return d
		}
		let i = t.indexOf(':')
		let h = parseInt(t.substring(0, i))
		if (t.endsWith('AM') && h == '12')
			h = 0
		else if (t.endsWith('PM') && h != '12')
			h += 12
		d.setHours(h)
		let s = t.lastIndexOf(' ')
		d.setMinutes(t.substring(i + 1, s))
		d.setSeconds(0)
		d.setMilliseconds(0)
		return d
	},
	on_repeat_change(s) {
		let i = s.selectedIndex
		let d = _.$('#date_row input')
		let ed = _.$('#end_date_row input')
		if (i == 0)
			ed.value=''
		else if (i == 1) {
			if (ed.value == '')
				ed.value = d.value
		} else if (ed.value == d.value)
			ed.value = ''
		_.$('#end_date_row').style.display = i == 0 ? 'none' : null
		return false
	},
	replace_row(view_def, many) {
		if (!many)
			return false
		let row = _.$('#' + many + "_row")
		if (!row)
			return false
		net.get_text(context+'/Views/' + view_def + '/many_table_row/' + row.dataset.one_id + '/' + many, function(t){_.dom.replace(row, t)})
		return true	
	},
	resize_textarea(ta) {
		if (ta.value)
			ta.style.height = ta.scrollHeight + 'px'
	},
	send(el, on_success) {
		let form = el.closest('form')
		if (!_.form.validate(form))
			return
		if (form.method.toLowerCase() == 'get')
			net.get_json(form.getAttribute('action') + '?' + new URLSearchParams(new FormData(form)).toString(), on_success)
		else
			net.post(form.getAttribute('action'), new FormData(form), on_success)
	},
	set_all_cb(f, c, v) {
		for (const e of f.elements)
			if ((c && e.className === c) || (!c && e.type === 'checkbox'))
				e.checked = v
	},
	show_saved(button) {
		button = _.$(button)
		_.dom.hide(button)
		let span = $span({styles:{'background-color':'yellow'}}, 'saved')
		button.insertAdjacentElement('afterend', span)
		_.form.x = span.clientHeight // force layout
		span.addEventListener("transitionend", function(){this.remove(); _.dom.show(button)}, false)
		span.style.transition = 'opacity 2s'
		span.style.opacity = 0
	},
	submit(s, options) {
		options = options || {}
		let form = s.closest('form')
		if (options.validate && !_.form.validate(form))
			return
		if (options.confirm && !options.confirm())
			return
		if (!_.form.changed(form)) {
			if (options.on_complete)
				options.on_complete()
			return
		}
		if (options.on_submit)
			options.on_submit()
		if (s != form && !options.keep_submit)
			s.style.visibility = 'hidden'
		if (form.db_submit_id && !form.db_submit_id.value)
			form.db_submit_id.value = s.id
		net.post(form.action, new FormData(form), function(o) {
				if (form.action && form.action.endsWith('/insert') && Dialog.top()) {
					form.action = form.action.substring(0, form.action.length-6) + 'update'
					$input({name:'db_key_value', parent:form, type:'hidden', value:o.publish.data})
					let t = form.action.split('/')
					let view = app.get('view:' + decodeURIComponent(t[t.length - 2]).replaceAll('+', ' '))
					if (view)
						view.id = o.publish.data
				}
			}, function() {
				if (s != form)
					s.style.visibility = ''
				if (options.on_complete)
					options.on_complete()
			})
	},
	validate(f) {
		let required_cb = false
		let e = f.elements
		for (let i=0; i<e.length; i++) {
			if (e[i].tagName !== 'TEXTAREA' && !e[i].checkValidity()) {
				let msg = e[i].dataset['required-msg']
				if (msg)
					e[i].setCustomValidity(msg)
				e[i].reportValidity()
				return
			}
			if (e[i].validate && !e[i].validate())
				return false
			if (e[i].disabled)
				continue
			if (e[i].getAttribute('nospaces') && e[i].value && e[i].value.indexOf(' ') != -1) {
				e[i].setCustomValidity('Spaces are not allowed')
				e[i].reportValidity()
				return false
			}
			if ('time' === e[i].dataset['type'] && !this.validate_time(e[i].name, 'true' == e[i].dataset['check_order']))
				return false
			if (e[i].getAttribute('required') || e[i].dataset['required-msg']) {
				let v = null
				if (e[i].tagName === 'SELECT')
					v = e[i].options[e[i].selectedIndex].text
				else if (e[i].tagName === 'INPUT' && e[i].type==='hidden')
					v = e[i].value
				else if (e[i].tagName === 'INPUT' && e[i].type==='radio') {
					let set = false
					let s = e[i].parentNode
					while (s) {
						if (s.tagName === 'SPAN' && s.firstChild.tagName === 'INPUT' && s.firstChild.checked) {
							set = true
							break
						}
						s = s.nextSibling
					}
					if (set)
						continue
				} else
					v = e[i].value
				if (!v || v === '') {
					let msg = e[i].dataset['required-msg']
					if (!msg) {
						let title = e[i].dataset['title']
						if (!title)
							title = e[i].title
						if (!title)
							title = e[i].getAttribute('name').replaceAll('_', ' ')
						msg = 'Please enter a value for \"'+title+'\"'
					}
					Dialog.alert('Required Field', msg, e[i])
					return false
				}
			}
			let max = e[i].getAttribute('max')
			if (parseFloat(e[i].value) > parseFloat(max)) {
				e[i].setCustomValidity('Value Too High', 'Please enter value of at most ' + max)
				e[i].reportValidity()
				return false
			}
			let min = e[i].getAttribute('min')
			if (parseFloat(e[i].value) < parseFloat(min)) {
				e[i].setCustomValidity('Value Too Low', 'Please enter a value of at least ' + min)
				e[i].reportValidity()
				return false
			}
			if (f.required_cb_name && e[i].name.startsWith(f.required_cb_name) && (e[i].checked || e[i].value === 'true'))
				required_cb = true
			if (e[i].tagName === 'INPUT' && e[i].type === 'email' && !e[i].checkValidity()) {
				e[i].setCustomValidity('Invalid Email', 'Please enter a valid email address')
				e[i].reportValidity()
				return false
			}
		}
		if (f.required_cb_name && !required_cb) {
			Dialog.alert('Required Field', 'Please select at least one ' + f.required_cb_name.substring(3))
			return false
		}
		if (f.db_required_set) {
			let s = f.db_required_set.value.split(',')
			for (let i=1; i<s.length; i++) {
				let e = f[s[i]]
				if (e.type === 'number' && parseInt(e.value) || e.type !== 'number' && e.value)
					return true
			}
			Dialog.alert('Required Field', s[0])
			return false
		}
		return true
	},
	validate_time(name, check_order) {
		let s = _.$('#' + name + '1')
		let h = s.options[s.selectedIndex].text
		if (h.length > 0) {
			let m = _.$('#' + name + '2')
			m = m && m.options[m.selectedIndex].text || '00'
			let ap = _.$('#' + name + '3')
			ap = ap.options[ap.selectedIndex].text
			s.form[name].value = h + ':' + m + ' ' + ap
		}
		if (name === 'end_time' && check_order) {
			let end_time = s.form['end_time'].value
			if (end_time == null)
				return true
			let start_time = s.form['start_time'].value
			if (start_time == null)
				return true
			if (_.form.new_time(end_time).getTime() < _.form.new_time(start_time).getTime()) {
				Dialog.alert('Times out of order', 'The end time is before the start time.')
				return false
			}
		}
		return true
	}
}
