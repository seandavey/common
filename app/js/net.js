export const net = {
	delete(url, on_ok) {
		fetch(url, {method: 'DELETE'})
			.then(response => response.json())
			.then(o => net._handle_response(o, on_ok))
 	},
	get_html(url, f, bg) {
		if (!bg)
			app.hide_popups()
		fetch(url)
			.then(response => response.text())
			.then(text => { if (f) { f(_.dom.extract_js(text)) } })
	},
	get_json(url, on_ok, bg) {
		if (!bg)
			app.hide_popups()
		fetch(url)
			.then(response => response.json())
			.then(o => net._handle_response(o, on_ok))
	},
	get_text(url, f) {
		fetch(url)
			.then(response => response.text())
			.then(text => { if (f) f(text) })
	},
	_handle_response(o, on_ok, on_complete) {
		if (o.error) {
			Dialog.alert('error', o.error)
			if (on_complete)
				on_complete(o)
			return
		}
		if (o.alert)
			Dialog.alert(o.alert.title, o.alert.body)
		if (o.message)
			Dialog.alert(null, o.message)
		if (o.warning)
			Dialog.alert('Warning', o.warning)
		if (o.publish)
			app.publish(o.publish.object, o.publish.message, o.publish.data)
		if (o.js)
			eval(o.js)
		if (on_ok && o.ok)
			on_ok(o)
		if (on_complete)
			on_complete(o)
	},
	post(url, data, on_ok, on_complete) {
		if (data instanceof Function)
			data = data()
		else if (typeof data === 'object' && !(data instanceof FormData))
			data = new URLSearchParams(data).toString()
		let o = { body: data, method: 'POST' }
		if (typeof data === 'string')
			o.headers = { 'Content-Type': 'application/x-www-form-urlencoded' }
		fetch(url, o)
			.then(response => response.json())
			.then(o => net._handle_response(o, on_ok, on_complete))
			.catch(err => console.log(err))
 	},
	replace(el, url, options) {
		el = _.$(el)
		if (url)
			el.dataset.url = url
		else
			url = el.dataset.url
		options = options || {}
		if (!options.bg) {
			_.rich_text.remove(el)
			app.hide_popups()
		}
		var tab
		if (el.id.startsWith('c_')) {
			let tabs = el.querySelector('.nav-tabs')
			if (tabs) {
				let t = tabs.parentNode.tabs.current_tab
				if (t)
					tab = t.innerText
			}
		}
		net.get_text(url, function(html) {
			_.dom.set_html(el, html)
			if (tab) {
				let tabs = el.querySelector('.nav-tabs')
				if (tabs) {
					tabs = tabs.parentNode.tabs
					tab = tabs.getTab(tab)
					if (tab)
						tabs.show(tab)
				}
			}
			if (options.on_complete)
				options.on_complete()
			if (options.top)
				window.scrollTo(0, options.top)
			else if (!options.ignore) {
				let r = el.getBoundingClientRect()
				let h = el.tagName === 'TD' ? _.dom.children_height(el): r.height
				if (r.top + h < 0 || r.right < 0 || r.top > window.innerHeight || r.left > window.innerWidth)
					window.scrollBy(r.right < 0 ? r.left : 0, r.top + h < 0 ? r.top : 0)
			}
		})
	}
}
