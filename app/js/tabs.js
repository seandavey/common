export class Tabs {
	constructor(c, labels, options) {
		this.options = options || {};
		c.tabs = this;
		this.id = c.id;
		this.current_tab = null;
		this.tabs = $nav({classes:['nav','nav-tabs']});
		c.insertBefore(this.tabs, c.firstChild);
		if (labels && labels.length > 0) {
			let pane = c.querySelector('.tab-content').firstChild;
			for (var i=0; i<labels.length; i++) {
				this.addTab(labels[i], pane);
				pane = pane.nextElementSibling;
			}
			if (!this.current_tab) {
				if (this.options.start_tab) {
					let index = labels.indexOf(this.options.start_tab);
					if (index != -1)
						this.current_tab = c.firstChild.children[index];
				}
				if (!this.current_tab)
					if (this.options.start_with_last)
						this.current_tab = c.firstChild.lastChild;
					else
						this.current_tab = c.firstChild.firstChild;
			}
			this.current_tab.classList.add('active');
			this.current_tab.pane.style.display = 'block';
		}
	}
	addTab(label, pane) {
		let tab = this.newTab(label, pane)
		if (this.save_tab && label === this.save_tab)
			this.current_tab = tab
		return tab
	}
	closeTab(tab) {
		if (!tab)
			tab = this.current_tab
		if (this.current_tab === tab) {
			let t = tab.nextSibling
			if (!t)
				t = tab.previousSibling
			if (t)
				this.show(t)
		}
		tab.pane.remove()
		tab.remove()
	}
	getTab(text) {
		for (let i=0; i<this.tabs.children.length; i++) {
			let tab = this.tabs.children[i]
			if (tab.innerText == text)
				return tab
		}
		return null
	}
	newTab(label, pane) {
		let b = $button({classes:['nav-link'], html:label, parent:this.tabs, styles:{cursor:'pointer',outline:0}})
		if (this.options.show_close)
			_.ui.icon('x', {events:{click:function(e){let tab=this.parentNode;let tabs=tab.parentNode.parentNode.tabs;tabs.closeTab(tab);e.stopPropagation();return false;}}, parent:b, styles:{marginLeft:5}})
		b.addEventListener('click', this.show.bind(this, b))
		if (!pane)
			pane = $div({classes:['tab-pane'], parent:_.$('#'+this.id).lastChild})
		pane.style.visibility = 'visible'
		b.pane = pane
		return b
	}
	show(tab) {
		if (typeof tab === 'string') {
			tab = this.getTab(tab)
			if (tab == null)
				return
		}
		if (this.current_tab) {
			let b = this.current_tab
			if (b.tagName !== 'BUTTON')
				b = b.querySelector('button')
			this.current_tab.classList.remove('active')
			this.current_tab.pane.style.display ='none'
		}
		let b = tab
		if (b.tagName !== 'BUTTON')
			b = b.querySelector('button')
		b.classList.add('active')
		tab.pane.style.display = 'block'
		this.current_tab = tab
		this.save_tab = b.innerHTML
		return false
	}
}
