package app;

import app.Site.Message;
import db.DBConnection;

public interface Subscriber {
	public void publish(Object object, Message message, Object data, DBConnection db);
}
