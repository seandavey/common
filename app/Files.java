package app;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Path;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;

public class Files {
	public static void
	deleteDirectory(String ...directories) {
		Path path = Site.site.getPath();
		for (String d : directories)
			path = path.resolve(d);
		try {
			java.nio.file.Files.walk(path)
				.map(Path::toFile)
				.forEach(File::delete);
		} catch (IOException e) {
		}
	}

	//--------------------------------------------------------------------------

	public static void
	deleteFiles(Path path) {
		try {
			java.nio.file.Files.walk(path)
				.filter(java.nio.file.Files::isRegularFile)
				.map(Path::toFile)
				.forEach(File::delete);
		} catch (IOException e) {
		}
	}

	//--------------------------------------------------------------------------

	public static String
	getSafeFilename(String filename) {
		if (filename == null)
			return null;
		return filename.replace('\\', '_').replace('/', '_').replace(':', '_').replace(';', '_').replace("%20", " ");
	}

	//--------------------------------------------------------------------------

	public static String
	getNonexistantFilename(String filename, Path dir) {
		File file = dir.resolve(filename).toFile();
		if (!file.exists())
			return filename;
		int index = filename.lastIndexOf('.');
		String extension = index != -1 ? filename.substring(index) : "";
		if (index != -1)
			filename = filename.substring(0, index);
		int n = 0;
		while (file.exists())
			file = dir.resolve(filename + "-" + ++n + extension).toFile();
		return filename + "-" + n + extension;
	}

	//--------------------------------------------------------------------------

	public static boolean
	isViewable(String filename) {
		if (filename == null)
			return false;
		filename = filename.toLowerCase();
		return filename.endsWith(".txt") || filename.endsWith(".htm") || filename.endsWith(".html");
	}

	//--------------------------------------------------------------------------

	public static LocalDateTime
	modified(java.io.File file) {
		return LocalDateTime.ofInstant(Instant.ofEpochMilli(file.lastModified()), ZoneId.systemDefault());
	}

	//--------------------------------------------------------------------------

	public static List<String>
	read(Path file) {
		List<String> lines;
		try {
			lines = java.nio.file.Files.readAllLines(file);
		} catch (FileNotFoundException e) {
			lines = new ArrayList<>();
			lines.add("File " + file + " not found");
		} catch (IOException e) {
			System.out.println(file);
			throw new RuntimeException(e);
		}
		return lines;
	}

	//--------------------------------------------------------------------------

	public static void
	write(Path path, String text) {
		try {
			FileWriter fw = new FileWriter(path.toFile());
			fw.write(text);
			fw.close();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	//--------------------------------------------------------------------------

	public static void
	write(Path path, InputStream is) {
		byte[] buffer = new byte[2048];
		try (OutputStream os = new FileOutputStream(path.toString())) {
			int len = 0;
			while ((len = is.read(buffer)) > 0)
				os.write(buffer, 0, len);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
}
