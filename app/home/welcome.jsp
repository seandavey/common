<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="app.Request,app.Site" %>
<%
Request r = new Request(request, null, response, out);
	Site.site.newHead(r).close();
	r.w.mainOpen();
	if (Site.credentials.update(true, r)) {
		String site_title = Site.site.getDisplayName();
		r.w.h1(site_title != null ? "Welcome to " + site_title : "Welcome");
%>
	<p>
		Thank you for activating your account. To log in to the site please go to
		<a href="<%=Site.site.absoluteURL(Site.site.getHomePage())%>" target="_blank"><%=Site.site.absoluteURL(Site.site.getHomePage())%></a>
	</p>
<%
		String welcome_letter = Site.settings.getString("welcome letter");
		if (welcome_letter != null)
			r.w.write(welcome_letter);
	}
	r.close();
%>
