package app;

import java.io.File;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.Comparator;

import jakarta.servlet.http.Part;
import ui.Table;
import util.Text;
import web.HTMLWriter;

public class FileManager extends Module {
	private String m_root;

	//--------------------------------------------------------------------------

	@Override
	public boolean
	doGet(boolean full_page, Request r) {
		if (!r.userIsAdmin())
			return true;
		if (super.doGet(full_page, r))
			return true;
		String servlet_path = r.request.getServletPath();
		String path = servlet_path.substring(servlet_path.indexOf("FileManager") + 11);
		if (path.length() > 0)
			path = path.substring(1);
		writeDirectory(path, r);
		return true;
	}

	//--------------------------------------------------------------------------

	@Override
	public boolean
	doPost(Request r) {
		if (super.doPost(r))
			return true;
		if (!r.userIsAdmin())
			return false;
		String segment_two = r.getPathSegment(2);
		if (segment_two != null)
			switch (segment_two) {
			case "mkdir":
				Path directory = getRoot();
				String path = r.getPathSegment(1);
				if (path.length() > 0)
					directory = directory.resolve(path);
				directory = directory.resolve(r.getParameter("dir"));
				directory.toFile().mkdirs();
				return true;
			case "upload":
				try {
					Part part = r.getPart("file");
					directory = getRoot();
					path = r.getPathSegment(1);
					if (path.length() > 0)
						directory = directory.resolve(path);
					part.write(directory.resolve(part.getSubmittedFileName()).toString());
					return true;
				} catch (Exception e) {
					throw new RuntimeException(e);
				}
			}
		return false;
	}

	//--------------------------------------------------------------------------

	private Path
	getRoot() {
		if (m_root == null)
			return Site.site.getPath();
		return Site.site.getPath(m_root);
	}

	//--------------------------------------------------------------------------

	public FileManager
	setRoot(String root) {
		m_root = root;
		return this;
	}

	//--------------------------------------------------------------------------

	public void
	write(Request r) {
		HTMLWriter w = r.w;
		w.setId("upload")
			.aOnClickOpen("var d=prompt('enter directory name');if(d)net.post('" + absoluteURL() + "/'+fm_dir+'/mkdir','dir='+d,function(){change_dir(fm_dir==''?d:fm_dir+'/'+d)});");
		w.addStyle("padding-right:3px;margin-bottom:-2px")
			.img("images", "folder.png").write("Create Directory").tagClose()
			.nbsp()
			.aOnClickOpen("Dialog.upload('" + absoluteURL() + "/'+fm_dir+'/upload',function(){change_dir(fm_dir)})");
		w.addStyle("padding-right:3px;margin-bottom:-2px")
			.img("images", "upload.png").write("Upload").tagClose()
			.hr()
			.js("var fm_dir='';")
			.write("<div id=\"fm\">");
		writeDirectory("", r);
		w.write("</div>")
			.js("function change_dir(dir){fm_dir=dir;net.replace('#fm','" + absoluteURL() + "'+(fm_dir==''?'':'/'+fm_dir),dir)}");
	}

	//--------------------------------------------------------------------------

	private void
	writeDirectory(String path, Request r) {
		HTMLWriter w = r.w;
		w.write("<nav aria-label=\"breadcrumb\"><ol class=\"breadcrumb\">");
		if (path.length() > 0) {
			String[] dirs = path.split("/");
			String p = "";
			w.write("<li class=\"breadcrumb-item\"><a href=\"#\" onclick=\"change_dir('')\">root</a></li>");
			for (int i=0; i<dirs.length-1; i++) {
				if (i > 0)
					p += "/";
				p += dirs[i];
				w.write("<li class=\"breadcrumb-item\"><a href=\"#\" onclick=\"change_dir('").write(p).write("')\">").write(dirs[i]).write("</a></li>");
			}
			w.write("<li class=\"breadcrumb-item active\">").write(dirs[dirs.length-1]).write("</li>");
		} else
			w.write("<li class=\"breadcrumb-item active\">root</li>");
		w.write("</ol></nav>");
		Table table = r.w.ui.table();
		table.tr();
		w.addStyle("vertical-align:top");
		table.td();
		Path file_path = getRoot();
		if (path.length() > 0)
			file_path = file_path.resolve(path);
		File[] files = file_path.toFile().listFiles();
		if (files != null) {
			Arrays.sort(files, new Comparator<File>(){
				@Override
				public int
				compare(File o1, File o2) {
					return o1.getName().compareToIgnoreCase(o2.getName());
				}
			});
			Table table2 = w.ui.table().addDefaultClasses().addClass("table-borderless");
			for (File file : files) {
				String name = file.getName();
				if (name.charAt(0) != '.' && !name.equals("CVS") && !name.equals("WEB-INF")) {
					table2.tr().td();
					if (file.isDirectory()) {
						w.aOnClickOpen("change_dir('" + (path.length() > 0 ? path + "/" + name : name) + "')");
						w.addStyle("padding-right:3px;margin-bottom:-2px");
						w.img("images", "folder.png");
						w.write(name);
						w.tagClose();
					} else if (name.endsWith(".png") || name.endsWith(".jpg") || name.endsWith(".jpeg") || name.endsWith(".gif") || name.endsWith(".pdf")) {
						String src = Site.context;
						if (path.length() > 0)
							src += "/" + path.replaceAll("\\\\", "/");
						w.aOnClickOpen("_.$('#file').innerHTML='<img src=&quot;" + src + "/" + name + "&quot; />'");
						w.addStyle("padding-right:3px;margin-bottom:-2px");
						w.img("images", "document.png");
						w.write(name);
						w.tagClose();
					} else {
						w.addStyle("padding-right:3px;margin-bottom:-2px");
						w.img("images", "document.png");
						w.write(name);
					}
					long length = file.length();
					w.addStyle("padding-right:20px;text-align:right");
					table2.td(length < 1024 ? Long.toString(length) :
						length < 1048576 ? Text.two_digits_max.format((double)length / 1024) + " k" :
						length < 1073741824 ? Text.two_digits_max.format((double)length / 1048576) + " M" :
						Text.two_digits_max.format((double)length / 1073741824) + " G");
					table2.td(app.Files.modified(file).toString());
				}
			}
			table2.close();
		}
		w.addStyle("vertical-align:top");
		w.setId("file");
		table.td();
		table.close();
	}
}
