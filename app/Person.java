package app;

import java.nio.file.Path;

import com.eclipsesource.json.Json;
import com.eclipsesource.json.JsonArray;
import com.eclipsesource.json.JsonObject;
import com.eclipsesource.json.JsonValue;

import blocks.Blocks;
import db.DBConnection;
import db.SQL;
import db.Select;
import db.object.DBField;
import db.object.DBObject;
import jakarta.servlet.http.Part;
import util.Text;
import web.HTMLWriter;

public class Person {
	@DBField
	protected String			m_email;
	protected final int			m_id;
	@DBField
	private String				m_first;
	@DBField
	private JsonObject			m_json;
	@DBField
	private String				m_last;
	@DBField
	private boolean				m_must_change_credentials;
	@DBField
	private String				m_picture;
	@DBField
	protected String			m_pronouns;
	@DBField
	private String				m_theme;
	@DBField
	private String				m_timezone;
	@DBField
	private String				m_user_name;
	@DBField
	private String				m_uuid;

	//--------------------------------------------------------------------------

	public
	Person(Request r) {
		m_user_name = r.request.getRemoteUser();
		m_id = r.db.lookupInt("id", "people", "user_name=" + SQL.string(m_user_name), -1);
		load(r.db);
	}

	//--------------------------------------------------------------------------

	public
	Person(int id, DBConnection db) {
		m_id = id;
		load(db);
	}

	//--------------------------------------------------------------------------

	public final void
	deleteData(String name, DBConnection db) {
		if (m_json == null)
			return;
		m_json.remove(name);
		if (m_json.isEmpty())
			m_json = null;
		store(db);
	}

	//--------------------------------------------------------------------------

	public final void
	deletePicture(DBConnection db) {
		if (m_picture != null) {
			Site.site.getPath("people", Integer.toString(m_id), m_picture).toFile().delete();
			m_picture = null;
		}
		store(db);
	}

	//--------------------------------------------------------------------------

	public final double
	getDataDouble(String name, double default_value) {
		if (m_json == null)
			return default_value;
		JsonValue value = m_json.get(name);
		if (value == null)
			return default_value;
		return value.asDouble();
	}

	//--------------------------------------------------------------------------

	public final String
	getDataString(String name) {
		if (m_json == null)
			return null;
		JsonValue value = m_json.get(name);
		if (value == null || value.isNull())
			return null;
		return value.asString();
	}

	//--------------------------------------------------------------------------

	public final String
	getEmail() {
		return m_email;
	}

	//--------------------------------------------------------------------------

	public final int
	getId() {
		return m_id;
	}

	//--------------------------------------------------------------------------

	public final String
	getName() {
		return Text.join(" ", m_first, m_last);
	}

	//--------------------------------------------------------------------------

	public final String
	getPicture() {
		return m_picture == null ? null : Site.context + "/people/" + m_id + "/" + m_picture;
	}

	//--------------------------------------------------------------------------

	public final String
	getTheme() {
		return m_theme;
	}

	//--------------------------------------------------------------------------

	public final String
	getTimezone() {
		return m_timezone;
	}

	//--------------------------------------------------------------------------

	public final String
	getUsername() {
		return m_user_name;
	}

	//--------------------------------------------------------------------------

	public final String
	getUUID(DBConnection db) {
		if (m_uuid == null) {
			m_uuid = Text.uuid();
			store(db);
		}
		return m_uuid;
	}

	//--------------------------------------------------------------------------

	public final void
	load(DBConnection db) {
		DBObject.load(this, "people", m_id, db);
	}

	//--------------------------------------------------------------------------

	public final boolean
	mustChangeCredentials() {
		return m_must_change_credentials;
	}

	//--------------------------------------------------------------------------

	private void
	setData(String name, JsonValue data, DBConnection db) {
		if (data == null) {
			deleteData(name, db);
			return;
		}
		if (m_json == null)
			m_json = new JsonObject();
		m_json.set(name, data);
		store(db);
	}

	//--------------------------------------------------------------------------

	public final void
	setData(String name, String data, DBConnection db) {
		setData(name, Json.value(data), db);
	}

	//--------------------------------------------------------------------------

	public final void
	setData(String name, boolean data, DBConnection db) {
		setData(name, Json.value(data), db);
	}

	//--------------------------------------------------------------------------

	public final void
	setData(String name, double data, DBConnection db) {
		setData(name, Json.value(data), db);
	}

	//--------------------------------------------------------------------------

	public void
	setPicture(Request r) {
		try {
			Path path = Site.site.getPath("people", Integer.toString(m_id));
			path.toFile().mkdirs();
			Part file = r.getPart("file");
			m_picture = Files.getSafeFilename(file.getSubmittedFileName());
			if (m_picture != null) {
				path = path.resolve(m_picture);
				file.write(path.toString());
				store(r.db);
				r.response.add("path", Site.context + "/people/" + m_id + "/" + m_picture);
			}
		} catch (Exception e) {
			r.handleException(e);
		}
	}

	//--------------------------------------------------------------------------

	private void
	store(DBConnection db) {
		DBObject.store(this, "people", m_id, db);
	}

	//--------------------------------------------------------------------------

	public final boolean
	testData(String name) {
		if (m_json == null)
			return false;
		JsonValue value = m_json.get(name);
		if (value == null)
			return false;
		return value.asBoolean();
	}

	//--------------------------------------------------------------------------

	public final void
	write(boolean keep_space, int max_size, boolean printer_friendly, Request r) {
		HTMLWriter w = r.w;
		boolean use_link = !printer_friendly && r.getUser() != null;
		if (use_link)
			w.addStyle("text-decoration:none")
				.aOnClickOpen("Person.popup(" + m_id + ")");
		if (!printer_friendly) {
			if (keep_space)
				w.write("<div style=\"display:inline-block;white-space:nowrap;width:").write(max_size).write("px;\">");
			if (m_picture != null)
				w.addStyle("max-height:" + max_size + "px;max-width:" + max_size + "px")
					.img("people", Integer.toString(m_id), m_picture);
			if (keep_space)
				w.write("</div>");
			if (m_picture != null)
				w.nbsp();
		}
		w.write(getName());
		if (use_link)
			w.tagClose();
	}

	//--------------------------------------------------------------------------

	protected void
	writeProfile(Request r) {
		Blocks b = new Blocks(m_id == r.getUser().getId(), r.w).lookup(new Select("*").from("people").whereIdEquals(m_id), r.db);
		r.w.js("Dialog.top().set_title('Profile','h3')");
		JsonObject page = ((People)Modules.get("People")).getPageTemplate();
		JsonArray blocks = page.get("blocks").asArray();
		for (int i=0; i<blocks.size(); i++)
			b.write(blocks.get(i).asObject());
		b.close();
	}
}
