package app;

import java.util.List;

import app.Site.Message;
import db.DBConnection;
import db.Select;
import util.Array;

public class Avatar implements Subscriber {
	private final Person	m_person;
	private String[]		m_roles;

	//--------------------------------------------------------------------------

	public
	Avatar(Person p) {
		m_person = p;
		Site.site.subscribe("user_roles", this);
	}

	//--------------------------------------------------------------------------

	public final void
	destroy() {
		Site.site.unsubscribe("user_roles", this);
	}

	//--------------------------------------------------------------------------

	public final Person
	getPerson() {
		return m_person;
	}

	//--------------------------------------------------------------------------

	public final boolean
	hasRole(String role, DBConnection db) {
		if (m_roles == null) {
			List<String> a = db.readValues(new Select("role").from("user_roles").whereEquals("people_id", m_person.getId()));
			m_roles = a.toArray(new String[a.size()]);
		}
		if (role.equals("administrator"))
			return Array.indexOf(m_roles, "administrator") != -1;
		if (role.equals("guest"))
			return Array.indexOf(m_roles, "guest") != -1;
		for (String r : m_roles)
			if (role.equals(r) || "admin".equals(r) || "administrator".equals(r))
				return true;
		return false;
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	publish(Object object, Message message, Object data, DBConnection db) {
		m_roles = null;
	}
}
