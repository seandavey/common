package db;

import java.sql.Types;

import app.Site;
import db.column.ColorColumn;
import db.column.Column;
import db.jdbc.JDBCColumn;
import db.jdbc.JDBCTable;
import ui.SelectOption;

public class Categories {
	private boolean					m_allow_editing;
	private Class<? extends Object>	m_object_class = Option.class;
	private final boolean			m_support_color;
	private final String			m_table;

	//--------------------------------------------------------------------------

	public
	Categories(String table, boolean support_color) {
		m_table = table + "_categories";
		m_support_color = support_color;
	}

	//--------------------------------------------------------------------------

	public final void
	addJDBCColumn(JDBCTable table_def) {
		table_def.add(new JDBCColumn(m_table).setOnDeleteSetNull(true));
	}

	//--------------------------------------------------------------------------

	public final void
	adjustTables(DBConnection db) {
		JDBCTable table_def = new JDBCTable(m_table)
			.add(new JDBCColumn("text", Types.VARCHAR));
		if (m_support_color)
			table_def.add(new JDBCColumn("color", Types.VARCHAR));
		JDBCTable.adjustTable(table_def, true, true, db);
		Site.site.removeObjects(m_table);
	}

	//--------------------------------------------------------------------------

	public final Column
	getColumn() {
		return getOptions().newColumn(m_table + "_id").setDisplayName("category");
	}

	//--------------------------------------------------------------------------

	public final String
	getColumnName() {
		return m_table + "_id";
	}

	//--------------------------------------------------------------------------

//	public int
//	getID(String text, DBConnection db) {
//		return db.lookupInt("id", m_table, "text=" + SQL.string(text), 0);
//	}

	//--------------------------------------------------------------------------

	public final SelectOption
	getOption(Rows rows) {
		String id = rows.getString(getColumnName());
		if (id != null)
			return getOptions().getOptionByValue(id);
		return null;
	}

	//--------------------------------------------------------------------------

	public final synchronized Options
	getOptions() {
		Options options = (Options)Site.site.getObjects(m_table);
		if (options != null)
			return options;
		options = new Options(new Select("*").from(m_table).orderBy("lower(text)")).setAllowEditing(true);
		options.setAllowEditing(m_allow_editing);
		options.setObjectClass(m_object_class);
		Site.site.addObjects(options);
		return options;
	}

	//--------------------------------------------------------------------------

	public final OrderBy
	getOrderBy() {
		return  new OrderBy("(SELECT lower(text) FROM " + m_table + " WHERE " + m_table + ".id=" + m_table + "_id)", false, true);
	}

	//--------------------------------------------------------------------------

	public final ViewDef
	newViewDef(String name) {
		if (!name.equals(m_table))
			return null;
		ViewDef view_def = getOptions().newViewDef()
			.setRecordName("Category", true)
			.setColumn(new Column("text").setDisplayName("category").setIsRequired(true));
		if (m_support_color)
			view_def.setColumn(new ColorColumn("color"));
		return view_def;
	}

	//--------------------------------------------------------------------------

	public final Categories
	setAllowEditing(boolean allow_editing) {
		m_allow_editing = allow_editing;
		return this;
	}

	//--------------------------------------------------------------------------

	public final Categories
	setObjectClass(Class<? extends Object> object_class) {
		m_object_class = object_class;
		return this;
	}
}
