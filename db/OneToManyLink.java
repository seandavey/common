package db;

import app.Request;
import app.Site;
import db.column.LookupColumn;
import db.column.LookupColumn.Mode;

public class OneToManyLink extends RelationshipDef<OneToManyLink> {
	private final String	m_many_lookup_display_column;
	private Mode			m_mode;

	//--------------------------------------------------------------------------

	public
	OneToManyLink(String many_view_def_name, String many_lookup_display_column) {
		this.many_view_def_name = many_view_def_name;
		m_many_lookup_display_column = many_lookup_display_column;
	}

	//--------------------------------------------------------------------------

	public final LookupColumn
	getManyLookupColumn(Request r) {
		LookupColumn lookup_column = new LookupColumn("many_id", many_view_def_name, m_many_lookup_display_column, (String)null /*m_many_table_column_where*/, m_many_lookup_display_column);
		lookup_column.setDisplayName(Site.site.getViewDef(many_view_def_name, r.db).getRecordName());
		if (m_mode != null)
			lookup_column.setMode(m_mode);
		return lookup_column;
	}

	//--------------------------------------------------------------------------

	public final Mode
	getMode() {
		return m_mode;
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	leftJoin(Select query, String one_table, Request r) {
		query.leftJoin(one_table, Site.site.getViewDef(many_view_def_name, r.db).getFrom());
	}

	//--------------------------------------------------------------------------

	public final OneToManyLink
	setMode(Mode mode) {
		m_mode = mode;
		return this;
	}
}
