package db;

import app.Request;
import db.column.LookupColumn;
import db.jdbc.JDBCColumn;
import db.jdbc.JDBCTable;
import util.Array;

public class OneTable {
	private final String	m_column_display_name;
	final String			m_column_name;
	private final String	m_table;

	//--------------------------------------------------------------------------

	public
	OneTable(String table, String column_display_name) {
		m_table = table;
		m_column_name = table + "_id";
		m_column_display_name = column_display_name;
	}

	//--------------------------------------------------------------------------

	public final void
	adjust(JDBCTable table_def) {
		table_def.add(new JDBCColumn(m_table));
	}

	//--------------------------------------------------------------------------

	public final void
	adjust(Select query, Request r) {
		int id = r.getSessionInt(m_column_name, 0);
		if (id == 0)
			query.andWhere(m_column_name + " IS NULL");
		else
			query.andWhere(m_column_name + "=" + id);
	}

	//--------------------------------------------------------------------------

	public final LookupColumn
	adjust(ViewDef view_def) {
		String[] column_names = view_def.getColumnNamesForm();
		if (column_names != null)
			view_def.setColumnNamesForm(Array.concat(column_names, m_column_name));
		LookupColumn column = new LookupColumn(m_column_name, m_table, "name") {
			@Override
			public boolean
			isHidden(Request r) {
				if (super.isHidden(r))
					return true;
				return r.getSessionInt(m_column_name, 0) != 0;
			}
		}.setAllowNoSelection(true).setDefaultToSessionAttribute().setDisplayName(m_column_display_name);
		view_def.setColumn(column);
		return column;
	}

	//--------------------------------------------------------------------------

	public final String
	getFilter(String filter, Request r) {
		int id = r.getSessionInt(m_column_name, 0);
		if (id == 0)
			return m_column_name + " IS NULL AND (" + filter + ")";
		return m_column_name + "=" + id + " AND (" + filter + ")";
	}

	//--------------------------------------------------------------------------

	public final boolean
	isOneMode(Request r) {
		return r.getSessionInt(m_column_name, 0) == 0;
	}

	//--------------------------------------------------------------------------

	public final void
	setOneMode(Request r) {
		r.removeSessionAttribute(m_column_name);
	}
}
