package db;

import app.Request;
import app.Site;
import util.Text;
import web.HTMLWriter;

public abstract class Action {
	private final String	m_name;
	private final boolean	m_reload_component;
	private final boolean	m_uses_form;

	//--------------------------------------------------------------------------

	public
	Action(String name, boolean uses_form, boolean reload_component) {
		m_name = name;
		m_uses_form = uses_form;
		m_reload_component = reload_component;
	}

	//--------------------------------------------------------------------------

	public String
	getName() {
		return m_name;
	}

	//--------------------------------------------------------------------------

	public abstract void
	perform(String id, Request r);

	//--------------------------------------------------------------------------

	public final void
	write(String view_def_name, String id, Request r) {
		if (m_uses_form)
			writeForm(view_def_name, id, r);
		else
			writeLink(view_def_name, id, r);
	}

	//--------------------------------------------------------------------------

	private void
	writeFields(HTMLWriter w) {
		w.addStyle("width:80px;margin-left:10px")
			.ui.buttonOnClick(Text.capitalize(m_name), m_reload_component ? "_.form.send(this,function(t){net.replace(_.c(this),null,{top:window.pageYOffset})})" : "_.form.send(this)");
	}

	//--------------------------------------------------------------------------

	private void
	writeForm(String view_def_name, String id, Request r) {
		HTMLWriter w = r.w;
		w.formOpen("POST", Site.context + "/Views/" + view_def_name + "/actions/" + m_name + "/" + id);
		writeFields(w);
		w.tagClose();
	}

	//--------------------------------------------------------------------------

	protected void
	writeLink(String view_def_name, String id, Request r) {
		StringBuilder on_click = new StringBuilder();
		if (m_reload_component)
			on_click.append("this.parentNode.append('running...');this.style.display='none';");
		on_click.append("net.post(context+'/Views/")
			.append(view_def_name)
			.append("/actions/")
			.append(m_name)
			.append("/")
			.append(id)
			.append("',null,function(){");
		if (m_reload_component)
			on_click.append("net.replace(_.c(this),null,{top:document.body.offsetHeight})");
		else
			on_click.append("Dialog.alert('','done')");
		on_click.append("}.bind(this))");
		r.w.aOnClick(m_name, on_click.toString());
	}
}
