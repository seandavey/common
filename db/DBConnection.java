package db;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.sql.Types;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import app.Site;
import app.Site.Message;
import db.jdbc.JDBCColumn;
import db.jdbc.JDBCTable;
import util.Time;

public class DBConnection {
	private Map<String,JDBCTable>	m_cache = new HashMap<>();
	private Connection 				m_connection;
	private String					m_db;

	//--------------------------------------------------------------------------

	private RuntimeException
	abort(Exception e, String string) {
		close();
		if (string != null)
			System.out.println(string);
		Site.site.log(e);
		return new RuntimeException(string, e);
	}

	//--------------------------------------------------------------------------

	public final void
	addColumn(String table, String column, String type, String size, String primary_table) {
		try {
			Statement st = newStatement();
			StringBuilder sql = new StringBuilder("ALTER TABLE ").append(table);
			sql.append(" ADD COLUMN ").append(SQL.columnName(column)).append(' ').append(type);
			if (type.equals("BOOLEAN"))
				sql.append(" DEFAULT FALSE");
			else if (type.endsWith("CHAR") && size != null && size.length() > 0) {
				sql.append('(');
				sql.append(size);
				sql.append(')');
			}
			if (primary_table != null) {
				sql.append(" REFERENCES ");
				sql.append(primary_table);
				sql.append("(id) ON DELETE CASCADE");
			}
			st.execute(sql.toString());
			st.close();
			if (primary_table != null)
				createIndex(table, column);
		} catch (SQLException e) {
			// ignore, probably already exists
		}
	}

	//--------------------------------------------------------------------------

	public final void
	addColumn(String table, JDBCColumn column) {
		try {
			Statement st = newStatement();
			StringBuilder sql = new StringBuilder("ALTER TABLE ")
				.append(table)
				.append(" ADD COLUMN ");
			column.appendSQL(sql);
			String primary_table = column.getPrimaryTable();
			if (primary_table != null && !tableExists(primary_table))
				createTable(primary_table, null);
			Site.site.log(sql.toString(), false);
			st.execute(sql.toString());
			st.close();
			if (primary_table != null)
				createIndex(table, column.name);
		} catch (SQLException e) {
			Site.site.log(e);
		}
	}

	//--------------------------------------------------------------------------

	public final void
	alterColumnType(String table, String column, String type, String size) {
		try {
			Statement st = newStatement();
			String sql = "ALTER TABLE " + table + " ALTER COLUMN " + SQL.columnName(column) + " TYPE " + type;

			if (type.endsWith("CHAR") && size != null && size.length() > 0)
				sql += "(" + size + ")";
			else if (type.equals("DATE"))
				sql += " USING to_date(" + column + ",'MM/DD/YYYY')";
			else if (type.equals("INTEGER"))
				sql += " USING to_number(" + column + ",'9999999999')";
			Site.site.log(sql, false);
			st.execute(sql);
			st.close();
		} catch (SQLException e) {
			throw abort(e, null);
		}
	}

	//--------------------------------------------------------------------------

	public final boolean
	changed(String table, String column, int id, NameValuePairs nvp, Map<String,Object> previous_values) {
		if (nvp.containsName(column)) {
			String value = lookupString(new Select(column).from(table).whereIdEquals(id));
			if (!Site.areEqual(nvp.getString(column), value)) {
				previous_values.put(column, value);
				return true;
			}
		}
		return false;
	}

	//--------------------------------------------------------------------------
	// insert a copy of a record and then copies of related records
	// values in nvp passed in are kept

	public final Result
	clone(String from, NameValuePairs nvp, int id) {
		try {
			Statement st = newStatement();
			ResultSet rs = st.executeQuery("SELECT * FROM " + from + " WHERE id=" + id);
			if (rs.next()) {
				ResultSetMetaData rsmd = rs.getMetaData();
				for (int i=1; i<=rsmd.getColumnCount(); i++) {
					String c = rsmd.getColumnName(i);
					if ("id".equals(c))
						continue;
					if (!nvp.containsName(c))
						nvp.set(c, rs.getString(i));
				}
			}
			st.close();
		} catch (SQLException e) {
			Site.site.log(e);
		}
		Result result = insert(from, nvp.getNamesString(from, this), nvp.getValuesString(from, this));
		if (result.error == null)
			for (String[] key : getExportedKeys(from))
				cloneMany(key[0], key[1], id, result.id);
		return result;
	}

	//--------------------------------------------------------------------------

	public final void
	cloneMany(String from, String one_column, int from_id, int to_id) {
		try {
			NameValuePairs nvp = new NameValuePairs();
			Statement st = newStatement();
			ResultSet rs = st.executeQuery("SELECT * FROM " + from + " WHERE " + one_column + "=" + from_id);
			ResultSetMetaData rsmd = rs.getMetaData();
			while (rs.next()) {
				nvp.clear();
				for (int i=1; i<=rsmd.getColumnCount(); i++) {
					String c = rsmd.getColumnName(i);
					if ("id".equals(c))
						continue;
					if (one_column.equals(c))
						nvp.set(c, to_id);
					else
						nvp.set(c, rs.getString(i));
				}
				insert(from, nvp.getNamesString(from, this), nvp.getValuesString(from, this));
			}
			st.close();
		} catch (SQLException e) {
			Site.site.log(e);
		}
	}

	//--------------------------------------------------------------------------

	public final void
	close() {
		try {
			if (m_connection != null) {
				if (!m_connection.isClosed())
					m_connection.close();
				m_connection = null;
			}
		} catch (SQLException e) {
			Site.site.log(e);
		}
	}

	//--------------------------------------------------------------------------

	public final int
	countRows(String from, String where) {
		StringBuilder query = new StringBuilder("SELECT COUNT(*) FROM ");
		query.append(from);
		if (where != null) {
			query.append(" WHERE ");
			query.append(where);
		}

		try {
			Statement st = newStatement();
			ResultSet rs = st.executeQuery(query.toString());
			rs.next();
			int count = rs.getInt(1);
			st.close();
			return count;
		} catch (SQLException e) {
			throw abort(e, query.toString());
		}
	}

	//--------------------------------------------------------------------------

	public final int
	countRows(Select query) {
		try {
			Statement st = newStatement();
			ResultSet rs = st.executeQuery("SELECT COUNT(*) FROM (" + query.toString() + ") a");
			rs.next();
			int count = rs.getInt(1);
			st.close();
			return count;
		} catch (SQLException e) {
			throw abort(e, query.toString());
		}
	}

	//--------------------------------------------------------------------------

//	public final void
//	createConcatAggregate() {
//		try {
//			Statement st = newStatement();
//			ResultSet rs = st.executeQuery("SELECT 1 from pg_proc WHERE proname='concat' AND proisagg IS TRUE");
//			if (!rs.isBeforeFirst())
//				st.execute("CREATE AGGREGATE concat(BASETYPE=text,SFUNC=textcat,STYPE=text,INITCOND='')");
//			st.close();
//		} catch (SQLException e) {
//		}
//	}

	//--------------------------------------------------------------------------

//	public final void
//	createNaturalSortFunction() {
//		try {
//			Statement st = newStatement();
//			st.executeQuery("create or replace function naturalsort(text)\nreturns bytea language sql immutable strict as $f$\nselect string_agg(convert_to(coalesce(r[2], length(length(r[1])::text) || length(r[1])::text || r[1]), 'SQL_ASCII'),'\\x00')\nfrom regexp_matches($1, '0*([0-9]+)|([^0-9]+)', 'g') r;\n$f$;");
//			st.executeQuery("create or replace function naturalsort(varchar)\nreturns bytea language sql immutable strict as $f$\nselect string_agg(convert_to(coalesce(r[2], length(length(r[1])::text) || length(r[1])::text || r[1]), 'SQL_ASCII'),'\\x00')\nfrom regexp_matches($1, '0*([0-9]+)|([^0-9]+)', 'g') r;\n$f$;");
//			st.close();
//		} catch (SQLException e) {
//		}
//	}

	//--------------------------------------------------------------------------

	public final boolean
	createIndex(String table, String columns) {
		try {
			Statement st = newStatement();
			st.execute("CREATE INDEX IF NOT EXISTS " + table + '_' + columns.replace(',', '_') + " ON " + table + '(' + columns + ')');
			st.close();
			return true;
		} catch (SQLException e) {
			return false;
		}
	}

	//--------------------------------------------------------------------------

	public final void
	createManyToManyLinkTable(String table1, String table2) {
		createTable(table1 + "_" + table2, false, table1 + "_id INTEGER REFERENCES " + table1 + "(id) ON DELETE CASCADE," + table2 + "_id INTEGER REFERENCES " + table2 + "(id) ON DELETE CASCADE", null, false);
		createIndex(table1 + "_" + table2, table1 + "_id");
		createIndex(table1 + "_" + table2, table2 + "_id");
	}

	//--------------------------------------------------------------------------

	public final Exception
	createManyTable(String one_table, String many_table, String columns) {
		return createManyTable(one_table, many_table, columns, null, false);
	}

	//--------------------------------------------------------------------------

	public final Exception
	createManyTable(String one_table, String many_table, String columns, String owner_table, boolean timestamp_records) {
		return createManyTable(one_table, many_table, one_table + "_id", columns, owner_table, timestamp_records);
	}

	//--------------------------------------------------------------------------

	public final Exception
	createManyTable(String one_table, String many_table, String many_column, String columns, String owner_table, boolean timestamp_records) {
		StringBuilder c = new StringBuilder(many_column);
		c.append(" INTEGER REFERENCES ");
		c.append(one_table);
		c.append("(id) ON DELETE CASCADE");
		if (columns != null) {
			c.append(',');
			c.append(columns);
		}
		Exception e = createTable(many_table, true, c.toString(), owner_table, timestamp_records);
		if (e == null)
			createIndex(many_table, one_table + "_id");
		return e;
	}

	//--------------------------------------------------------------------------

	public final Exception
	createTable(String table, String columns) {
		return createTable(table, true, columns, null, false);
	}

	//--------------------------------------------------------------------------

	public final Exception
	createTable(String table, boolean add_id_column, String columns, String owner_table, boolean timestamp_records) {
		if (table == null || table.length() == 0)
			throw new RuntimeException("table is null or zero length in DBConnection.createTable");
		if (tableExists(table))
			return null;
		try {
			StringBuilder s = new StringBuilder("CREATE TABLE ");
			Statement st = newStatement();
			s.append(table);
			s.append(" (");
			if (add_id_column)
				s.append("id SERIAL PRIMARY KEY");
			if (columns != null && columns.length() > 0) {
				if (add_id_column)
					s.append(',');
				s.append(columns);
			}
			if (timestamp_records)
				s.append(",_timestamp_ TIMESTAMP");
			if (owner_table != null && owner_table.length() > 0) {
				s.append(",_owner_ INTEGER REFERENCES ");
				s.append(owner_table);
				s.append("(id) ON DELETE CASCADE");
			}
			s.append(")");
			st.execute(s.toString());
			st.close();
			return null;
		} catch (SQLException e) {
			Site.site.log(e);
			return e;
		}
	}

	//--------------------------------------------------------------------------

	public final void
	delete(String table, int id, boolean publish) {
		delete(table, "id=" + id, publish);
	}

	//--------------------------------------------------------------------------

	public final void
	delete(String table, String column, int id, boolean publish) {
		delete(table, column + "=" + id, publish);
	}

	//--------------------------------------------------------------------------

	public final void
	delete(String table, String column, String value, boolean publish) {
		delete(table, column + "=" + SQL.string(value), publish);
	}

	//--------------------------------------------------------------------------

	public final void
	delete(String table, String where, boolean publish) {
		try {
			Statement st = newStatement();
			String sql = "DELETE FROM " + table;
			if (where != null)
				sql += " WHERE " + where;
				st.execute(sql);
			st.close();
		} catch (SQLException e) {
			throw abort(e, null);
		}
		if (publish)
			Site.site.publish(table, Message.DELETE, where, this);
	}

	//--------------------------------------------------------------------------

	public final void
	dropColumn(String table, String column) {
		try {
			Statement st = newStatement();
			String sql = "ALTER TABLE " + table + " DROP COLUMN IF EXISTS " + SQL.columnName(column);
			Site.site.log(sql, false);
			st.execute(sql);
			st.close();
		} catch (SQLException e) {
			throw abort(e, null);
		}
	}

	//--------------------------------------------------------------------------

	public final void
	dropIndex(String index) {
		try {
			Statement st = newStatement();
			st.execute("DROP INDEX " + index);
			st.close();
		}
		catch (SQLException e) {
			throw abort(e, null);
		}
	}

	//--------------------------------------------------------------------------

	public final void
	dropTable(String table) {
		if (!tableExists(table))
			return;

		List<String[]> exported_keys = getExportedKeys(table);
		for (String[] exported_key : exported_keys)
			dropTable(exported_key[0]);

		try {
			Statement st = newStatement();
			st.execute("DROP TABLE " + table);
			st.close();
		} catch (SQLException e) {
			throw abort(e, null);
		}
	}

	//--------------------------------------------------------------------------
	// TODO: remove this

	public final void
	execute(String sql) {
		try {
			Statement st = newStatement();
			st.execute(sql);
			st.close();
		} catch (SQLException e) {
			throw abort(e, sql);
		}
	}

	//--------------------------------------------------------------------------

	public final boolean
	exists(String from, String where) {
		StringBuilder query = new StringBuilder("SELECT EXISTS(SELECT 1 FROM ").append(from);
		if (where != null)
			query.append(" WHERE ").append(where);
		query.append(")");

		try {
			Statement st = newStatement();
			ResultSet rs = st.executeQuery(query.toString());
			rs.next();
			boolean exists = rs.getBoolean(1);
			st.close();
			return exists;
		} catch (SQLException e) {
			throw abort(e, query.toString());
		}
	}

	//--------------------------------------------------------------------------

	public final boolean
	exists(Select query) {
		return exists(query.getFrom(), query.getWhere());
	}

	//--------------------------------------------------------------------------

	public final List<String>
	findTablesWithColumn(String column) {
		ArrayList<String> tables = new ArrayList<>();
		try {
			ResultSet t = getMetaData().getTables(null, "public", null, new String[] { "TABLE" });
			while (t.next()) {
				String table = t.getString("TABLE_NAME");
				ResultSet columns = getMetaData().getColumns(null, "public", table, column);
				if (columns.isBeforeFirst())
					tables.add(table);
				columns.close();
			}
			t.close();
		} catch (SQLException e) {
			throw abort(e, null);
		}
		return tables;
	}

	//--------------------------------------------------------------------------

	public final JDBCColumn
	getColumn(String table_name, String column_name) {
		JDBCTable jdbc_table = getJDBCTable(table_name);
		if (jdbc_table == null)
			return null;
		return jdbc_table.getColumn(column_name);
	}

	//--------------------------------------------------------------------------
	/**
	 * Returns an array of strings containing the column names for a result set.
	 * @param rs ResultSet
	 * @return	array of column names
	 */

	public final String[]
	getColumnNames(ResultSet rs) {
		ArrayList<String> columns = new ArrayList<>();
		try {
			ResultSetMetaData rsmd = rs.getMetaData();
			for (int i=1,n=rsmd.getColumnCount(); i<=n; i++) {
				String column_name = rsmd.getColumnName(i);
				if (!(column_name.equals("id") || column_name.startsWith("_")))
					columns.add(column_name);
			}
		} catch (SQLException e) {
		}
		return columns.toArray(new String[columns.size()]);
	}

	//--------------------------------------------------------------------------

	public final List<String[]>
	getExportedKeys(String table) {
		try {
			ArrayList<String[]> exported_keys = new ArrayList<>();
			ResultSet rs = getMetaData().getExportedKeys(null, "public", table);
			while (rs.next()) {
				int delete_rule = rs.getInt("DELETE_RULE");
				exported_keys.add(new String[] {
					rs.getString("FKTABLE_NAME"),
					rs.getString("FKCOLUMN_NAME"),
					delete_rule == DatabaseMetaData.importedKeyNoAction ? "no action" : delete_rule == DatabaseMetaData.importedKeyCascade ? "cascade" : "set null"
				});
			}
			rs.close();
			return exported_keys;
		} catch (SQLException e) {
			throw abort(e, null);
		}
	}

	//--------------------------------------------------------------------------

	public final ResultSet
	getIndexes(String table) {
		try {
			return getMetaData().getIndexInfo(null, "public", table, false, false);
		} catch (SQLException e) {
			throw abort(e, null);
		}
	}

	//--------------------------------------------------------------------------

	private final DatabaseMetaData
	getMetaData() {
		try {
			if (m_connection == null || m_connection.isClosed())
				m_connection = newConnection();
			return m_connection.getMetaData();
		} catch (SQLException e) {
			throw abort(e, null);
		}
	}

	//--------------------------------------------------------------------------

	public final Site
	getSite() {
		return Site.site;
	}

	//--------------------------------------------------------------------------

	public final JDBCTable
	getJDBCTable(String name) {
		if (m_cache.containsKey(name))
			return m_cache.get(name);
		if (!tableExists(name)) {
			m_cache.put(name, null);
			return null;
		}
		return newJDBCTable(name);
	}

	//--------------------------------------------------------------------------

	public final List<String>
	getTableNames(boolean get_hidden_tables) {
		ArrayList<String> table_names = new ArrayList<>();
		try {
			ResultSet tables = getMetaData().getTables(null, "public", null, new String[] { "TABLE" });
			while (tables.next()) {
				String table = tables.getString("TABLE_NAME");
				if (get_hidden_tables || table.charAt(0) != '_')
					table_names.add(table);
			}
			tables.close();
		} catch (SQLException e) {
			throw abort(e, null);
		}
		if (table_names.size() == 0)
			return null;
		return table_names;
	}

	//--------------------------------------------------------------------------

	public final List<String>
	getTables(String name) {
		return readValues(new db.Select("table_name").from("information_schema.tables").where("table_name ILIKE '" + name + "'").orderBy("1"));
	}

	//--------------------------------------------------------------------------

	public final boolean
	hasColumn(String table_name, String column_name) {
		return getColumn(table_name, column_name) != null;
	}

	//--------------------------------------------------------------------------

	public final Result
	insert(String from, NameValuePairs nvp) {
		return insert(from, nvp.getNamesString(from, this), nvp.getValuesString(from, this));
	}

	//--------------------------------------------------------------------------
	/**
	 * insert a new row into a table.
	 * @param from the table
	 * @param columns comma separated list of columns, may be null
	 * @param values comma separated list of values
	 * @return id of new record
	 */

	public final Result
	insert(String from, String columns, String values) {
		int id = -1;
		boolean return_id = hasColumn(from, "id");
		if (hasColumn(from, "_timestamp_") && (columns == null || columns.indexOf("_timestamp_") == -1)) {
			if (columns != null)
				columns += ",_timestamp_";
			else
				columns = "_timestamp_";
			if (values != null)
				values += ",'" + DateTimeFormatter.ofPattern("MM/dd/yyyy hh:mm:ss a").format(Time.newDateTime()) + "'";
			else
				values = "'" + DateTimeFormatter.ofPattern("MM/dd/yyyy hh:mm:ss a").format(Time.newDateTime()) + "'";
		}
		StringBuilder query = new StringBuilder("INSERT INTO ");
		query.append(from);
		if (columns != null)
			query.append(" (").append(columns).append(')');
		if (values != null)
			query.append(" VALUES(").append(values).append(")");
		else
			query.append(" DEFAULT VALUES");
		if (return_id)
			query.append(" RETURNING id");
		try {
			Statement st = newStatement();
			st.execute(query.toString());
			if (return_id) {
				ResultSet rs = st.getResultSet();
				if (rs.next())
					id = rs.getInt(1);
			}
			st.close();
		} catch (SQLException e) {
			String err = e.toString();
			if (err.indexOf("violates unique constraint") != -1)
				err = err.substring(err.indexOf("Key ") + 4);
			else if (err.indexOf("violates not-null constraint") != -1) {
				int index = err.indexOf('"');
				err = err.substring(index + 1, err.indexOf('"', index + 1)) + " is required";
			}
			return new Result(err);
		}
		Site.site.publish(from, Message.INSERT, id, this);
		return new Result(id);
	}

	//--------------------------------------------------------------------------

	public final boolean
	lookupBoolean(Select query) {
		boolean value = false;
		try {
			Statement st = newStatement();
			ResultSet rs = st.executeQuery(query.limit(1).toString());
			if (rs.next())
				value = rs.getBoolean(1);
			st.close();
		} catch (SQLException e) {
			System.out.println(query.toString());
			Site.site.log(e);
		}
		return value;
	}

	//--------------------------------------------------------------------------

	public final LocalDate
	lookupDate(Select query) {
		Date date = null;
		try {
			Statement st = newStatement();
			ResultSet rs = st.executeQuery(query.limit(1).toString());
			if (rs.next())
				date = rs.getDate(1);
			st.close();
		} catch (SQLException e) {
			System.out.println(query.toString());
			Site.site.log(e);
		}
		return date == null ? null : date.toLocalDate();
	}

	//--------------------------------------------------------------------------

	public final double
	lookupDouble(Select query, double default_value) {
		double value = default_value;

		try {
			Statement st = newStatement();
			ResultSet rs = st.executeQuery(query.limit(1).toString());
			if (rs.next())
				value = rs.getDouble(1);
			st.close();
		} catch (SQLException e) {
			System.out.println(query.toString());
			Site.site.log(e);
		}

		return value;
	}

	//--------------------------------------------------------------------------
	/**
	 * lookup the int value for a column in a single row of a table.
	 * @param column the column that contains the value you want
	 * @param from name of the table
	 * @param where
	 * @param default_value value to return when row not found
	 * @return
	 */

	public final int
	lookupInt(String column, String from, String where, int default_value) {
		return lookupInt(new Select(column).from(from).where(where), default_value);
	}

	//--------------------------------------------------------------------------

	public int
	lookupInt(Select query, int default_value) {
		int value = default_value;

		try {
			Statement st = newStatement();
			ResultSet rs = st.executeQuery(query.limit(1).toString());
			if (rs.next())
				value = rs.getInt(1);
			st.close();
		} catch (SQLException e) {
			System.out.println(query.toString());
			Site.site.log(e);
		}

		return value;
	}

	//--------------------------------------------------------------------------

	public final float
	lookupFloat(Select query, float default_value) {
		float value = default_value;

		try {
			Statement st = newStatement();
			ResultSet rs = st.executeQuery(query.limit(1).toString());
			if (rs.next())
				value = rs.getFloat(1);
			st.close();
		} catch (SQLException e) {
			System.out.println(query.toString());
			Site.site.log(e);
		}

		return value;
	}

	//--------------------------------------------------------------------------

	public final String
	lookupString(Select query) {
		StringBuilder value = new StringBuilder();

		try {
			Statement st = newStatement();
			ResultSet rs = st.executeQuery(query.toString());
			if (rs.next()) {
				int num_columns = rs.getMetaData().getColumnCount();
				for (int i=1; i<=num_columns; i++) {
					String s = rs.getString(i);
					if (s != null) {
						if (value.length() > 0)
							value.append(' ');
						value.append(s.trim());
					}
				}
			}
			st.close();
		} catch (SQLException e) {
			throw abort(e, query.toString());
		}

		if (value.length() == 0)
			return null;
		return value.toString();
	}

	//--------------------------------------------------------------------------

	public final String
	lookupString(String column, String from, int id) {
		return lookupString(new Select(column).from(from).whereIdEquals(id));
	}

	//--------------------------------------------------------------------------

	public final String
	lookupString(String column, String from, String where) {
		return lookupString(new Select(column).from(from).where(where));
	}

	//--------------------------------------------------------------------------

	public final LocalDateTime
	lookupTimestamp(Select query) {
		Timestamp timestamp = null;

		try {
			Statement st = newStatement();
			ResultSet rs = st.executeQuery(query.limit(1).toString());
			if (rs.next())
				timestamp = rs.getTimestamp(1);
			st.close();
		} catch (SQLException e) {
			System.out.println(query.toString());
			Site.site.log(e);
		}

		if (timestamp == null)
			return null;
		return timestamp.toLocalDateTime();
	}

	//--------------------------------------------------------------------------

	private Connection
	newConnection() {
		if (m_db != null)
			try {
				return DriverManager.getConnection(m_db);
			} catch (SQLException e) {
				String s = e.toString();
				Site.site.log(s, s.indexOf("too many connections") == -1 && s.indexOf("remaining connection slots are reserved") == -1);
			}
		return Site.site.newConnection();
	}

	//--------------------------------------------------------------------------

	public final JDBCTable
	newJDBCTable(String name) {
		JDBCTable jdbc_table = new JDBCTable(name, getMetaData());
		m_cache.put(name, jdbc_table);
		return jdbc_table;
	}

	//--------------------------------------------------------------------------

	private Statement
	newStatement() throws SQLException {
		if (m_connection == null)
			m_connection = newConnection();
		if (m_connection.isClosed())
			m_connection = newConnection();
		return m_connection.createStatement();
	}

	//--------------------------------------------------------------------------

	public final String[]
	readRow(Select query) {
		String[] cols = null;
		ResultSet rs = select(query);
		try {
			int num_cols = rs.getMetaData().getColumnCount();
			if (rs.next()) {
				cols = new String[num_cols];
				for (int i=0; i<num_cols; i++)
					cols[i] = rs.getString(i + 1);
			}
			rs.getStatement().close();
		} catch (SQLException e) {
			throw abort(e, null);
		}
		return cols;
	}

	//--------------------------------------------------------------------------

//	public final void
//	readRow(Select query, NameValuePairs nvp) {
//		ResultSet rs = select(query);
//		try {
//			if (rs.next())
//				for (int i=1,n=rs.getMetaData().getColumnCount(); i<=n; i++)
//					nvp.set(rs.getMetaData().getColumnName(i), rs.getString(i));
//			rs.getStatement().close();
//		} catch (SQLException e) {
//			throw abort(e, null);
//		}
//	}

	//--------------------------------------------------------------------------

	public final List<String[]>
	readRows(Select query) {
		ArrayList<String[]> rows = new ArrayList<>();
		ResultSet rs = select(query);
		try {
			int num_cols = rs.getMetaData().getColumnCount();
			while (rs.next()) {
				String[] cols = new String[num_cols];
				for (int i=0; i<num_cols; i++)
					cols[i] = rs.getString(i + 1);
				rows.add(cols);
			}
			rs.getStatement().close();
		} catch (SQLException e) {
			throw abort(e, null);
		}
		return rows;
	}

	//--------------------------------------------------------------------------

	public final Object[]
	readRowObjects(Select query) {
		Object[] objects = null;
		ResultSet rs = select(query);
		try {
			if (rs.next())
				objects = readRowObjects(rs);
			rs.getStatement().close();
		} catch (SQLException e) {
			throw abort(e, null);
		}
		return objects;
	}

	//--------------------------------------------------------------------------

	private static Object[]
	readRowObjects(ResultSet rs) throws SQLException {
		ResultSetMetaData meta_data = rs.getMetaData();
		int num_cols = meta_data.getColumnCount();
		Object[] values = new Object[num_cols];
		for (int i=0; i<num_cols; i++) {
			int type = meta_data.getColumnType(i + 1);
			if (type == Types.BIT || type == Types.BOOLEAN)
				values[i] = rs.getBoolean(i + 1);
			else if (type == Types.DATE) {
				values[i] = rs.getDate(i + 1);
				if (values[i] != null)
					values[i] = ((java.sql.Date)values[i]).toLocalDate();
			} else if (type == Types.DOUBLE)
				values[i] = rs.getDouble(i + 1);
			else if (type == Types.INTEGER) {
				int n = rs.getInt(i + 1);
				if (!rs.wasNull())
					values[i] = n;
			} else if (type == Types.REAL)
				values[i] = rs.getFloat(i + 1);
			else if (type == Types.TIME) {
				values[i] = rs.getTime(i + 1);
				if (values[i] != null)
					values[i] = ((java.sql.Time)values[i]).toLocalTime();
			} else if (type == Types.TIMESTAMP)
				values[i] = rs.getTimestamp(i + 1);
			else
				values[i] = rs.getString(i + 1);
		}
		return values;
	}

	//--------------------------------------------------------------------------

	public final List<Object[]>
	readRowsObjects(Select query) {
		ArrayList<Object[]> rows = new ArrayList<>();
		ResultSet rs = select(query);
		try {
			while (rs.next())
				rows.add(readRowObjects(rs));
			rs.getStatement().close();
		} catch (SQLException e) {
			throw abort(e, null);
		}
		return rows;
	}

	//--------------------------------------------------------------------------

	public final List<String>
	readValues(Select query) {
		ArrayList<String> values = new ArrayList<>();
		ResultSet rs = select(query);
		try {
			while (rs.next())
				values.add(rs.getString(1));
			rs.getStatement().close();
		} catch (SQLException e) {
			throw abort(e, null);
		}
		return values;
	}

	//--------------------------------------------------------------------------

	public final List<Integer>
	readValuesInt(Select query) {
		ArrayList<Integer> rows = new ArrayList<>();
		ResultSet rs = select(query);
		try {
			while (rs.next())
				rows.add(rs.getInt(1));
			rs.getStatement().close();
		} catch (SQLException e) {
			throw abort(e, null);
		}
		return rows;
	}

	//--------------------------------------------------------------------------

	public final void
	renameColumn(String table, String column, String new_name) {
		try {
			Statement st = newStatement();
			st.execute("ALTER TABLE " + table + " RENAME COLUMN " + SQL.columnName(column) + " TO " + SQL.columnName(new_name));
			st.close();
		} catch (SQLException e) {
			Site.site.log(e);
		}
	}

	//--------------------------------------------------------------------------

	public final void
	renameTable(String table, String new_name) {
		try {
			Statement st = newStatement();
			st.execute("ALTER TABLE " + table + " RENAME TO " + new_name);
			if (sequenceExists(table + "_id_seq")) {
				st.execute("ALTER TABLE " + table + "_id_seq" + " RENAME TO " + new_name + "_id_seq");
				st.execute("ALTER TABLE " + new_name + " ALTER COLUMN id SET DEFAULT nextval('public." + new_name + "_id_seq'::text)");
			}
			st.close();
			List<String[]> referencing_tables = getExportedKeys(new_name);
			for (String[] referencing_table : referencing_tables)
				if (referencing_table[1].equals(table + "_id"))
					renameColumn(referencing_table[0], referencing_table[1], new_name + "_id");
		} catch (SQLException e) {
			Site.site.log(e);
		}
	}

	//--------------------------------------------------------------------------

	public final boolean
	rowExists(String table, int id) {
		return rowExists(table, "id=" + id);
	}

	//--------------------------------------------------------------------------

	public final boolean
	rowExists(String table, String where) {
		boolean row_exists = false;
		try {
			Statement st = newStatement();
			ResultSet rs;
			if (where == null || where.length() == 0)
				rs = st.executeQuery("SELECT 1 FROM " + table + " LIMIT 1");
			else
				rs = st.executeQuery("SELECT 1 FROM " + table + " WHERE " + where + " LIMIT 1");
			row_exists = rs.isBeforeFirst();
			st.close();
		} catch (SQLException e) {
			throw abort(e, null);
		}
		return row_exists;
	}

	//--------------------------------------------------------------------------

	public final ResultSet
	select(Select query) {
		if (query == null) {
			Site.site.log("null query in DBConnection.select", true);
			return null;
		}
		return select(query.toString());
	}

	//--------------------------------------------------------------------------

	public final ResultSet
	select(String query) {
		try {
			Statement statement = newStatement();
			statement.execute(query);
			return statement.getResultSet();
		} catch (SQLException e) {
			close();
			m_connection = newConnection();
			try {
				Statement statement = newStatement();
				statement.execute(query);
				return statement.getResultSet();
			} catch (SQLException e1) {
				throw abort(e1, query);
			}
		}
	}

	//--------------------------------------------------------------------------

	public final boolean
	sequenceExists(String table) {
		boolean	sequence_exists = false;

		try {
			ResultSet tables = getMetaData().getTables(null, "public", table, new String[] { "SEQUENCE" });
			sequence_exists = tables.isBeforeFirst();
			tables.close();
		} catch (SQLException e) {
			throw abort(e, null);
		}

		return sequence_exists;
	}

	//--------------------------------------------------------------------------

	public final void
	setColumnDefault(String table, String column_name, String default_value) {
		try {
			Statement st = newStatement();
			if (default_value == null || default_value.length() == 0)
				st.execute("ALTER TABLE " + table + " ALTER COLUMN " + SQL.columnName(column_name) + " DROP DEFAULT");
			else {
				JDBCColumn jdbc_column = getJDBCTable(table).getColumn(column_name);
				if (jdbc_column.isString() || jdbc_column.isDate() || jdbc_column.isTime() || jdbc_column.isTimestamp())
					st.execute("ALTER TABLE " + table + " ALTER COLUMN " + SQL.columnName(column_name) + " SET DEFAULT " + SQL.string(default_value));
				else
					st.execute("ALTER TABLE " + table + " ALTER COLUMN " + SQL.columnName(column_name) + " SET DEFAULT " + default_value);
			}
			st.close();
		} catch (SQLException e) {
			throw abort(e, null);
		}
	}

	//--------------------------------------------------------------------------

	public final void
	setDB(String db) {
		close();
		m_db = db;
	}

	//--------------------------------------------------------------------------

	public final void
	setForeignKey(String table, String column, String reftable, String refcolumn, boolean cascade) {
		try {
			Statement st = newStatement();
			String name = table + "_" + column + "_fkey";
			st.execute("ALTER TABLE " + table + " DROP CONSTRAINT IF EXISTS " + name);
			st.execute("ALTER TABLE " + table + " ADD CONSTRAINT " + name + " FOREIGN KEY (" + column + ") REFERENCES " + reftable + "(" + refcolumn + ") ON DELETE " + (cascade ? "CASCADE" : "SET NULL"));
			st.close();
		} catch (SQLException e) {
			throw abort(e, null);
		}
	}

	//--------------------------------------------------------------------------

	public final void
	setNotNull(String table, String column, boolean not_null) {
		try {
			Statement st = newStatement();
			st.execute("ALTER TABLE " + table + " ALTER " + column + (not_null ? " SET NOT NULL" : " DROP NOT NULL"));
			st.close();
		} catch (SQLException e) {
			throw abort(e, null);
		}
	}

	//--------------------------------------------------------------------------

	public final boolean
	tableExists(String table) {
		boolean	table_exists = false;

		try {
			ResultSet tables = getMetaData().getTables(null, "public", table, new String[] { "TABLE" });
			table_exists = tables.isBeforeFirst();
			tables.close();
		} catch (SQLException e) {
			throw abort(e, null);
		}

		return table_exists;
	}

	//--------------------------------------------------------------------------

	public final void
	truncateTable(String table) {
		String sql = "TRUNCATE TABLE " + table + " RESTART IDENTITY";
		try {
			Statement st = newStatement();
			st.execute(sql);
			st.close();
		} catch (SQLException e) {
			throw abort(e, sql);
		}
	}

	//--------------------------------------------------------------------------

	public final void
	update(String from, NameValuePairs nvp, int id) {
		if (nvp.size() == 0)
			Site.site.log("nvp empty in DBConnection.update(), id:" + id, true);
		else
			update(from, nvp.getUpdateString(from, this), id);
	}

	//--------------------------------------------------------------------------

	public final void
	update(String from, NameValuePairs nvp, String where) {
		if (nvp.size() == 0)
			Site.site.log("nvp empty in DBConnection.update() where " + where, true);
		else
			update(from, nvp.getUpdateString(from, this), where);
	}

	//--------------------------------------------------------------------------

	public final void
	update(String from, String names_and_values, int id) {
		update(from, names_and_values, "id=" + id);
	}

	//--------------------------------------------------------------------------

	public final void
	update(String from, String names_and_values, String where) {
		if (names_and_values.length() == 0) {
			Site.site.log("empty names_and_values for " + from + ": " + where, false);
			return;
		}
		String sql = "UPDATE " + from + " SET " + names_and_values;
		if (where != null)
			sql += " WHERE " + where;
		try {
			Statement st = newStatement();
			st.executeUpdate(sql);
			st.close();
			Site.site.publish(from, Message.UPDATE, where, this);
		} catch (SQLException e) {
			throw abort(e, sql);
		}
	}

	//--------------------------------------------------------------------------

	public final Result
	updateOrInsert(String from, NameValuePairs nvp, String where) {
		if (where != null && where.length() > 0)
			if (exists(from, where)) {
				update(from, nvp.getUpdateString(from, this), where);
				if (hasColumn(from, "id"))
					return new Result(lookupInt("id", from, where, -1));
				return new Result(-1); // many_many table
			}
		return insert(from, nvp);
	}
}
