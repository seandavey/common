package db.feature;

import app.Request;
import db.View;

public abstract class QuickFilter extends Feature {
	public
	QuickFilter(Location location) {
		super(location);
	}

	// --------------------------------------------------------------------------

	public abstract void
	appendDefaultValue(StringBuilder s, View v, Request r);

	// --------------------------------------------------------------------------

	public abstract String
	getFilter();

	// --------------------------------------------------------------------------

	// only used in ViewDef.insert and use is currently commented out
//	public abstract String
//	set(NameValuePairs nvp);
}
