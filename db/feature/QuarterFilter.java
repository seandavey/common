package db.feature;

import app.Request;
import db.View;
import util.Time;
import web.HTMLWriter;

public class QuarterFilter extends QuickFilter {
	private final String			m_table;

	private static final String[]	s_ordinals = { "1st", "2nd", "3rd", "4th" };

	// --------------------------------------------------------------------------

	public
	QuarterFilter(String table, Location location) {
		super(location);
		m_table = table;
	}

	// --------------------------------------------------------------------------

	@Override
	public void
	appendDefaultValue(StringBuilder s, View v, Request r) {
		int latest_month =  r.db.lookupInt("(extract(month from max(date))::int -1) /3", m_table, v.getState().getBaseFilter(), Time.newDate().getMonthValue() - 1);
		if (s.length() > 0)
			s.append(" AND ");
		s.append(getFilter()).append("='").append(latest_month).append("'");
	}

	// --------------------------------------------------------------------------

	@Override
	public String
	getFilter() {
		return "(extract(month from date)::int - 1)/3";
	}

	// --------------------------------------------------------------------------

//	@Override
//	public String
//	set(NameValuePairs nvp) {
//		return getFilter() + "='" + nvp.getDate("date").getMonthValue() / 3 + "'";
//	}

	// --------------------------------------------------------------------------

	@Override
	public boolean
	write(Location location, View v, Request r) {
		int quarter = (Time.newDate().getMonthValue() - 1) / 3;
		String filter = v.getState().getFilter();
		if (filter != null) {
			int start = filter.indexOf("(extract(month from date)::int - 1)/3='");
			if (start != -1) {
				start += 39;
				int end = filter.indexOf("'", start);
				quarter = Integer.parseInt(filter.substring(start, end));
			}
		}
		HTMLWriter w = r.w
			.setAttribute("data-filter", getFilter())
			.ui.selectOpen(null, true);
		for (int i=0; i<4; i++) {
			w.write("<option");
			if (i == quarter)
				w.write(" selected=\"selected\"");
			w.write(" value=\"").write(i).write("\">").write(s_ordinals[i]).write(" quarter</option>");
		}
		w.tagClose();
		return true;
	}
}
