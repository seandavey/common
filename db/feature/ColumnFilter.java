package db.feature;

import app.Request;
import db.RowsSelect;
import db.Select;
import db.View;
import ui.Option;

public class ColumnFilter extends QuickFilter {
	private boolean			m_allow_none;
	private final String	m_column;
	private String			m_filter;
	private final String	m_label;

	// --------------------------------------------------------------------------

	public
	ColumnFilter(String column, String label, Location location) {
		super(location);
		m_column = column;
		m_label = label;
	}

	// --------------------------------------------------------------------------

	@Override
	public void
	appendDefaultValue(StringBuilder s, View v, Request r) {
	}

	// --------------------------------------------------------------------------

	@Override
	public String
	getFilter() {
		return m_filter != null ? m_filter : m_column;
	}

	// --------------------------------------------------------------------------

//	@Override
//	public String
//	set(NameValuePairs nvp) {
//		String value = nvp.getString(m_column);
//		if (value == null || value.length() == 0)
//			return null;
//		return getFilter() + "=" + SQL.string(value);
//	}

	// --------------------------------------------------------------------------

	public final ColumnFilter
	setAllowNone(boolean allow_none) {
		m_allow_none = allow_none;
		return this;
	}

	// --------------------------------------------------------------------------

	public final ColumnFilter
	setFilter(String filter) {
		m_filter = filter;
		return this;
	}

	// --------------------------------------------------------------------------

	@Override
	public boolean
	write(Location location, View v, Request r) {
		Select query = new Select(m_column).distinctOn("lower(" + m_column + ")").from(v.getFrom()).where(v.getWhere()).andWhere(v.getState().getBaseFilter()).orderByLower(m_column);
		RowsSelect rs = new RowsSelect(null, query, m_column, null, r);
		if (rs.size() == 0)
			return false;
		String filter = v.getState().getFilter();
		if (filter != null)
			if (filter.indexOf(m_column + " IS NULL") != -1)
				filter = "null";
			else {
				int start = filter.indexOf(m_column + "='");
				if (start != -1) {
					StringBuilder s = new StringBuilder(filter.substring(start + m_column.length() + 2));
					for (int i=0; i<s.length(); i++)
						if (s.charAt(i) == '\'') {
							if (i == s.length() - 1 || s.charAt(i + 1) != '\'') {
								filter = s.substring(0, i);
								break;
							}
							s.deleteCharAt(i);
						}
				} else
					filter = null;
			}
		if (filter == null)
			filter = "all";
		if (m_label != null)
			r.w.write(m_label).space();
		r.w.setAttribute("data-filter", getFilter());
		rs.addFirstOption(new Option("All", "all"));
		if (m_allow_none)
			rs.addFirstOption(new Option("None", "null"));
		rs.addFirstOption(new Option("-", null))
			.setInline(true)
			.setSelectedOption(null, filter)
//			.setType(Type.DROPDOWN)
			.write(r.w);
		return true;
	}
}
