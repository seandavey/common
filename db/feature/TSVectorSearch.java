package db.feature;

import app.Request;
import db.View;
import db.ViewDef;
import util.Text;
import web.HTMLWriter;

public class TSVectorSearch extends Feature {
	private final String	m_placeholder;
	private ViewDef			m_view_def;

	// --------------------------------------------------------------------------

	public
	TSVectorSearch(String placeholder, Location location) {
		super(location);
		m_placeholder = placeholder;
	}

	// --------------------------------------------------------------------------

	@Override
	public TSVectorSearch
	init(ViewDef view_def) {
		m_view_def = view_def;
		return this;
	}

	// --------------------------------------------------------------------------

	@Override
	public boolean
	write(Location location, View v, Request r) {
		String id = Text.uuid();
		HTMLWriter w = r.w;
		w.js("function quicksearch(){net.post(context+'/ViewStates/" + m_view_def.getName() + "/tsvectorsearch',{value:_.$('#" + id + "').value},function(){net.replace(_.c('#" + id + "'))})}")
			.write("<div style=\"display:inline-block;\">")
			.write("<input id=\"").write(id).write("\" type=\"text\" onkeydown=\"if(event.key=='Enter')quicksearch()\"");
		if (m_placeholder != null)
			w.write(" placeholder=\"").write(m_placeholder).write('"');
		String quicksearch = v.getState().getQuicksearch();
		if (quicksearch != null)
			w.write(" value=\"").write(quicksearch).write('"');
		w.write(" autocomplete=\"off\"> ")
			.addStyle("margin-left:10px;vertical-align:middle")
				.ui.buttonOnClick(Text.search, "quicksearch()");
		if (quicksearch != null && quicksearch.length() > 0)
			w.space().aOnClick("View All", "net.post(context+'/ViewStates/" + v.getViewDef().getName() + "/tsvectorsearch','value=',function(){net.replace(_.c(this))}.bind(this))");
		w.write("</div>");
		return true;
	}
}
