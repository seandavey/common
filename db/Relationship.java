package db;

import app.Request;
import app.Site;
import db.View.Mode;
import db.column.Column;
import db.column.LookupColumn;
import ui.Table;
import web.HTMLWriter;
import web.JS;

public class Relationship {
	public final RelationshipDef<?>	def;
	public final View				one;

	private View					m_many;

	//--------------------------------------------------------------------------

	public
	Relationship(RelationshipDef<?> def, View one) {
		this.def = def;
		this.one = one;
	}

	//--------------------------------------------------------------------------

	final boolean
	addOneColumns(Select query, Request r) {
		String column = def.getManyTableColumn();
		if (column != null) {
			if (column.indexOf(',') != -1) {
				StringBuilder buf = new StringBuilder();
				String[] columns = column.split(",");
				for (int j=0; j<columns.length; j++) {
					if (j > 0)
						buf.append("||' '||");
					buf.append(columns[j]);
				}
				column = buf.toString();
			}
			ViewDef many_view_def = Site.site.getViewDef(def.many_view_def_name, r.db);
			String many_from = many_view_def.getFrom();
			StringBuilder s = new StringBuilder();
			s.append("trim(', ' FROM (SELECT concat(");
			s.append(column);
			s.append("||', ') FROM (SELECT ");
			s.append(def.getManyTableColumn());
			s.append(" FROM ");
			s.append(many_from);
			if (def instanceof OneToMany)
				s.append(" WHERE " +
					one.getFrom() +
					".id=" +
					many_from +
					"." +
					one.getFrom() +
					"_id");
			else
				s.append(" LEFT JOIN " +
					((ManyToMany)def).getManyManyTable() +
					" ON(" +
					many_from +
					".id=" +
					((ManyToMany)def).getManyManyTable() +
					"." +
					many_from +
					"_id) WHERE " +
					one.getFrom() +
					".id=" +
					((ManyToMany)def).getManyManyTable() +
					"." +
					one.getFrom() +
					"_id");
			s.append(" ORDER BY ");
			String order_by = many_view_def.getDefaultOrderBy();
			s.append(order_by != null ? order_by : column);
			s.append(") AS t)) AS ");
			s.append(many_view_def.getRecordNamePlural().toLowerCase().replace(' ', '_'));
			query.addRelationColumn(s.toString());
			return true;
		}
		return false;
	}

	//--------------------------------------------------------------------------

	final String
	getAddButtonOnClick(boolean set_owner) {
		return "_.db.relationship_add(this,'" +
			def.many_view_def_name + "'," +
			JS.string(one.getViewDef().getName()) +
			(def instanceof OneToManyLink && ((OneToManyLink)def).getMode() == LookupColumn.Mode.TABLE ? ",true" : ",false") +
			(set_owner ? ",_.c(this))" : ",null)");
	}

	//--------------------------------------------------------------------------

	final View
	getMany(Request r) {
		if (m_many == null) {
			m_many = Site.site.newView(def.many_view_def_name, r);
			m_many.m_state.setFilter(null);
			m_many.setPrinterFriendly(one.isPrinterFriendly());
			m_many.m_relationship = this;
			if (def instanceof OneToMany)
				if (m_many.getColumn(one.getFrom() + "_id") == null)
					m_many.setColumn(new Column(one.getFrom() + "_id").setIsHidden(true));
		}
		return m_many;
	}

	//--------------------------------------------------------------------------

	final void
	writeManyTableRow(int one_id, Request r) {
		r.one_id = one_id;
		getMany(r);
		if (one_id != 0) {
			if (def instanceof ManyToMany) {
				String o = one.getFrom();
				String m = m_many.getFrom();
				if (r.db.hasColumn(m, o + "_id"))
					m_many.setWhere(m + "." + o + "_id=" + one_id + " AND " + m + "_id" + "=id");
				else
					m_many.setWhere(o + "_id=" + one_id + " AND " + m + "_id=" + m + ".id");
			} else
				m_many.setWhere(one.getFrom() + "_id=" + one_id);
			m_many.select();
		}

		boolean show_add_button = m_many.showAddButton();
		if (!show_add_button && (m_many.data == null || !m_many.data.isBeforeFirst()))
			return;

		HTMLWriter w = r.w;
		w.setAttribute("data-one_id", one.getMode() == Mode.ADD_FORM ? 0 : one.data != null ? one.data.getInt("id") : one.getState().getKeyValue())
			.setId(m_many.getViewDef().getName() + "_row")
			.addClass("mb-3 bg-light px-1 pt-1 table-responsive");
		int row = w.tagOpen("div");
		int rwc_id = 0;
		rwc_id = writeManyTableRowLabel(show_add_button, w);

		m_many.getViewDef().beforeList(one, r);
		m_many.m_num_rows = 0;
//		if (one.getMode() != Mode.ADD_FORM) {
			Table table = m_many.tableOpen();
			String[] columns = m_many.getColumnNamesFormTable();
			m_many.initTotals(columns);
			m_many.writeViewJSON();
			m_many.writeColumnHeadsRow(columns, false);
			while (m_many.next())
				m_many.writeRow(columns);
			table.tfoot();
			m_many.writeTotalsRow("Total");
			m_many.m_total_rows = m_many.m_num_rows;
			m_many.tableClose(table, columns);
//		} else {
//			w.ui.table().addDefaultClasses().open().close();
//			m_many.writeViewJSON();
//		}
		if (rwc_id != 0) {
			m_many.getPager().setRecordName(def.getRecordName());
			m_many.writeRowWindowControls(rwc_id);
		}
		w.tagsCloseTo(row);
	}

	//--------------------------------------------------------------------------

	private int
	writeManyTableRowLabel(boolean show_add_link, HTMLWriter w) {
		int rwc_id = 0;
		rwc_id = m_many.writeRowWindowControlsSpan();
		if (show_add_link && !m_many.isPrinterFriendly())
			w.addStyle("float:right")
				.ui.buttonOnClick(m_many.getViewDef().getAddButtonText(), m_many.getViewDef().getAddButtonOnClick(this, true));
		return rwc_id;
	}
}