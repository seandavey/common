package db.access;

import java.time.LocalDate;

import app.Person;
import app.Request;
import app.Stringify;
import db.Rows;
import db.Select;
import db.column.ColumnBase;
import db.column.LookupColumn;
import util.Time;

@Stringify
public class RecordOwnerAccessPolicy extends AccessPolicy {
	private boolean	m_can_delete_past_records = true;
	private boolean	m_can_edit_past_records = true;
	private String	m_column = "_owner_";

	//--------------------------------------------------------------------------

	@Override
	public void
	adjustQuery(Select query) {
		query.addColumns(m_column);
	}

	//--------------------------------------------------------------------------

	@Override
	public boolean
	canDeleteRow(String from, int id, Request r) {
		return r.userIsAdmin() || m_delete && userOwnsRecord(from, id, r) && (m_can_delete_past_records || !recordIsPast(from, id, r));
	}

	//--------------------------------------------------------------------------

	@Override
	public boolean
	canUpdateRow(String from, int id, Request r) {
		return r.userIsAdmin() || m_delete && userOwnsRecord(from, id, r) && (m_can_edit_past_records || !recordIsPast(from, id, r));
	}

	//--------------------------------------------------------------------------

	public final ColumnBase<?>
	getColumn(String display_name) {
		return new LookupColumn(m_column, "people", "first,last").setDefaultToUserId().setDisplayName(display_name).setIsReadOnly(true);
	}

    //--------------------------------------------------------------------------

	private boolean
	recordIsPast(LocalDate start, LocalDate end) {
		LocalDate now = Time.newDate();
		if (end != null)
			return now.isAfter(end);
		if (start != null)
			return now.isAfter(start);
		return false;
	}

    //--------------------------------------------------------------------------

	private boolean
	recordIsPast(String from, int id, Request r) {
		Object[] row = r.db.readRowObjects(new Select("date,end_date").from(from).whereIdEquals(id));
		return recordIsPast((LocalDate)row[0], (LocalDate)row[1]);
	}

    //--------------------------------------------------------------------------

	public final RecordOwnerAccessPolicy
	setCanDeletePastRecords(boolean can_delete_past_records) {
		m_can_delete_past_records = can_delete_past_records;
		return this;
	}

    //--------------------------------------------------------------------------

	public final RecordOwnerAccessPolicy
	setCanEditPastRecords(boolean can_edit_past_records) {
		m_can_edit_past_records = can_edit_past_records;
		return this;
	}

    //--------------------------------------------------------------------------

	public final RecordOwnerAccessPolicy
	setColumn(String column) {
		m_column = column;
		return this;
	}

	//--------------------------------------------------------------------------

	@Override
	public boolean
	showDeleteButtonForRow(Rows data, Request r) {
		return r.userIsAdmin() || m_delete && userOwnsRecord(data, r) && (m_can_delete_past_records || !recordIsPast(data.getDate("date"), data.getDate("end_date")));
	}

	//--------------------------------------------------------------------------

	@Override
	public boolean
	showEditButtonForRow(Rows data, Request r) {
		return r.userIsAdmin() || m_edit && userOwnsRecord(data, r) && (m_can_edit_past_records || !recordIsPast(data.getDate("date"), data.getDate("end_date")));
	}

    //--------------------------------------------------------------------------

	private boolean
	userOwnsRecord(int owner, Request r) {
		Person user = r.getUser();
		if (user == null)
			return false;
		return user.getId() == owner || owner == 0;
	}

    //--------------------------------------------------------------------------

	protected boolean
	userOwnsRecord(Rows data, Request r) {
		return userOwnsRecord(data.getInt(m_column), r);
	}

    //--------------------------------------------------------------------------

	protected boolean
	userOwnsRecord(String from, int id, Request r) {
		return userOwnsRecord(r.db.lookupInt(new Select(m_column).from(from).whereIdEquals(id), 0), r);
	}
}