package db;

import app.Request;

public interface DeleteHook {
	/**
	 * called after delete has been executed by database
	 * @param where
	 * @param request
	 */
	default void
	afterDelete(String where, Request r) {
	}

	/**
	 * called before delete is sent to database
	 * @param where
	 * @param request
	 * @return error string or null
	 */
	default String
	beforeDelete(String where, Request r) {
		return null;
	}
}
