package db.column;

import app.Request;
import app.Site;
import db.DBConnection;
import db.Filter;
import db.Form;
import db.QueryAdjustor;
import db.Rows;
import db.Select;
import db.SelectRenderer;
import db.View;
import db.access.AccessPolicy;
import web.JS;

public class LookupColumn extends ColumnBase<LookupColumn> implements ColumnValueRenderer {
	public enum Mode { SELECT, TABLE }
	private boolean			m_adjust_query;
	private boolean			m_allow_adding;
	private boolean			m_allow_editing;
//	private String			m_base_url;
	private final String	m_column;
	private final String[]	m_columns;
	private Mode			m_mode = Mode.SELECT;
	private boolean			m_natural_sort;
	protected String		m_one_column;
	private final String 	m_order_by;
	private boolean			m_show_form_link;
	private final String 	m_table;
	private final String	m_value_column;

	//--------------------------------------------------------------------------
	/**
	 * a column that has valid values looked up in another table
	 * @param name the name of the column
	 * @param table the table with valid values
	 * @param column column(s) to display in the popup of valid values, delimit multiple columns with commas
	 */

	public
	LookupColumn(String name, String table, String column) {
		this(name, table, column, new Select("*").from(table).orderBy(column));
	}

	//--------------------------------------------------------------------------

	public
	LookupColumn(String name, String table, String column, String where, String order_by) {
		this(name, table, column, new Select("*").from(table).where(where).orderBy(order_by));
	}

	//--------------------------------------------------------------------------

	public
	LookupColumn(String name, String table, String column, Select query) {
		super(name);
		m_table = table;
		m_column = column;
		m_columns = column.split(",");
		m_one_column = name;
		m_order_by = query.getOrderBy();
		// check if table includes a join
		int index = table.indexOf(' ');
		if (index == -1)
			m_value_column = table + ".id";
		else
			m_value_column = table.substring(0, index) + ".id";

		m_input_renderer = new SelectRenderer(query, column, "id");
		setValueRenderer(this, true);
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	adjustQuery(Select query, db.View.Mode mode) {
		if (m_adjust_query)
			query.addColumn("(SELECT " + m_column + " FROM " + m_table + " WHERE " + m_value_column + "=" + m_name + ") AS " + getDisplayName(false));
	}

	//--------------------------------------------------------------------------

	public final String
	getOrderBy(String from, boolean descending, boolean nulls_first) {
		String[] order_by_columns = m_order_by.split(",");
		StringBuilder s = new StringBuilder();

		for (String order_by : order_by_columns) {
			if (s.length() > 0)
				s.append(',');
			s.append("(SELECT ")
				.append(m_natural_sort ? "naturalsort(" : "LOWER(")
				.append(order_by.endsWith(" DESC") ? order_by.subSequence(0, order_by.length() - 5) : order_by)
				.append(") FROM ")
				.append(m_table)
				.append(" WHERE ")
				.append(from)
				.append('.')
				.append(m_one_column)
				.append('=')
				.append(m_value_column)
				.append(')');
			if (nulls_first)
				s.append(" NULLS FIRST");
			if (descending)
				s.append(" DESC");
		}
		return s.toString();
	}

	//--------------------------------------------------------------------------

	public final boolean
	hasOptions(DBConnection db) {
		if (m_input_renderer == null)
			return true;
		return ((SelectRenderer)m_input_renderer).hasOptions(db);
	}

	//--------------------------------------------------------------------------

	private String
	lookupValue(int id, Request r) {
		if (id <= 0)
			return null;
		Rows rows = new Rows(new Select(m_column).from(m_table).whereEquals(m_value_column, id), r.db);
		StringBuilder s = new StringBuilder();
		while (rows.next())
			for (String column : m_columns) {
				String value = rows.getString(column);
				if (value != null) {
					if (s.length() > 0)
						s.append(" ");
					s.append(value);
				}
			}
		rows.close();
		return s.toString();
	}

	//--------------------------------------------------------------------------

	public final LookupColumn
	setAdjustQuery(boolean adjust_query) {
		m_adjust_query = adjust_query;
		return this;
	}

	//--------------------------------------------------------------------------

	public final LookupColumn
	setAllowAdding() {
		m_allow_adding = true;
		((SelectRenderer)m_input_renderer).setInline(true);
		return this;
	}

	//--------------------------------------------------------------------------

	public final LookupColumn
	setAllowEditing() {
		m_allow_editing = true;
		return this;
	}

	//--------------------------------------------------------------------------

	public final LookupColumn
	setAllowNoSelection(boolean allow_no_selection) {
		((SelectRenderer)m_input_renderer).setAllowNoSelection(allow_no_selection);
		return this;
	}

	//--------------------------------------------------------------------------

//	public LookupColumn
//	setBaseURL(String base_url) {
//		m_base_url = base_url;
//		return this;
//	}

	//--------------------------------------------------------------------------

	public final LookupColumn
	setFilter(Filter filter) {
		((SelectRenderer)m_input_renderer).setFilter(filter);
		return this;
	}

	//--------------------------------------------------------------------------

//	public LookupColumn
//	setFirstOption(String first_option_text, String first_option_value) {
//		((SelectRenderer)m_input_renderer).setFirstOption(new ui.Option(first_option_text, first_option_value));
//		return this;
//	}

    //--------------------------------------------------------------------------

	@Override
	public LookupColumn
	setIsRequired(boolean is_required) {
		((SelectRenderer)m_input_renderer).setIsRequired(is_required);
		return super.setIsRequired(is_required);
	}

    //--------------------------------------------------------------------------

	public final LookupColumn
	setMode(Mode mode) {
		m_mode = mode;
		if (mode != Mode.SELECT)
			m_input_renderer = null;
		return this;
	}

    //--------------------------------------------------------------------------

	public final LookupColumn
	setNaturalSort(boolean natural_sort) {
		m_natural_sort = natural_sort;
		return this;
	}

    //--------------------------------------------------------------------------

	@Override
	public LookupColumn
	setOnChange(String on_change) {
		((SelectRenderer)m_input_renderer).setOnChange(on_change);
		return this;
	}

	//--------------------------------------------------------------------------

	public final LookupColumn
	setOneColumn(String one_column) {
		m_one_column = one_column;
		return this;
	}

	//--------------------------------------------------------------------------

	public LookupColumn
	setQueryAdjustor(QueryAdjustor query_adjustor) {
		((SelectRenderer)m_input_renderer).setQueryAdjustor(query_adjustor);
		return this;
	}

	//--------------------------------------------------------------------------

	public final LookupColumn
	setShowFormLink(boolean show_form_link) {
		m_show_form_link = show_form_link;
		return this;
	}

	//--------------------------------------------------------------------------

	private void
	writeEditButton(View v, Request r) {
		AccessPolicy access_policy = Site.site.getViewDef(m_table, r.db).getAccessPolicy();
		if (access_policy != null && !access_policy.showEditButtons(v.data, r))
			return;
		StringBuilder options = new StringBuilder("column:'").append(m_name).append("',db_view_def:'").append(v.getViewDef().getName()).append('\'');
		if (v.getMode() == View.Mode.EDIT_FORM)
			options.append(",db_key_value:").append(v.data.getString("id"));
		String title = "Edit " + Site.site.getViewDef(m_table, r.db).getRecordNamePlural();
		r.w.addStyle("position:absolute;right:0;top:0")
			.setAttribute("title", title)
			.ui.buttonIconOnClick("pencil", "new Dialog({title:" + JS.string(title) + ",url:context+'/Views/" + m_table + "/component'," + options.toString() + "})");
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	writeInput(View v, Form f, boolean inline, Request r) {
		if (m_mode == Mode.TABLE) {
			r.w.setId("select_list_hidden")
				.hiddenInput("many_id", null);
			Site.site.newView(m_table, r).setMode(View.Mode.SELECT_LIST).writeComponent();
			return;
		}
		m_input_renderer.writeInput(v, f, this, inline, r);
		if (m_allow_adding) {
			String title = JS.string("Add " + Site.site.getViewDef(m_table, r.db).getRecordName());
			r.w.nbsp().
				ui.buttonIconOnClick("plus", "_.table.dialog_add(this,null,context+'/Views/" + m_table + "/component?db_mode=ADD_FORM'," + title + ")");
		} else if (m_allow_editing)
			writeEditButton(v, r);
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	writeValue(View v, ColumnBase<?> column, Request r) {
		int id = 0;
		if (v.getMode() == View.Mode.ADD_FORM) {
			String default_value = column.getDefaultValue(v.getState(), r);
			if (default_value != null)
				id = Integer.parseInt(default_value);
		} else if (m_adjust_query) {
			r.w.write(v.data.getString(getDisplayName(false)));
			return;
		} else
			id = v.data.getInt(m_one_column);
		if (id == 0)
			return;
		String value = lookupValue(id, r);
		if (value != null)
			if (m_show_form_link) {
				r.w.setAttribute("title", id);
				r.w.aOnClick(value, "new Dialog({url:'" + Site.context + "/Views/" + m_table + "/component?db_mode=" + View.Mode.READ_ONLY_FORM + "&db_key_value=" + id + "'})");
			} //else if (m_base_url != null)
				//request.writer.a(value, m_base_url + id);
			else
				r.w.write(value);
	}
}