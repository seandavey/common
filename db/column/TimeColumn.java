package db.column;

import java.time.LocalDate;
import java.time.LocalTime;

import app.Request;
import db.Form;
import db.NameValuePairs;
import db.View;
import db.ViewDef;
import util.Time;

public class TimeColumn extends ColumnBase<TimeColumn> {
	private boolean m_adjust_meridian;
	private boolean m_check_order = true;
	private String	m_date_column;
	private int		m_resolution = 5;

	// --------------------------------------------------------------------------

	public
	TimeColumn(String name) {
		super(name);
	}

	// --------------------------------------------------------------------------

	public TimeColumn
	setAdjustMeridian(boolean adjust_meridian) {
		m_adjust_meridian = adjust_meridian;
		return this;
	}

	// --------------------------------------------------------------------------

	public TimeColumn
	setCheckOrder(boolean check_order) {
		m_check_order = check_order;
		return this;
	}

	// --------------------------------------------------------------------------

	public TimeColumn
	setDateColumn(String date_column) {
		m_date_column = date_column;
		return this;
	}

	// --------------------------------------------------------------------------

	public TimeColumn
	setResolution(int resolution) {
		m_resolution = resolution;
		return this;
	}

	//--------------------------------------------------------------------------
	// id != 0 on update, 0 on insert

	@Override
	public void
	setValue(String value, NameValuePairs nvp, ViewDef view_def, int id, Request r) {
		if (value != null && value.length() > 0 &&  m_date_column != null) {
			LocalDate date = LocalDate.parse(r.getParameter(m_date_column));
			LocalTime time = Time.formatter.parseTime(value);
			value = Time.toSiteZone(time, date, r).toString();
		}
		nvp.set(m_name, value);
	}

	// --------------------------------------------------------------------------

	@Override
	public void
	writeInput(View v, Form f, boolean inline, Request r) {
		LocalTime time = getValueTime(m_name, v, r);
		if (m_date_column != null)
			time = Time.toUserZone(time, getValueDate(m_date_column, v, r), r);
		r.w.timeInput(m_name, time, m_resolution, m_check_order, m_adjust_meridian, inline);
	}
}
