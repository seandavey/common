package db.column;

import app.Request;
import db.Form;
import db.View;

public interface ColumnInputRenderer {
	// return false if nothing was written
	public void
	writeInput(View v, Form f, ColumnBase<?> column, boolean inline, Request r);
}
