package db.column;

import app.Person;
import app.Request;
import app.Site;
import db.Form;
import db.View;

public class PersonColumn extends LookupColumn {
	private final boolean m_keep_space;

	//--------------------------------------------------------------------------

	public
	PersonColumn(String name, boolean keep_space) {
		super(name, "people", "first,last","active","LOWER(first),LOWER(last)");
		m_keep_space = keep_space;
	}

	// --------------------------------------------------------------------------
	// normally other column types don't override this method

	@Override
	public void
	writeInput(View v, Form f, boolean inline, Request r) {
		if (r.userIsAdmin()) {
			m_input_renderer.writeInput(v, f, this, inline, r);
			return;
		}
		super.writeInput(v, f, inline, r);
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	writeValue(View v, ColumnBase<?> column, Request r) {
		String id = v.getMode() == View.Mode.ADD_FORM ? column.getDefaultValue(v.getState(), r) : v.data.getString(m_one_column);
		if (id == null || id.length() == 0)
			return;
		Person person = Site.site.newPerson(Integer.parseInt(id), r.db);
		if (person != null)
			person.write(m_keep_space, 32, v.isPrinterFriendly(), r);
	}
}
