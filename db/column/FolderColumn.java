package db.column;

import java.util.Map;

import app.Request;
import db.Form;
import db.Rows;
import db.RowsSelect;
import db.SQL;
import db.Select;
import db.View;
import db.View.Mode;

public class FolderColumn extends ColumnBase<FolderColumn> {
	private String 			m_and_where_column;
	private final String	m_table;

	//--------------------------------------------------------------------------

	public
	FolderColumn(String table) {
		super("folder");
		m_table = table;
	}

	//--------------------------------------------------------------------------

	@Override
	public String
	getDisplayName(boolean use_non_breaking_spaces) {
		return "in folder";
	}

	//--------------------------------------------------------------------------

	private RowsSelect
	newRowsSelect(View.Mode mode, Rows data, Request r) {
		Select query = new Select("id,title").from(m_table).where("kind='f'").orderBy("title");
		if (m_and_where_column != null)
			query.andWhere(m_and_where_column + "=" + r.getSessionAttribute(m_and_where_column));
		if (mode != Mode.ADD_FORM)
			query.andWhere("title != " + SQL.string(data.getString("title")));
		return new RowsSelect(m_name, query, "title", "id", r);
	}

	//--------------------------------------------------------------------------

	public final FolderColumn
	setAndWhereColumn(String and_where_column) {
		m_and_where_column = and_where_column;
		return this;
	}
	// --------------------------------------------------------------------------

	@Override
	public boolean
	showOnForm(View.Mode mode, Rows data, Request r) {
		if (!super.showOnForm(mode, data, r))
			return false;
		RowsSelect s = newRowsSelect(mode, data, r);
		return s.size() > 0;
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	writeInput(View v, Form f, boolean inline, Request r) {
		RowsSelect s = newRowsSelect(v.getMode(), v.data, r);
		if (s.size() > 0) {
			s.setAllowNoSelection(true);
			if (v.getMode() != Mode.ADD_FORM)
				s.setSelectedOption(null, v.data.getString("folder"));
			s.write(r.w);
		} else
			r.w.write("(there aren't any folders to put this in)");
//			r.w.addStyle("margin-left:10px")
//				.ui.buttonOnClick(Text.add, "_.table.dialog_add(this," + JSON.string(m_table) + ",context+'/Views/'+" + JSON.string(v.getViewDef().getName()) + "+'/component?db_mode=ADD_FORM&kind=folder','Create Folder')");
	}

	//--------------------------------------------------------------------------

	@Override
	public boolean
	writeValue(View v, Map<String,Object> data, Request r) {
		int id = v.data.getInt("folder");
		if (id == 0)
			return false;
		String folder = r.db.lookupString(new Select("title").from(v.getViewDef().getFrom()).whereIdEquals(id));
		r.w.write(folder);
		return true;
	}
}
