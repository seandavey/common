package db.column;

import app.Request;
import app.Roles;
import app.Site;
import app.Site.Message;
import app.Subscriber;
import db.DBConnection;
import db.Form;
import db.View;

public class RolesColumn extends OptionsColumn implements Subscriber {
	public
	RolesColumn(String name) {
		super(name);
		setAllowNoSelection(true);
		Site.site.subscribe("_roles_", this);
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	publish(Object object, Message message, Object data, DBConnection db) {
		m_texts = null;
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	writeInput(View v, Form f, boolean inline, Request r) {
		if (m_texts == null)
			m_texts = Roles.getRoleNames();
		super.writeInput(v, f, inline, r);
	}
}
