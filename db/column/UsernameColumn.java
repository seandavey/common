package db.column;


public class UsernameColumn extends Column {
	public
	UsernameColumn(String name) {
		super(name);
		setIsRequired(true);
		setIsUnique(true);
	}
}
