package db.column;

import java.time.LocalDate;
import java.util.Map;

import app.Request;
import db.Form;
import db.NameValuePairs;
import db.View;
import db.View.Mode;
import db.ViewDef;
import util.Array;
import util.Time;
import web.JS;

public class BirthdayColumn extends ColumnBase<BirthdayColumn> {
	private final boolean m_birthday_is_date;

	// --------------------------------------------------------------------------

	public
	BirthdayColumn(String name, boolean birthday_is_date) {
		super(name);
		m_birthday_is_date = birthday_is_date;
	}

	// --------------------------------------------------------------------------

	@Override
	public String
	getOrderBy() {
		return "EXTRACT(DOY FROM " + m_name + ")";
	}

	// --------------------------------------------------------------------------

	@Override
	public String
	getOrderByDesc() {
		return "EXTRACT(DOY FROM " + m_name + ") DESC";
	}

	// --------------------------------------------------------------------------

	@Override
	public void
	setValue(String value, NameValuePairs nvp, ViewDef view_def, int id, Request r) {
		if (!m_birthday_is_date) {
			if (value != null && value.length() > 0) {
				int index = value.indexOf(' ');
				if (index != -1) {
					int month = Array.indexOf(Time.formatter.getMonthNamesLong(), value.substring(0, index));
					if (month >= 0 && month < 12) {
						nvp.set(m_name, "1900-" + (month + 1) + "-" + value.substring(index + 1));
						return;
					}
				}
			}
			nvp.set(m_name, (String)null);
		} else
			super.setValue(value, nvp, view_def, id, r);
	}

	// --------------------------------------------------------------------------

	@Override
	public String
	validate(ViewDef view_def, String value, int id, Request r) {
		if (value == null || value.length() == 0)
			return null;
		if (!m_birthday_is_date) {
			String[] tokens = value.split(" ");
			if (tokens.length == 2)
				try {
					Integer.parseInt(tokens[1]);
				} catch (NumberFormatException e) {
					return getDisplayName(false) + " value \"" + JS.escape(value) + "\" is not a valid birthday";
				}
		} else
			try {
				LocalDate.parse(value);
			} catch (Exception e) {
				return getDisplayName(false) + " value \"" + JS.escape(value) + "\" is not a valid date";
			}
		return null;
	}

	// --------------------------------------------------------------------------

	@Override
	public void
	writeInput(View v, Form f, boolean inline, Request r) {
		if (!m_birthday_is_date) {
			String month = null;
			String day = null;
			if (v.getMode() == Mode.EDIT_FORM) {
				LocalDate date = v.data.getDate(m_name);
				if (date != null) {
					month = Time.formatter.getMonthNameLong(date);
					day = Integer.toString(date.getDayOfMonth());
				}
			}
			r.w.hiddenInput(m_name, null)
				.setId(m_name + "1")
				.ui.select(null, Time.formatter.getMonthNamesLong(), month, "", true)
				.setId(m_name + "2")
				.nbsp().numberInput(null, "1", "31", null, day, true)
				.js("_.$('#" + m_name + "1').validate=function(){" +
				"this.form." + m_name + ".value=this.selectedIndex==0?'':this.options[this.selectedIndex].text+' '+_.$('#" + m_name + "2').value;" +
				"return true;}");
			return;
		}
		super.writeInput(v, f, inline, r);
	}

	//---------------------------------------------------------------------- ----

	@Override
	public boolean
	writeValue(View v, Map<String,Object> data, Request r) {
		LocalDate date = v.data.getDate(m_name);
		if (date == null)
			return false;
		if (!m_birthday_is_date) {
			r.w.write(Time.formatter.getMonthNameLong(date)).nbsp().write(date.getDayOfMonth());
			return true;
		}
		return super.writeValue(v, data, r);
	}
}
