package db.column;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.InputStream;
import java.nio.file.Path;
import java.util.Map;

import app.ImageMagick;
import app.Images;
import app.Request;
import db.Form;
import db.NameValuePairs;
import db.Select;
import db.View;
import db.View.Mode;
import db.ViewDef;
import jakarta.servlet.http.Part;
import web.HTMLWriter;

public class PictureColumn extends FileColumn {
	private int			m_image_size;
	private final int	m_max_side;
//	protected int		m_popup_height;
//	protected int		m_popup_width;
	private boolean		m_show_rotate_buttons;
	private boolean		m_size_is_max_side = true;
	private final int	m_thumb_size;
	private boolean		m_use_image_magick;

	//--------------------------------------------------------------------------

	public
	PictureColumn(String name, String table, String directory, int thumb_size, int max_side) {
		super(name, table, directory);
		m_thumb_size = thumb_size;
		m_max_side = max_side;
		m_kind = "picture";
		m_accept = "image/*";
		m_is_filterable = false;
		m_is_sortable = false;
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	beforeDelete(ViewDef view_def, String where, Request r) {
		if (!r.db.hasColumn(m_table, m_name))
			return;
		int id = Integer.parseInt(where.substring(where.indexOf("=") + 1));
		String file_name = r.db.lookupString(new Select(m_name).from(m_table).whereIdEquals(id));
		if (file_name == null || file_name.length() == 0)
			return;
		File f = getDirectory(id, r).resolve(file_name).toFile();
		if (f.exists())
			f.delete();
		if (m_thumb_size > 0) {
			f = getDirectory(id, r).resolve("thumbs").resolve(file_name).toFile();
			if (f.exists())
				f.delete();
		}
	}

	//--------------------------------------------------------------------------

	@Override
	public Path
	handleFile(Part part, InputStream is, String is_filename, ViewDef view_def, boolean insert, int id, Request r) {
		Path path = super.handleFile(part, is, is_filename, view_def, insert, id, r);
		if (path == null)
			return null;
		if (m_use_image_magick) {
			if (m_max_side > 0)
				ImageMagick.scale(path, path, m_max_side);
			if (m_thumb_size > 0)
				ImageMagick.makeThumb(path.getParent(), path.getFileName().toString(), m_thumb_size);
			if (r.db.hasColumn(m_table, "width")) {
				int[] size = ImageMagick.getSize(path);
				if (size != null) {
					NameValuePairs nvp = new NameValuePairs();
					nvp.set("width", size[0]);
					nvp.set("height", size[1]);
					view_def.update(id, nvp, r);
				}
			}
			Images.setTimestampToExifDate(path, view_def, id, r);
			return path;
		}
		BufferedImage image = null;
		if (m_max_side > 0) {
			try {
				image = Images.load(path.toString());
			} catch (OutOfMemoryError e) {
				System.out.println(path.toString());
				r.log(e);
				return null;
			}
			if (image != null) {
				int width = image.getWidth();
				int height = image.getHeight();
				if (width > m_max_side || height > m_max_side)
					image = Images.resize(image, m_max_side);
				Images.write(image, path);
			}
		}
		if (m_thumb_size > 0) {
			if (image == null)
				image = Images.load(path.toString());
			Images.makeThumb(image, path.subpath(0, path.getNameCount() - 1), path.getFileName().toString(), m_thumb_size, m_size_is_max_side);
		}
		if (r.db.hasColumn(m_table, "width")) {
			if (image == null)
				image = Images.load(path.toString());
			if (image != null) {
				NameValuePairs nvp = new NameValuePairs();
				nvp.set("width", image.getWidth());
				nvp.set("height", image.getHeight());
				view_def.update(id, nvp, r);
			}
		}
		return path;
	}

	//--------------------------------------------------------------------------

	public final PictureColumn
	setImageSize(int image_size) {
		m_image_size = image_size;
		return this;
	}

	//--------------------------------------------------------------------------

//	public PictureColumn
//	setPopupSize(int width, int height) {
//		m_popup_width = width;
//		m_popup_height = height;
//		return this;
//	}

	//--------------------------------------------------------------------------

	public final PictureColumn
	setShowRotateButtons(boolean show_rotate_buttons) {
		m_show_rotate_buttons = show_rotate_buttons;
		return this;
	}

	//--------------------------------------------------------------------------

	public final PictureColumn
	setSizeIsMaxSide(boolean size_is_max_side) {
		m_size_is_max_side = size_is_max_side;
		return this;
	}

	//--------------------------------------------------------------------------

	public final PictureColumn
	setUseImageMagick(boolean use_image_magick) {
		m_use_image_magick = use_image_magick;
		return this;
	}

	//--------------------------------------------------------------------------

	@Override
	protected void
	writeEditLinks(View v, HTMLWriter w) {
		super.writeEditLinks(v, w);
		if (m_show_rotate_buttons)
			w.addStyle("cursor:pointer")
				.setAttribute("onclick", "Img.rotate(this,'counterclockwise','" + v.getViewDef().getFrom() + "','" + m_name + "'," + v.data.getInt("id") + "," + m_thumb_size + ")")
				.setAttribute("title", "rotate counterclockwise")
				.img("images", "gallery", "rotate_counterclockwise.png")
				.space()
				.addStyle("cursor:pointer")
				.setAttribute("onclick", "Img.rotate(this,'clockwise','" + v.getViewDef().getFrom() + "','" + m_name + "'," + v.data.getInt("id") + "," + m_thumb_size + ")")
				.setAttribute("title", "rotate clockwise")
				.img("images", "gallery", "rotate_clockwise.png");
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	writeInput(View v, Form f, boolean inline, Request r) {
		String filename = getValue(m_name, v, r);
		HTMLWriter w = r.w;
		if (v.getMode() == Mode.ADD_FORM || filename == null)
			w.fileInput(m_name, m_accept, m_multiple);
		else {
			int max_size = m_image_size == 0 ? 100 : m_image_size;
			w.addStyle("max-width:" + max_size + "px;max-height:" + max_size);
			w.img(getURLPath(v.data) + filename);
			writeEditLinks(v, w);
		}
	}

	//--------------------------------------------------------------------------

	@Override
	public boolean
	writeValue(View v, Map<String,Object> data, Request r) {
		String file_name = v.data.getString(m_name);
		if (file_name != null) {
			String url_path = getURLPath(v.data);
			HTMLWriter w = r.w;

			if (m_thumb_size == 0 || m_image_size > 0) {
				if (m_image_size > 0)
					w.addStyle("max-width:" + m_image_size + "px;max-height:" + m_image_size + "px;cursor:pointer")
						.setAttribute("onclick", "Img.show(this)");
				w.img(url_path + file_name);
			} else {
				w.aOpen(url_path + file_name);
				w.img(url_path + "thumbs/" + file_name)
					.tagClose();
			}
			return true;
		}
		return false;
	}
}
