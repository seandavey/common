package db.column;

public interface ColumnLabelRenderer {
	public String
	getLabel(ColumnBase<?> column);
}