package db;

public class Select implements Cloneable {
	private StringBuilder	m_columns = new StringBuilder();
	private boolean			m_distinct;
	private String			m_distinct_on;
	private StringBuilder	m_from = new StringBuilder();
	private String			m_group_by;
	private int				m_limit;
	private StringBuilder	m_order_by;
	private StringBuilder	m_where;

    //--------------------------------------------------------------------------

	public
	Select() {
	}

    //--------------------------------------------------------------------------

	public
	Select(String columns) {
		m_columns.append(columns);
	}

    //--------------------------------------------------------------------------

	public final Select
	addColumn(String column) {
		if (m_columns.length() > 0)
			m_columns.append(',');
		m_columns.append(column);
		return this;
	}

    //--------------------------------------------------------------------------

	public final Select
	addColumn(String column, String alias) {
		if (m_columns.length() > 0)
			m_columns.append(',');
		m_columns.append(column).append(" AS ").append(alias);
		return this;
	}

    //--------------------------------------------------------------------------

	public final Select
	addColumns(String columns) {
		if (columns.charAt(0) == '(' || m_columns.charAt(0) != '*') {
			m_columns.append(',');
			m_columns.append(columns);
		}
		return this;
	}

    //--------------------------------------------------------------------------

	public final Select
	addFrom(String from) {
		if (m_from.length() > 0)
			m_from.append(',');
		m_from.append(from);
		return this;
	}

    //--------------------------------------------------------------------------
	// doesn't add quotes, done by OrderBy.toQueryString() and passed in here

	public final Select
	addOrderBy(String order_by) {
		if (order_by == null || order_by.length() == 0)
			return this;
		String[] columns = order_by.split(",");
		for (String column : columns) {
			if (m_order_by == null)
				m_order_by = new StringBuilder();
			else
				m_order_by.append(',');
			int index = column.indexOf(" AS ");
			if (index != -1)
				m_order_by.append(column.substring(index + 4));
			else
				m_order_by.append(column);
		}
		return this;
	}

    //--------------------------------------------------------------------------

	public final Select
	addRelationColumn(String columns) {
		if (m_columns.charAt(0) == '*') {
			m_columns.insert(0, '.');
			m_columns.insert(0, (Object)m_from);
		} else if (m_distinct && m_columns.charAt(9) == '*') {
			m_columns.insert(9, '.');
			m_columns.insert(9, (Object)m_from);
		}
		m_columns.append(',');
		m_columns.append(columns);
		return this;
	}

    //--------------------------------------------------------------------------

	public final Select
	andEquals(String column, int id) {
		return andWhere(column + '=' + id);
	}

    //--------------------------------------------------------------------------

	public final Select
	andWhere(String where) {
		if (where == null)
			return this;
		if (m_where == null)
			m_where = new StringBuilder();
		else
			m_where.append(" AND ");
		m_where.append('(').append(where).append(')');
		return this;
	}

    //--------------------------------------------------------------------------

	@Override
	public Select
	clone() {
		Select s;
		try {
			s = (Select)super.clone();
		} catch (CloneNotSupportedException e) {
			throw new RuntimeException(e);
		}
		if (s.m_columns != null)
			s.m_columns = new StringBuilder(s.m_columns);
		if (s.m_from != null)
			s.m_from = new StringBuilder(s.m_from);
		if (s.m_order_by != null)
			s.m_order_by = new StringBuilder(s.m_order_by);
		if (s.m_where != null)
			s.m_where = new StringBuilder(s.m_where);
		return s;
	}

    //--------------------------------------------------------------------------

	public final Select
	distinct() {
		m_distinct = true;
		return this;
	}

    //--------------------------------------------------------------------------

	public final Select
	distinctOn(String distinct_on) {
		m_distinct = true;
		m_distinct_on = distinct_on;
		return this;
	}

    //--------------------------------------------------------------------------

	public final Select
	from(String from) {
		m_from.append(from);
		return this;
	}

    //--------------------------------------------------------------------------

	public final String
	getColumns() {
		return m_columns.toString();
	}

    //--------------------------------------------------------------------------

	public final String
	getFirstTable() {
		int comma = m_from.indexOf(",");
		int space = m_from.indexOf(" ");
		if (comma != -1) {
			if (space != -1)
				return m_from.substring(0, Math.min(comma, space));
			return m_from.substring(0, comma);
		}
		if (space != -1)
			return m_from.substring(0, space);
		return m_from.toString();
	}

    //--------------------------------------------------------------------------

	public final String
	getFrom() {
		return m_from.toString();
	}

    //--------------------------------------------------------------------------

	public final String
	getOrderBy() {
		return m_order_by == null ? null : m_order_by.toString();
	}

    //--------------------------------------------------------------------------

	public final String
	getWhere() {
		return m_where == null ? null : m_where.toString();
	}

    //--------------------------------------------------------------------------

	public final Select
	groupBy(String group_by) {
		m_group_by = group_by;
		return this;
	}

    //--------------------------------------------------------------------------

	public final Select
	insertColumns(String columns) {
		if (m_columns.length() > 0)
			m_columns.insert(0, ',');
		m_columns.insert(0, columns);
		return this;
	}

    //--------------------------------------------------------------------------
	// doesn't add quotes, done by OrderBy.toQueryString() and passed in here

	public final void
	insertOrderBy(String order_by) {
		if (order_by == null || order_by.length() == 0)
			return;
		if (m_order_by == null)
			m_order_by = new StringBuilder();
		else
			m_order_by.insert(0, ',');
		m_order_by.insert(0, order_by);
	}

    //--------------------------------------------------------------------------
	// JOIN many_table ON one_table.id=many_table.one_table_id

	public final Select
	join(String one_table, String many_table) {
		m_from.append(" JOIN ").append(many_table).append(" ON ").append(one_table).append(".id=").append(many_table).append('.').append(one_table).append("_id");
		return this;
	}

    //--------------------------------------------------------------------------

	public final Select
	join(String one_table, String many_table, String many_to_many_table) {
		join(one_table, many_to_many_table);
		m_from.append(" JOIN ").append(many_table).append(" ON ").append(many_table).append(".id=").append(many_to_many_table).append('.').append(many_table).append("_id");
		return this;
	}

    //--------------------------------------------------------------------------

	public final Select
	joinOn(String many_table, String on) {
		m_from.append(" JOIN ").append(many_table).append(" ON ").append(on);
		return this;
	}

    //--------------------------------------------------------------------------

	public final Select
	joinOne(String one_table, String many_table) {
		m_from.append(" JOIN ").append(many_table).append(" ON ").append(one_table).append(".").append(many_table).append("_id=").append(many_table).append(".id");
		return this;
	}

    //--------------------------------------------------------------------------

	public final Select
	leftJoin(String one_table, String many_table) {
		m_from.append(" LEFT");
		return join(one_table, many_table);
	}

    //--------------------------------------------------------------------------

	public final Select
	leftJoin(String one_table, String many_table, String many_to_many_table) {
		m_from.append(" LEFT");
		return join(one_table, many_table, many_to_many_table);
	}

    //--------------------------------------------------------------------------

	public final Select
	leftJoinOn(String many_table, String on) {
		m_from.append(" LEFT");
		return joinOn(many_table, on);
	}

    //--------------------------------------------------------------------------

	public final Select
	leftJoinOne(String one_table, String many_table) {
		m_from.append(" LEFT");
		return joinOne(one_table, many_table);
	}

    //--------------------------------------------------------------------------

	public final Select
	limit(int limit) {
		m_limit = limit;
		return this;
	}

    //--------------------------------------------------------------------------

	public final Select
	orderBy(String order_by) {
		if (order_by != null && order_by.length() > 0)
			m_order_by = new StringBuilder().append(order_by);
		return this;
	}

    //--------------------------------------------------------------------------

	public final Select
	orderByLower(String order_by) {
		if (order_by == null || order_by.length() == 0)
			return this;
		m_order_by = new StringBuilder();
		String[] columns = order_by.split(",");
		for (String column : columns) {
			if (m_order_by.length() > 0)
				m_order_by.append(",");
			m_order_by.append("LOWER(" + column + ")");
		}
		return this;
	}

    //--------------------------------------------------------------------------

	@Override
	public String
	toString() {
		StringBuilder sb = new StringBuilder("SELECT ");

		if (m_distinct) {
			sb.append("DISTINCT ");
			if (m_distinct_on != null) {
				sb.append("ON(");
				if (m_order_by != null) {
					String order_by = m_order_by.toString().replaceAll(" DESC", "");
					if (!order_by.equals(m_distinct_on))
						sb.append(order_by)
							.append(',');
				}
				sb.append(m_distinct_on);
				sb.append(')');
			}
		}
		sb.append(m_columns);
		if (m_from.length() > 0) {
			sb.append(" FROM ");
			sb.append(m_from);
		}
		if (m_where!= null && m_where.length() > 0) {
			sb.append(" WHERE ");
			sb.append(m_where);
		}
		if (m_group_by != null) {
			sb.append(" GROUP BY ");
			sb.append(m_group_by);
		}
		if (m_order_by != null) {
			sb.append(" ORDER BY ");
			sb.append(m_order_by);
		}
		if (m_limit > 0) {
			sb.append(" LIMIT ");
			sb.append(m_limit);
		}

		return sb.toString();
	}

    //--------------------------------------------------------------------------

	public final Select
	where(String where) {
		if (where != null && where.length() > 0)
			m_where = new StringBuilder().append(where);
		return this;
	}

    //--------------------------------------------------------------------------

	public final Select
	whereEquals(String column, int id) {
		m_where = new StringBuilder(column).append('=').append(id);
		return this;
	}

    //--------------------------------------------------------------------------

	public final Select
	whereEquals(String column, String value) {
		m_where = new StringBuilder(column).append("=").append(SQL.string(value));
		return this;
	}

    //--------------------------------------------------------------------------

	public final Select
	whereIdEquals(int id) {
		m_where = new StringBuilder("id=").append(id);
		return this;
	}

    //--------------------------------------------------------------------------

	public final Select
	whereIdEquals(String id) {
		return whereIdEquals(Integer.parseInt(id));
	}

    //--------------------------------------------------------------------------

	public final Select
	whereIsNull(String column) {
		m_where = new StringBuilder(column).append(" IS NULL");
		return this;
	}
}