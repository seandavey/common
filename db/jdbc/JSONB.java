package db.jdbc;

public class JSONB extends JDBCColumn {
	public
	JSONB(String name) {
		this.name = name;
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	appendCode(StringBuilder code) {
		code.append(".add(new JSONB(\"").append(name).append("\"))");
	}

	//--------------------------------------------------------------------------

	@Override
	public String
	getSQLType() {
		return "JSONB";
	}

	//--------------------------------------------------------------------------

	@Override
	public boolean
	isOther() {
		return true;
	}
}
