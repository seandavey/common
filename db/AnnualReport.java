package db;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.Map;
import java.util.TreeMap;

import app.Request;
import ui.InlineForm;
import ui.Table;
import util.Text;
import util.Time;
import web.HTMLWriter;
import web.JS;
import web.URLBuilder;

public class AnnualReport {
	public static class Sums {
		private final Object	m_id;
		private final double[]	m_sums = new double[12];

		// --------------------------------------------------------------------------

		public
		Sums(Object id) {
			m_id = id;
		}

		// --------------------------------------------------------------------------

		public
		Sums(Object id, double starting_balance) {
			m_id = id;
			m_sums[0] = starting_balance;
		}

		// --------------------------------------------------------------------------

		public void
		add(double amount, int period) {
			m_sums[period] += amount;
		}

		// --------------------------------------------------------------------------

		public void
		addSums(double sums[], int num_periods) {
			for (int i=0; i<num_periods; i++)
				sums[i] += m_sums[i];
		}

		//--------------------------------------------------------------------------

		public void
		writeRow(String name, int period, int year, AnnualReport report) {
			report.writeRow(name, m_id, false, m_sums, period, year);
		}
	}

	private String				m_detail_columns;
	private String				m_detail_filter;
	private String				m_detail_from;
	private String				m_detail_one_column;
	private boolean				m_detail_one_column_is_counter;
	private boolean				m_display_as_currency;
	private int					m_group_column_index;
	private String				m_group_label;
	private boolean				m_is_balance_report;
	private final String[]		m_item_columns;
	private final int			m_item_id_column_index;
	private boolean				m_item_is_double;
	private boolean				m_item_is_int;
	private final int			m_item_name_column_index;
	private final int			m_item_value_column_index;
	private final String		m_item_label;
	private Map<Object,Sums>	m_items;
	private boolean				m_join_with_dates_left;
	private String				m_join_with_dates_on;
	private String				m_join_with_dates_table;
	private String				m_param;
	private boolean				m_quarterly;
	private Request				m_r;
	private final Select		m_query;
	private boolean				m_show_controls;
	private Table				m_table;
	private boolean				m_total_last_detail_column = true;
	private String				m_url;
	private boolean				m_value_is_double = true;

	//--------------------------------------------------------------------------

	public
	AnnualReport(Select query, String item_label, int item_id_column_index, int item_name_column_index, int item_value_column_index) {
		m_query = query;
		m_item_label = item_label;
		m_item_id_column_index = item_id_column_index;
		m_item_name_column_index = item_name_column_index;
		m_item_value_column_index = item_value_column_index;
		m_item_columns = null;
	}

	//--------------------------------------------------------------------------

	public
	AnnualReport(Select query, String item_label, String[] item_columns) {
		m_query = query;
		m_item_label = item_label;
		m_item_columns = item_columns;
		m_item_id_column_index = 0;
		m_item_name_column_index = 0;
		m_item_value_column_index = 0;
	}

	//--------------------------------------------------------------------------

	protected void
	add(Rows rows, Map<Object,Sums> items, Map<String,Map<Object,Sums>> groups, int period) {
		Object item_name = rows.getString(m_item_name_column_index);
		if (item_name == null)
			return;
		if (m_group_column_index != 0) {
			String group_name = rows.getString(m_group_column_index);
			if (group_name == null)
				group_name = "";
			items = groups.get(group_name);
			if (items == null) {
				items = new TreeMap<>();
				groups.put(group_name, items);
			}
		}
		if (m_item_is_int)
			item_name = Integer.valueOf((String)item_name);
		else if (m_item_is_double)
			item_name = Double.valueOf((String)item_name);
		Sums item_sums = items.get(item_name);
		if (item_sums == null) {
			item_sums = new Sums(m_item_id_column_index == 0 ? item_name : rows.getString(m_item_id_column_index));
			items.put(item_name, item_sums);
		}
		item_sums.add(rows.getDouble(m_item_value_column_index), period);
	}

	//--------------------------------------------------------------------------

	public final AnnualReport
	initSums(Map<Object, Sums> items) {
		m_items = items;
		return this;
	}

	//--------------------------------------------------------------------------

	public final AnnualReport
	setDetail(String detail_columns, String detail_from, String detail_one_column) {
		m_detail_columns = detail_columns;
		m_detail_from = detail_from;
		m_detail_one_column = detail_one_column;
		return this;
	}

	//--------------------------------------------------------------------------

	public final AnnualReport
	setDetailFilter(String filter) {
		m_detail_filter = filter;
		return this;
	}

	//--------------------------------------------------------------------------

	public final AnnualReport
	setDetailOneColumnsIsCounter(boolean detail_one_column_is_counter) {
		m_detail_one_column_is_counter = detail_one_column_is_counter;
		return this;
	}

	//--------------------------------------------------------------------------

	public final AnnualReport
	setDisplayAsCurrency(boolean display_as_currency) {
		m_display_as_currency = display_as_currency;
		return this;
	}

	//--------------------------------------------------------------------------

	public final AnnualReport
	setGroup(String label, int group_column_index) {
		m_group_label = label;
		m_group_column_index = group_column_index;
		return this;
	}

	//--------------------------------------------------------------------------

	public final AnnualReport
	setIsBalanceReport(boolean is_balance_report) {
		m_is_balance_report = is_balance_report;
		return this;
	}

	//--------------------------------------------------------------------------

	public final AnnualReport
	setItemIsDouble(boolean item_is_double) {
		m_item_is_double = item_is_double;
		return this;
	}

	//--------------------------------------------------------------------------

	public final AnnualReport
	setItemIsInt(boolean item_is_int) {
		m_item_is_int = item_is_int;
		return this;
	}

	//--------------------------------------------------------------------------

	public final AnnualReport
	setJoinWithDates(String table, String on, boolean left) {
		m_join_with_dates_table = table;
		m_join_with_dates_on = on;
		m_join_with_dates_left = left;
		return this;
	}

	//--------------------------------------------------------------------------

	public final AnnualReport
	setParam(String param) {
		m_param = param;
		return this;
	}

	//--------------------------------------------------------------------------

	public final AnnualReport
	setQuarterly(boolean quarterly) {
		m_quarterly = quarterly;
		return this;
	}

	//--------------------------------------------------------------------------

	public final AnnualReport
	setShowControls(boolean show_controls) {
		m_show_controls = show_controls;
		return this;
	}

	//--------------------------------------------------------------------------

	public final AnnualReport
	setTotalLastDetailColumn(boolean total_last_detail_column) {
		m_total_last_detail_column = total_last_detail_column;
		return this;
	}

	//--------------------------------------------------------------------------

	public final AnnualReport
	setUrl(String url) {
		m_url = url;
		return this;
	}

	//--------------------------------------------------------------------------

	public final AnnualReport
	setValueIsDouble(boolean value_is_double) {
		m_value_is_double = value_is_double;
		return this;
	}

	//--------------------------------------------------------------------------

	public final double
	write(String title, Request r) {
		m_r = r;
		String detail_id = r.getParameter("detail_id");
		if (detail_id != null) {
			writeDetail(detail_id);
			return 0;
		}

		HTMLWriter w = r.w;
		int y = r.getInt("year", 0);
		int year = y != 0 ? y : Time.newDate().getYear();
		if (y == 0 || !m_show_controls) {
			w.write("<div class=\"annual_report\">");
			if (title != null)
				w.h3(title);
			if (m_show_controls)
				writeControls(year);
			w.write("<div id=\"report\">");
		}

		int num_periods;
		int period_size;
		String q_or_m = r.getParameter("period");
		if (q_or_m == null)
			q_or_m = m_quarterly ? "quarterly" : "monthly";
		if (q_or_m.equals("monthly")) {
			num_periods = 12;
			period_size = 1;
		} else {
			num_periods = 4;
			period_size = 3;
		}
		LocalDate[] periods = new LocalDate[num_periods - 1];
		for (int i=0,month=period_size; i<num_periods-1; i++,month+=period_size)
			periods[i] = LocalDate.of(year, month + 1, 1).minusDays(1);

		Map<String,Map<Object,Sums>> groups = null;
		if (m_group_column_index != 0)
			groups = new TreeMap<>();
		else if (m_items == null)
			m_items = new TreeMap<>();
		int period = 0;
		if (m_join_with_dates_table != null) {
			if (m_join_with_dates_left)
				m_query.leftJoinOn(m_join_with_dates_table, m_join_with_dates_on + " AND date>='" + year + "-01-01' AND date<'" + (year + 1) + "-01-01'");
			else
				m_query.joinOn(m_join_with_dates_table, m_join_with_dates_on + " AND date>='" + year + "-01-01' AND date<'" + (year + 1) + "-01-01'");
		} else {
			m_query.andWhere("date>='" + year + "-01-01'");
			m_query.andWhere("date<'" + (year + 1) + "-01-01'");
		}
		try (Rows rows = new Rows(m_query.orderBy("date"), r.db)) {
			while (rows.next()) {
				LocalDate date = rows.getDate("date");
				if (date != null)
					while (period < num_periods - 1 && date.compareTo(periods[period]) > 0)
						++period;
				if (m_item_columns != null)
					for (String c : m_item_columns) {
						Sums item_sums = m_items.get(c);
						if (item_sums == null) {
							item_sums = new Sums(c);
							m_items.put(c, item_sums);
						}
						item_sums.add(rows.getDouble(c), period);
					}
				else
					add(rows, m_items, groups, period);
			}
		}

		m_table = w.ui.table().addClass("table-bordered").addDefaultClasses().open();
		m_table.th(m_item_label);
		for (int p=0; p<=period; p++) {
			m_table.th();
			if (period_size == 3) {
				w.write('Q');
				w.write(p + 1);
			} else
				w.write(Time.formatter.getMonthNameShort(p + 1));
		}
		if (period > 0 && !m_is_balance_report)
			m_table.th("Total");

		double totals[] = new double[num_periods];
		if (m_group_column_index != 0)
			for (String group : groups.keySet()) {
				m_table.tr();
				w.addStyle("background:#eee");
				m_table.td(period > 0 ? period + num_periods : num_periods - 1, "<b>" + (group.length() > 0 ? group : "(no group)") + "</b>");
				Map<Object,Sums> group_items = groups.get(group);
				double sums[] = new double[num_periods];
				for (Object item_name : group_items.keySet()) {
					Sums item_sums = group_items.get(item_name);
					item_sums.writeRow((String)item_name, period, year, this);
					item_sums.addSums(sums, num_periods);
					item_sums.addSums(totals, num_periods);
				}
				if (group_items.size() > 1 && Arrays.stream(sums).sum() != 0)
					writeRow(m_group_label + " total", null, true, sums, period, year);
			}
		else
			for (Object item_name : m_items.keySet()) {
				Sums item_sums = m_items.get(item_name);
				item_sums.writeRow(item_name.toString(), period, year, this);
				item_sums.addSums(totals, num_periods);
			}
		double total = writeRow("<b>Total</b>", null, true, totals, period, year);
		m_table.close();
		if (y == 0)
			w.write("</div></div>");
		return total;
	}

	//--------------------------------------------------------------------------

	private void
	writeControls(int year) {
		HTMLWriter w = m_r.w;
		StringBuilder js = new StringBuilder("""
			function replace_report(){
				var s=_.$('#year_select')
				net.replace('#report','""")
			.append(m_r.request.getRequestURI())
			.append("?year='+s.options[s.selectedIndex].value+'&period='+(_.$('#period0').checked?'monthly':'quarterly')");
		if (m_param != null)
			js.append("+'&").append(m_param).append('=').append(m_r.getParameter(m_param)).append("'");
		js.append(");}");
		w.js(js.toString())
			.write("<div style=\"margin-bottom:10px;\">")
			.setAttribute("onchange", "replace_report()");
		InlineForm f = w.ui.inlineForm(null).open().formGroup();
		w.setId("year_select")
			.ui.selectOpen(null, true);
		int this_year = Time.newDate().getYear();
		for (int i=m_r.db.lookupInt("extract(year from MIN(date))", "transactions", null, this_year); i<=this_year+1; i++) {
			w.write("<option");
			if (i == year)
				w.write(" selected=\"selected\"");
			w.write(">").write(i).write("</option>");
		}
		w.tagClose();
		f.formGroup();
		String[] options = new String[] {"monthly", "quarterly"};
		w.ui.radioButtons("period", options, options, m_quarterly ? "quarterly" : "monthly");
		w.addStyle("float:right")
			.setAttribute("title", "open print view in new tab")
			.ui.buttonIconOnClick("printer", "_.open_print_window('div.annual_report')");
		f.close();
		w.write("</div>");
	}

	//--------------------------------------------------------------------------

	private void
	writeDetail(String detail_id) {
		HTMLWriter w = m_r.w;
		int this_year = Time.newDate().getYear();
		String y = m_r.getParameter("year");
		int year = y != null ? Integer.parseInt(y) : this_year;
		Select select = new Select(m_detail_columns).from(m_detail_from);
		if (m_detail_one_column_is_counter)
			select.where(m_detail_one_column + ">0");
		else
			try {
				int id = Integer.parseInt(detail_id);
				select.whereEquals(m_detail_one_column, id);
			}
			catch (NumberFormatException e) {
				select.whereEquals(m_detail_one_column, detail_id);
			}
		select.andWhere("date>='" + year + "-01-01' AND date<'" + (year + 1) + "-01-01'").orderBy("date");
		if (m_detail_filter != null)
			select.andWhere(m_detail_filter);

		double total = 0;
		try (Rows rows = new Rows(select, m_r.db)) {
			int column_count = rows.getColumnCount();
			m_table = w.ui.table().addDefaultClasses().open();
			for (String column_name : rows.getColumnNames())
				m_table.th(Text.capitalize(column_name.replaceAll("_", " ")));
			while (rows.next()) {
				m_table.tr();
				for (int i=1; i<=column_count; i++)
					if (m_total_last_detail_column && i == column_count) {
						m_table.tdRight();
						if (m_value_is_double) {
							double value = rows.getDouble(i);
							total += value;
							if (m_display_as_currency)
								w.writeCurrency(value);
							else
								w.write(Text.two_digits.format(value));
						} else {
							int value = rows.getInt(i);
							total += value;
							w.write(value);
						}
					} else {
						m_table.td();
						w.write(rows.getString(i));
					}
			}
			if (m_total_last_detail_column) {
				m_table.tr();
				w.addStyle("text-align:right;");
				m_table.td(column_count - 1, "<b>Total</b>");
				writeValue(total);
			}
			m_table.close();
		}
	}

	//--------------------------------------------------------------------------

	final double
	writeRow(String name, Object detail_id, boolean align_right, double values[], int period, int year) {
		HTMLWriter w = m_r.w;
		m_table.tr();
		if (align_right)
			m_table.tdRight();
		else
			m_table.td();
		boolean link = m_detail_columns != null && detail_id != null && (!(detail_id instanceof Integer) || (Integer)detail_id != 0);
		if (link) {
			URLBuilder url = new URLBuilder(m_url != null ? m_url : m_r.request.getRequestURI()).set("detail_id", detail_id.toString()).set("year", year);
			if (m_param != null)
				url.set(m_param, m_r.getParameter(m_param));
			String on_click = "new Dialog({title:" + JS.string(name) + ",url:'" + url.toString() + "'}).open()";
			w.aOnClick(name, on_click);
		} else
			w.write(name);
		double total = 0;
		for (int p=0; p<=period; p++) {
			double v = values[p];
			total += v;
			if (m_is_balance_report)
				v = total;
			writeValue(v);
		}
		if (period > 0 && !m_is_balance_report)
			writeValue(total);
		return total;
	}

	//--------------------------------------------------------------------------
	
	private void
	writeValue(double v) {
		m_table.tdRight();
		if (v == 0.0)
			return;
		if (m_display_as_currency)
			m_r.w.writeCurrency(v);
		else if (m_value_is_double)
			m_r.w.write(Text.two_digits.format(v));
		else
			m_r.w.write((int)v);
	}
}
