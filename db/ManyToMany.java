package db;

import app.Request;
import app.Site;
import db.column.LookupColumn;

public class ManyToMany extends RelationshipDef<ManyToMany> {
	private boolean			m_allow_adding;
	private final String	m_many_lookup_display_column_name;
	private final String	m_many_many_table;
	private QueryAdjustor	m_query_adjustor;

	//--------------------------------------------------------------------------

	public
	ManyToMany(String many_view_def_name, String many_many_table, String many_lookup_display_column) {
		this.many_view_def_name = many_view_def_name;
		m_many_many_table = many_many_table;
		m_many_lookup_display_column_name = many_lookup_display_column;
	}

	//--------------------------------------------------------------------------

	public final LookupColumn
	getManyLookupColumn(Request r) {
		ViewDef many_view_def = Site.site.getViewDef(many_view_def_name, r.db);
		ViewDef many_many_view_def = Site.site.getViewDef(m_many_many_table, r.db);
		String column_name = many_view_def.getFrom() + "_id";
		LookupColumn lookup_column = (LookupColumn)many_many_view_def.getColumn(column_name);
		if (lookup_column == null) {
			lookup_column = new LookupColumn(column_name,
				many_view_def.getFrom(),
				m_many_lookup_display_column_name,
				new Select("id," + m_many_lookup_display_column_name)
					.distinctOn(m_many_lookup_display_column_name)
					.from(many_view_def.getFrom())
					.orderByLower(m_many_lookup_display_column_name));
			lookup_column.setDisplayName(many_view_def.getRecordName().toLowerCase());
			if (m_query_adjustor != null)
				lookup_column.setQueryAdjustor(m_query_adjustor);
			many_many_view_def.setColumn(lookup_column);
		}
		if (m_allow_adding)
			lookup_column.setAllowAdding();
		return lookup_column;
	}

	//--------------------------------------------------------------------------

	public final String
	getManyManyTable() {
		return m_many_many_table;
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	leftJoin(Select query, String one_table, Request r) {
		int index = m_many_many_table.indexOf(one_table);
		String many_table;
		if (index == 0)
			many_table = m_many_many_table.substring(one_table.length() + 1);
		else
			many_table = m_many_many_table.substring(0, m_many_many_table.length() - one_table.length() - 1);

		query.leftJoin(one_table, many_table, m_many_many_table);
	}

	//--------------------------------------------------------------------------

	public final ManyToMany
	setAllowAdding(boolean allow_adding) {
		m_allow_adding = allow_adding;
		return this;
	}

	//--------------------------------------------------------------------------

	public final ManyToMany
	setQueryAdjustor(QueryAdjustor query_adjustor) {
		m_query_adjustor = query_adjustor;
		return this;
	}
}
