package db;

public class Result {
	public String		error;
	public final int	id;

	//--------------------------------------------------------------------------

	public
	Result(int id) {
		this.id = id;
	}

	//--------------------------------------------------------------------------

	public
	Result(String error) {
		this.error = error;
		id = 0;
	}
}
