package db;

import app.Request;
import web.HTMLWriter;

public class Reorderable {
	private final String	m_id_column;
	private String			m_instructions = "(click and drag items in the list to change their order)";
	private final String	m_many_many_table;
	private String			m_on_update;

	// --------------------------------------------------------------------------

	public Reorderable() {
		m_id_column = null;
		m_many_many_table = null;
	}

	// --------------------------------------------------------------------------

	public Reorderable(String many_many_table, String id_column) {
		m_many_many_table = many_many_table;
		m_id_column = id_column;
	}

	// --------------------------------------------------------------------------

	public void
	reorder(Request r) {
		int id = r.getUser().getId();
		String[] ids = r.getString("ids", null).split(",");
		String from = r.getPathSegment(1);
		if (m_many_many_table != null)
			r.db.delete(m_many_many_table, "people_id", id, false);
		for (int i=0; i<ids.length; i++)
			if (m_many_many_table != null)
				r.db.insert(m_many_many_table, "_order_,people_id," + m_id_column, i + 1 + "," + id + "," + ids[i]);
			else
				r.db.update(from, "_order_=" + (i + 1), Integer.parseInt(ids[i]));
	}

	// --------------------------------------------------------------------------

	public Reorderable
	setInstructions(String instructions) {
		m_instructions = instructions;
		return this;
	}

	// --------------------------------------------------------------------------

	public Reorderable
	setOnUpdate(String on_update) {
		m_on_update = on_update;
		return this;
	}

	// --------------------------------------------------------------------------

	public void
	write(int reorderable_id, ViewDef view_def, HTMLWriter w) {
		StringBuilder js = new StringBuilder("JS.get('sortable',function(){new Sortable(_.$('#reorderable")
			.append(reorderable_id)
			.append("').querySelector('tbody'),{onUpdate:function(e){var ids=[];e.from.childNodes.forEach(function(i){ids.push(i.id.substring(2))});net.post(context+'/Views/")
			.append(view_def.getName())
			.append("/reorder'")
			.append(",'ids='+ids.join(',')");
		if (m_on_update != null)
			js.append(",function(){").append(m_on_update).append("}");
		js.append(");}})});");
		w.js(js.toString());
		w.ui.helpText(m_instructions);
	}
}
