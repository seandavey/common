package db;

import db.section.Tabs;
import ui.Dropdown;
import util.Text;
import web.HTMLWriter;

public class Pager {
	private String				m_record_name;
	private final ViewState		m_state;
	private final ViewDef		m_view_def;
	private boolean				m_write_dropdown = true;
	private final HTMLWriter	m_w;

	// --------------------------------------------------------------------------

	public
	Pager(ViewState state, ViewDef view_def, HTMLWriter w) {
		m_state = state;
		m_view_def = view_def;
		m_w = w;
	}

	// --------------------------------------------------------------------------

	private String
	getLabel(int num) {
		if (m_record_name != null)
			return Text.pluralize(m_record_name, num);
		return num == 1 ? m_view_def.getRecordName() : m_view_def.getRecordNamePlural();
	}

	// --------------------------------------------------------------------------

	public final Pager
	setRecordName(String record_name) {
		m_record_name = record_name;
		return this;
	}

	// --------------------------------------------------------------------------

	public final Pager
	setWriteDropdown(boolean write_dropdown) {
		m_write_dropdown = write_dropdown;
		return this;
	}

	// --------------------------------------------------------------------------

	private void
	writeNumRecords(int num_rows, int total_rows) {
		m_w.addClass("list_num_records")
			.addStyle("display:inline;font-weight:bold;position:relative")
			.tagOpen("div");
		if (m_view_def.getSections() != null && m_state.isSectionActive(0) && m_view_def.getSections().get(0).getClass() == Tabs.class || m_state.getRowWindowStart() == 0 && num_rows >= total_rows)
			m_w.write(num_rows)
				.nbsp()
				.write(getLabel(num_rows));
		else {
			m_w.write(getLabel(num_rows))
				.space();
			if (m_write_dropdown)
				m_w.addStyle("text-decoration:none")
					.aOnClickOpen("app.dropdown(this.parentNode.querySelector('ul'))");
			m_w.write(m_state.getRowWindowStart() + 1);
			if (num_rows > 1)
				m_w.write(" - ").write(m_state.getRowWindowStart() + num_rows);
			if (m_state.getRowWindowSize() > 0 && m_state.getRowWindowStart() + num_rows < total_rows)
				m_w.write(" (of ").write(total_rows).write(')');
			if (m_write_dropdown)
				m_w.tagClose();
		}
		if (m_write_dropdown && num_rows < total_rows) {
			Dropdown dropdown = m_w.ui.dropdown();
			if (m_view_def.getSections() == null || !m_state.isSectionActive(0) || m_view_def.getSections().get(0).getClass() != Tabs.class)
				dropdown.aOnClick("Set number of rows...", "var n=prompt('number of rows','" + m_state.getRowWindowSize() + "');if(n)net.post(context+'/ViewStates/" + m_view_def.getName() + "','row_window_size='+n,function(){net.replace(_.c(this))}.bind(this))");
			dropdown.aOnClick("View all rows", "net.post(context+'/ViewStates/" + m_view_def.getName() + "','row_window_size=all',function(){net.replace(_.c(this))}.bind(this))")
				.close();
		}
		m_w.tagClose();
	}

	// --------------------------------------------------------------------------

	public final void
	write(int num_rows, int total_rows) {
		int row_window_size = m_state.getRowWindowSize();
		if (m_state.getRowWindowStart() > 0) {
			m_w.ui.buttonIconOnClick("skip-start", "net.post(context+'/ViewStates/" + m_view_def.getName() + "','row_window_start=0',function(){net.replace(_.c(this))}.bind(this))");
			m_w.nbsp();
			if (m_state.getRowWindowStart() > row_window_size) {
				m_w.ui.buttonIconOnClick("skip-backward", "net.post(context+'/ViewStates/" + m_view_def.getName() + "','row_window_start=" + (m_state.getRowWindowStart() - row_window_size) + "',function(){net.replace(_.c(this))}.bind(this))");
				m_w.nbsp();
			}
		}
		writeNumRecords(num_rows, total_rows);
		if (row_window_size > 0 && m_state.getRowWindowStart() + num_rows < total_rows) {
			if (m_state.getRowWindowStart() + num_rows < total_rows - row_window_size) {
				m_w.nbsp();
				m_w.ui.buttonIconOnClick("skip-forward", "net.post(context+'/ViewStates/" + m_view_def.getName() + "','row_window_start=" + (m_state.getRowWindowStart() + row_window_size) + "',function(){net.replace(_.c(this))}.bind(this))");
			}
			m_w.nbsp();
			m_w.ui.buttonIconOnClick("skip-end", "net.post(context+'/ViewStates/" + m_view_def.getName() + "','row_window_start=" + Math.max(total_rows - row_window_size, m_state.getRowWindowStart() + row_window_size) + "',function(){net.replace(_.c(this))}.bind(this))");
		}
	}
}
