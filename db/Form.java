package db;

import java.util.ArrayList;
import java.util.List;

import app.Request;
import app.Site;
import db.View.Mode;
import db.column.ColumnBase;
import db.column.LookupColumn;
import db.jdbc.JDBCColumn;
import util.Text;
import web.URLBuilder;

public class Form extends ui.FormBase<Form> {
	protected int					m_id;
	private View.Mode				m_mode;
	protected String				m_one;
	protected int					m_one_id;
	private final boolean			m_printer_friendly;
	protected final Relationship	m_relationship;
	protected final Request			m_r;
	protected final View			m_v;
	protected final ViewDef			m_view_def;
	protected final ViewState		m_view_state;

	//--------------------------------------------------------------------------

	public
	Form(int id, View v, Request r) {
		super(v.getViewDef().getRecordName().replace(' ', '_'), new URLBuilder(Site.context).append("/Views").segment(v.getViewDef().getName()).toString(), r.w);
		m_id = id;
		m_v = v;
		m_relationship = v.getRelationship();
		m_r = r;
		m_mode = v.getMode();
		m_printer_friendly = v.isPrinterFriendly();
		m_view_def = v.getViewDef();
		m_view_state = v.getState();

		m_multipart = v.hasFileColumn();
		m_buttons_location = Location.HEAD;

		m_one = r.getParameter("one");
		if (m_one != null)
			m_one_id = r.getInt("one_id", 0);
		else if (m_relationship != null) {
			m_one = m_relationship.one.getViewDef().getName();
			m_one_id = m_relationship.one.getID();
		}
	}

	// --------------------------------------------------------------------------

	@Override
	public void
	close() {
//		if (m_row_mark != -1)
//			rowClose();
		if (m_mode != View.Mode.READ_ONLY_FORM && m_buttons_location == Location.BOTTOM) {
			m_w.write("<div style=\"text-align:center;\">");
			writeSubmit();
			m_w.write("</div>");
		}
		if (m_mode == View.Mode.EDIT_FORM && !m_printer_friendly)
			if (m_view_def.getActions() != null)
				for (Action action : m_view_def.getActions().values()) {
					m_w.write("<div style=\"padding-left:10px;padding-right:10px\">");
					action.write(m_view_def.getName(), m_r.getParameter("db_key_value"), m_r);
					m_w.write("</div>");
				}
		if (m_form_mark != -1)
			m_w.tagClose(); // form
		if (m_view_def.showPrintLinkForm() && (m_mode == View.Mode.EDIT_FORM || m_mode == View.Mode.READ_ONLY_FORM) && !m_printer_friendly) {
			m_w.write("<p>");
			URLBuilder url = m_view_def.getURL(m_r);
			url.segment("print")
				.set("db_key_value", m_view_state.getKeyValue())
				.set("db_mode", m_mode);
			m_w.setAttribute("title", "view form for printing")
				.ui.buttonIconOnClick("printer", "window.open('" + url.toString() + "','','')")
				.write("</p>");
		}
		if (m_view_def.m_after_form != null)
			m_w.write(m_view_def.m_after_form);
		if (m_view_def.m_form_hooks != null)
			for (FormHook form_hook : m_view_def.m_form_hooks)
				form_hook.afterForm(m_id, m_v.data, m_view_def, m_mode, m_printer_friendly, m_r);
		if (!m_printer_friendly && m_mode == View.Mode.EDIT_FORM && (m_view_def.getAccessPolicy() == null || m_view_def.getAccessPolicy().showDeleteButtonForRow(m_v.data, m_r)))
				m_w.js("Dialog.top().add_delete(function(){" + m_view_def.getFormDeleteOnClick(m_v.data, m_relationship, true, m_r) + "});");
	}

	// --------------------------------------------------------------------------

	@Override
	public Form
	open() {
		if (m_printer_friendly)
			m_table = m_w.ui.table().addDefaultClasses();
		m_action += m_mode == View.Mode.EDIT_FORM ? "/update" : m_relationship != null && m_relationship.def instanceof OneToManyLink ? "/link" : "/insert";
		m_w.setAttribute("data-mode", m_mode.toString());
		if (m_mode != Mode.ADD_FORM)
			m_w.setAttribute("data-id", m_id);
		super.open();
		if (m_mode == View.Mode.ADD_FORM && m_relationship != null && m_relationship.def instanceof ManyToMany) {
			String many_many_table = ((ManyToMany)m_relationship.def).getManyManyTable();
			m_v.writeViewJSON(Site.site.getViewDef(many_many_table, m_r.db), m_relationship);
			m_w.hiddenInput("db_view_def", many_many_table);
		} else
			m_v.writeViewJSON();
		if (m_one != null)
			m_w.hiddenInput("db_one_name", m_one);
		if (m_mode == View.Mode.ADD_FORM) {
			if (m_one != null && (m_relationship == null || !(m_relationship.def instanceof OneToManyLink))) {
				String one_column = Site.site.getViewDef(m_one, m_r.db).getFrom() + "_id";
				ColumnBase<?> column = m_view_def.getColumn(one_column);
				if (column == null)
					//					if (!m_view_def.addBeforeOneExists())
					m_w.hiddenInput(one_column, m_one_id);
//				else if (column.isHidden(m_r) || !column.userCanView(m_view, m_r))
//					if (!m_r.userIsAdministrator())
////						if (!m_view_def.addBeforeOneExists())
//							m_w.hiddenInput(one_column, m_relationship.one.getID());
			}
		} else if (m_mode == View.Mode.EDIT_FORM)
			m_w.hiddenInput("db_key_value", m_view_state.getKeyValue());
		return this;
	}

	//--------------------------------------------------------------------------

	public Form
	setMode(Mode mode) {
		m_mode = mode;
		return this;
	}

	//--------------------------------------------------------------------------

//	private boolean
//	showSubmitButton() {
//		if (m_mode == View.Mode.ADD_FORM && m_relationship != null && m_relationship.def instanceof OneToManyLink) {
//			LookupColumn column = ((OneToManyLink)m_relationship.def).getManyLookupColumn(m_r);
//			if (column.getMode() == LookupColumn.Mode.TABLE)
//				return false;
//		}
//		return true;
//	}

	//--------------------------------------------------------------------------

	public void
	write() {
		open();
		writeBody();
		close();
	}

	//--------------------------------------------------------------------------

	protected void
	writeBody() {
		if (m_mode == View.Mode.ADD_FORM && m_relationship != null && m_relationship.def instanceof OneToManyLink) {
			LookupColumn column = ((OneToManyLink)m_relationship.def).getManyLookupColumn(m_r);
			int mark = m_w.addClass("mb-3 position-relative").tagOpen("div");
			writeColumnInput(column.getName(), column, false);
			m_w.tagsCloseTo(mark);
			return;
		}
		if (m_mode == View.Mode.ADD_FORM  && m_relationship != null && m_relationship.def instanceof ManyToMany) {
			LookupColumn column = ((ManyToMany)m_relationship.def).getManyLookupColumn(m_r);
			if (column.hasOptions(m_r.db))
				writeColumnInput(column.getName(), column, false);
//				String[] column_names_form = m_r.db.getTable(((ManyToMany)m_relationship.def).getManyManyTable()).getColumnNames();
//				for (int i=2; i<column_names_form.length; i++)
//					writeColumnRow(column_names_form[i], m_v.getColumn(column_names_form[i]));
			else
				m_w.write("No " + m_view_def.getRecordNamePlural() + " available");
			return;
		}

		List<Relationship> relationships = m_v.getRelationships();
		List<Relationship> relationships_written = new ArrayList<>();
		for (String column_name : m_v.getColumnNamesForm()) {
			boolean is_relationship = false;
			for (Relationship relationship : relationships)
				if (column_name.equals(relationship.def.many_view_def_name)) {
					is_relationship = true;
					if (relationship.def.showOnForm(m_mode, m_v.data)) {
						String view_role = relationship.def.getViewRole();
						if (view_role == null || m_r.userHasRole(view_role))
							relationship.writeManyTableRow(m_id, m_r);
						relationships_written.add(relationship);
					}
					break;
				}
			if (!is_relationship) {
				if (m_mode == Mode.READ_ONLY_FORM) {
					String view_link_column = m_view_def.getViewLinkColumn();
					if (column_name.equals(view_link_column))
						continue;
				}
				writeColumnRow(column_name, m_v.getColumn(column_name));
			}
		}
		if (m_table != null)
			m_table.close();
		for (Relationship relationship : relationships)
			if (relationship.def.showOnForm(m_mode, m_v.data) && !relationships_written.contains(relationship)) {
				String view_role = relationship.def.getViewRole();
				if (view_role == null || m_r.userHasRole(view_role))
					relationship.writeManyTableRow(m_id, m_r);
			}
	}

	// --------------------------------------------------------------------------

	public final void
	writeColumnInput(String column_name, boolean inline) {
		writeColumnInput(column_name, m_v.getColumn(column_name), inline);
	}

	// --------------------------------------------------------------------------

	final void
	writeColumnInput(String column_name, ColumnBase<?> column, boolean inline) {
		if (m_printer_friendly || m_mode == View.Mode.READ_ONLY_FORM || column != null && column.isComputed()) {
			if (column != null)
				column.writeValue(m_v, null, m_r);
			else
				ColumnBase.writeValue(column_name, 0, m_v, m_r);
			return;
		}
		if (column != null) {
			column.writeInput(m_v, this, inline, m_printer_friendly, m_r);
			return;
		}
		ColumnBase.writeInput(column_name, m_v, inline, m_r);
	}

	// --------------------------------------------------------------------------

	public final void
	writeColumnRow(String column_name, ColumnBase<?> column) {
		if (m_mode == View.Mode.ADD_FORM) {
			if (column_name.equals("id") || column != null && column.isComputed())
				return;
			if (column != null) {
				if (!column.showOnForm(m_mode, m_v.data, m_r))
					return;
				if (!m_r.userIsAdministrator() && column.isHidden(m_r)) {
					String default_value = column.getDefaultValue(m_v, m_r);
					if (default_value != null)
						m_w.hiddenInput(column_name, default_value);
					return;
				}
			}
		} else if (m_mode == View.Mode.EDIT_FORM) {
			if (column != null && !column.showOnForm(m_mode, m_v.data, m_r))
				return;
		} else if (m_mode == View.Mode.READ_ONLY_FORM)
			if (column != null && (column.isHidden(m_r) || !column.showOnForm(m_mode, m_v.data, m_r)))
				return;
		if (column != null && (column.isHidden(m_r) || !column.userCanView(m_mode, m_v.data, m_r))) {
			if (!column.userHasViewRole(m_r) && !m_r.userIsAdministrator()) {
				if (m_mode == View.Mode.ADD_FORM) {
					String default_value = column.getDefaultValue(m_v.getState(), m_r);
					if (default_value != null)
						m_w.hiddenInput(column_name, default_value);
				}
				return;
			}
			if (m_r.userIsAdministrator())
				m_r.w.write(" <span class=\"text-success\" style=\"font-size:small\">admin</span>");
		}
		rowOpen(column_name);
		m_w.write("<div>");
		writeColumnInput(column_name, column, false);
		m_w.write("</div>");
		rowClose();
	}

	// --------------------------------------------------------------------------

	@Override
	protected void
	writeLabel(String column_name) {
		ColumnBase<?> column = m_v.getColumn(column_name);
		if (column != null) {
			super.writeLabel(column.getLabel(m_v, m_r));
			column.writeAdministratorInfo(m_r);
		} else {
			JDBCColumn jdbc_column = m_r.db.getColumn(m_view_def.getFrom(), column_name);
			if (jdbc_column == null || m_printer_friendly || !jdbc_column.isBoolean() || m_mode == View.Mode.READ_ONLY_FORM)
				super.writeLabel(column_name);
		}
	}

	// --------------------------------------------------------------------------

	@Override
	public final void
	writeSubmit() {
		StringBuilder on_click = new StringBuilder();
		on_click.append("_.form.submit(this");
		if (m_mode == View.Mode.ADD_FORM || m_mode == View.Mode.EDIT_FORM)
			on_click.append(",{validate:true}");
		on_click.append(')');
		m_w.setId(Text.uuid());
		if (m_r.userIsAdministrator())
			m_w.setAttribute("title", m_view_state.getKeyValue());
		m_w.ui.buttonOnClick(m_submit_text, on_click.toString());
	}
}
