package util;

import java.text.DateFormatSymbols;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatterBuilder;
import java.time.format.FormatStyle;
import java.time.format.ResolverStyle;
import java.time.temporal.ChronoField;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.Locale;

public class TimeFormatter {
	private final DateFormatSymbols	m_date_format_symbols;
	private final java.time.format.DateTimeFormatter m_date_long;
	private final java.time.format.DateTimeFormatter m_date_medium;
	private final java.time.format.DateTimeFormatter m_date_short;
	private final java.time.format.DateTimeFormatter m_date_time;
	private final java.time.format.DateTimeFormatter m_month_date;
	private final java.time.format.DateTimeFormatter m_time;
	private final java.time.format.DateTimeFormatter m_weekday_long;
	private final java.time.format.DateTimeFormatter m_weekday_month_date;

	//--------------------------------------------------------------------------

	public
	TimeFormatter(Locale locale) {
		m_date_format_symbols = locale == null ? new DateFormatSymbols() : new DateFormatSymbols(locale);
		m_date_long = locale == null ? java.time.format.DateTimeFormatter.ofLocalizedDate(FormatStyle.LONG) : java.time.format.DateTimeFormatter.ofLocalizedDate(FormatStyle.LONG).withLocale(locale);
		m_date_medium = locale == null ? java.time.format.DateTimeFormatter.ofLocalizedDate(FormatStyle.MEDIUM) : java.time.format.DateTimeFormatter.ofLocalizedDate(FormatStyle.MEDIUM).withLocale(locale);
		m_date_short = locale == null ? java.time.format.DateTimeFormatter.ofPattern("M/d/uuuu").withResolverStyle(ResolverStyle.STRICT) : java.time.format.DateTimeFormatter.ofPattern("M/d/uuuu", locale).withResolverStyle(ResolverStyle.STRICT);
		m_date_time = locale == null ? java.time.format.DateTimeFormatter.ofPattern("MM/dd/yyyy h:mm a") : java.time.format.DateTimeFormatter.ofPattern("MM/dd/yyyy h:mm a", locale);
		m_month_date = locale == null ? new DateTimeFormatterBuilder().appendPattern("MMMM d").toFormatter() : new DateTimeFormatterBuilder().appendPattern("MMMM d").toFormatter(locale);
		m_time = locale == null ? java.time.format.DateTimeFormatter.ofPattern("h:mm a") : java.time.format.DateTimeFormatter.ofPattern("h:mm a", locale);
		m_weekday_long = locale == null ? java.time.format.DateTimeFormatter.ofPattern("EEEE") : java.time.format.DateTimeFormatter.ofPattern("EEEE", locale);
		m_weekday_month_date = locale == null ? new DateTimeFormatterBuilder().appendPattern("EEEE MMMM d").toFormatter() : new DateTimeFormatterBuilder().appendPattern("EEEE MMMM d").toFormatter(locale);
	}

   //--------------------------------------------------------------------------

	public final void
	appendTimeRange(LocalTime start, LocalTime end, StringBuilder s) {
		if (start == null)
			return;
		if (end == null) {
			s.append(getTime(start, true));
			return;
		}
		s.append(getTime(start, start.get(ChronoField.AMPM_OF_DAY) != end.get(ChronoField.AMPM_OF_DAY))).append('-').append(getTime(end, true));
	}

	//--------------------------------------------------------------------------

	public final String
	getDateLong(LocalDate date) {
		try {
			return m_date_long.format(date);
		} catch (Exception e) {
			return null;
		}
	}

	//--------------------------------------------------------------------------

	public final String
	getDateMedium(LocalDate date) {
		try {
			return m_date_medium.format(date);
		} catch (Exception e) {
			return null;
		}
	}

	//--------------------------------------------------------------------------

	public final String
	getDateShort(LocalDate date) {
		try {
			return m_date_short.format(date);
		} catch (Exception e) {
			return null;
		}
	}

	//--------------------------------------------------------------------------

	public final String
	getDateTime(LocalDateTime date_time) {
		try {
			return m_date_time.format(date_time);
		} catch (Exception e) {
			return null;
		}
	}

	//--------------------------------------------------------------------------

	public final String
	getMonthDate(LocalDate date) {
		try {
			return m_month_date.format(date);
		} catch (Exception e) {
			return null;
		}
	}

	//--------------------------------------------------------------------------

	public final String
	getMonthNameLong(LocalDate date) {
		try {
			return m_date_format_symbols.getMonths()[date.getMonthValue() - 1];
		} catch (Exception e) {
			return null;
		}
	}

	//--------------------------------------------------------------------------

	public final String
	getMonthNameLong(LocalDateTime datetime) {
		try {
			return m_date_format_symbols.getMonths()[datetime.getMonthValue() - 1];
		} catch (Exception e) {
			return null;
		}
	}

	//--------------------------------------------------------------------------

	public final String
	getMonthNameLong(int month) {
		try {
			return m_date_format_symbols.getMonths()[month - 1];
		} catch (Exception e) {
			return null;
		}
	}

	//--------------------------------------------------------------------------

	public final String
	getMonthNameShort(LocalDate date) {
		try {
			return m_date_format_symbols.getShortMonths()[date.getMonthValue() - 1];
		} catch (Exception e) {
			return null;
		}
	}

	//--------------------------------------------------------------------------

	public final String
	getMonthNameShort(int month) {
		try {
			return m_date_format_symbols.getShortMonths()[month - 1];
		} catch (Exception e) {
			return null;
		}
	}

	//--------------------------------------------------------------------------

	public final String[]
	getMonthNamesLong() {
		try {
			String[] months = m_date_format_symbols.getMonths();
			if (months.length > 12)
				months = Arrays.copyOfRange(months, 0, 12);
			return months;
		} catch (Exception e) {
			return null;
		}
	}

	//--------------------------------------------------------------------------

	public final String
	getTime(LocalTime time, boolean am_pm) {
		try {
			StringBuilder s = new StringBuilder();
			int h = time.getHour();
			s.append(h == 0 ? 12 : h == 12 ? 12 : h % 12);
			s.append(':');
			int m = time.getMinute();
			if (m < 10)
				s.append('0');
			s.append(m);
			if (am_pm) {
				String[] strings = m_date_format_symbols.getAmPmStrings();
				s.append(time.get(ChronoField.AMPM_OF_DAY) == 0 ? strings[0].toLowerCase() : strings[1].toLowerCase());
			}
			return s.toString();
		} catch (Exception e) {
			return null;
		}
	}

	//--------------------------------------------------------------------------

	public final String
	getTime(int minutes, boolean am_pm) {
		try {
			StringBuilder s = new StringBuilder();
			int h = minutes / 60;
			if (h > 11)
				h -= 12;
			s.append(h == 0 ? 12 : h);
			s.append(':');
			int m = minutes % 60;
			if (m < 10)
				s.append('0');
			s.append(m);
			if (am_pm) {
				String[] strings = m_date_format_symbols.getAmPmStrings();
				s.append(minutes == 1440 || minutes < 720 ? strings[0].toLowerCase() : strings[1].toLowerCase());
			}
			return s.toString();
		} catch (Exception e) {
			return null;
		}
	}

	//--------------------------------------------------------------------------

	public final String
	getTimeAgo(LocalDateTime date_time, LocalDateTime now) {
		try {
			long hours = ChronoUnit.HOURS.between(date_time, now);
			if (hours < 1) {
				long minutes = ChronoUnit.MINUTES.between(date_time, now);
				return minutes + " " + Text.pluralize("minute", minutes) + " ago";
			}
			StringBuilder s = new StringBuilder();
			if (ChronoUnit.DAYS.between(date_time, now) < 2 && date_time.getDayOfMonth() == now.getDayOfMonth())
				s.append("today");
			else {
				s.append(getMonthNameLong(date_time)).append(' ').append(date_time.getDayOfMonth());
				if (date_time.getYear() != now.getYear())
					s.append(", ").append(date_time.getYear());
			}
			s.append(" at ");
			s.append(getTime(date_time.toLocalTime(), true));
			return s.toString();
		} catch (Exception e) {
			return null;
		}
	}

	//--------------------------------------------------------------------------

	public final String
	getTimeRange(LocalTime start, LocalTime end) {
		try {
			if (end == null)
				return getTime(start, true);
			return getTime(start, start.get(ChronoField.AMPM_OF_DAY) != end.get(ChronoField.AMPM_OF_DAY)) + '-' + getTime(end, true);
		} catch (Exception e) {
			return null;
		}
	}

	//--------------------------------------------------------------------------

	public final String
	getWeekdayLong(LocalDate date) {
		try {
			return m_weekday_long.format(date);
		} catch (Exception e) {
			return null;
		}
	}

	//--------------------------------------------------------------------------

	public final String
	getWeekdayMonthDate(LocalDate date) {
		try {
			return m_weekday_month_date.format(date);
		} catch (Exception e) {
			return null;
		}
	}

	//--------------------------------------------------------------------------

	public final String[]
	getWeekdays() {
		try {
			return m_date_format_symbols.getWeekdays();
		} catch (Exception e) {
			return null;
		}
	}

	//--------------------------------------------------------------------------

	public final LocalTime
	parseTime(String s) {
		if (s == null || s.length() == 0)
			return null;
		try {
			return LocalTime.parse(s, m_time);
		} catch (Exception e) {
			try {
				return LocalTime.parse(s);
			} catch (Exception e2) {
				e2.printStackTrace();
				throw e2;
			}
		}
	}
}
