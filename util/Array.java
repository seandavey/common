package util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.stream.Stream;

public class Array {
	public static String[]
	concat(String[] a, String... s) {
		return Stream.concat(Arrays.stream(a), Arrays.stream(s)).toArray(String[]::new);
	}

	//--------------------------------------------------------------------------

	public static int
	indexOf(int[] a, int n) {
		for (int i=0; i<a.length; i++)
			if (a[i] == n)
				return i;
		return -1;
	}

	//--------------------------------------------------------------------------

	public static int
	indexOf(String[] a, String s) {
		if (a == null)
			return -1;
		for (int i=0; i<a.length; i++)
			if (a[i].equals(s))
				return i;
		return -1;
	}

	//--------------------------------------------------------------------------

	public static String[]
	insert(String[] a, String s, int index) {
		String[] new_strings = new String[a.length + 1];
		int j = 0;
		for (int i=0; i<a.length; i++) {
			if (i == index)
				j++;
			new_strings[j++] = a[i];
		}
		new_strings[index] = s;
		return new_strings;
	}

	//--------------------------------------------------------------------------

	public static String[]
	remove(String[] a, int index) {
		ArrayList<String> array_list = new ArrayList<>(a.length - 1);
		for (int i=0; i<index; i++)
			array_list.add(a[i]);
		for (int i=index+1; i<a.length; i++)
			array_list.add(a[i]);
		return array_list.toArray(new String[array_list.size()]);
	}

	//--------------------------------------------------------------------------

	public static String[]
	remove(String[] a, String s) {
		int i = indexOf(a, s);
		while (i != -1) {
			a = remove(a, i);
			i = indexOf(a, s);
		}
		return a;
	}

	//--------------------------------------------------------------------------

	public static int[]
	split(String s) {
		String[] a = s.split(",");
		int[] ints = new int[a.length];
		for (int i=0; i<a.length; i++)
			ints[i] = Integer.parseInt(a[i]);
		return ints;
	}
}
