package pages;

import app.Module;
import app.Request;
import app.Site;
import db.object.DBField;
import ui.NavBar;
import ui.SelectOption;
import util.Array;
import web.JS;

public class Page implements SelectOption {
	@DBField
	private boolean			m_embed;
	@DBField
	private boolean			m_enabled = true;
	private final boolean	m_has_menu;
	@DBField
	private int				m_id;
	boolean					m_is_site_page = true;
	private final Module	m_module;
	@DBField
	private String			m_name;
	@DBField
	private boolean			m_public;
	@DBField
	private String[]		m_roles;
	private boolean			m_show_in_other = true;
	@DBField
	private String			m_text;
	@DBField
	private String			m_url;

	//--------------------------------------------------------------------------

	public
	Page() {
		m_has_menu = false;
		m_module = null;
	}

	//--------------------------------------------------------------------------
	// page for user dropdown items

	public
	Page(String name, Module module, boolean has_menu) {
		m_name = name;
		m_module = module;
		m_has_menu = has_menu;
		m_url = "/" + name.replaceAll(" ", "+");
	}

	//--------------------------------------------------------------------------

	public
	Page(String name, String url) {
		m_name = name;
		m_url = url;
		m_has_menu = false;
		m_module = null;
	}

	//--------------------------------------------------------------------------

	public
	Page(String name, boolean has_menu) {
		m_name = name;
		m_has_menu = has_menu;
		m_url = "/" + name.replaceAll(" ", "+");
		m_module = null;
	}

	//--------------------------------------------------------------------------

	public void
	a(NavBar nav_bar, Request r) {
		String url = getURL();
		if (url.startsWith("http"))
			nav_bar.a(getName(), url, false);
		else if (url.startsWith("/Views/") || url.endsWith(".jsp"))
			nav_bar.a(getName(), Site.context + url);
		else {
			if (url.charAt(0) == '/')
				url = url.substring(1);
			r.w.setAttribute("data-url", url);
			nav_bar.aOnClick(getName(), "app.go('" + url + "')");
		}
	}

	//--------------------------------------------------------------------------

	public final Page
	addRole(String role) {
		if (m_roles == null)
			m_roles = new String[] {role};
		else if (Array.indexOf(m_roles, role) == -1)
			m_roles = Array.concat(m_roles, role);
		return this;
	}

	//--------------------------------------------------------------------------

	public final boolean
	canView(Request r) {
		if (m_module != null && !m_module.isActive())
			return false;
		if (!m_enabled)
			return false;
		if (m_public)
			return true;
		if (r.request.getRemoteUser() == null)
			return false;
		return m_roles == null || r.userHasRole(m_roles);
	}

	//--------------------------------------------------------------------------

	@Override
	public boolean
	equals(Object object) {
	    if (object == null)
	    	return false;
	    if (object == this)
	    	return true;
	    if (!(object instanceof Page))
	    	return false;
	    if (m_name == null)
	    	return ((Page)object).m_name == null;
	    return m_name.equals(((Page)object).m_name);
	}

	//--------------------------------------------------------------------------

	public final String
	getName() {
		return m_name;
	}

	//--------------------------------------------------------------------------

	public final boolean
	showInOther() {
		return m_show_in_other;
	}

	//--------------------------------------------------------------------------

	@Override // SelectOption
	public String
	getText() {
		return m_name;
	}

	//--------------------------------------------------------------------------

	public final String
	getURL() {
		if (m_url == null || m_embed)
			return "/" + m_name.replaceAll(" ", "+");
		return m_url;
	}

	//--------------------------------------------------------------------------

	@Override // SelectOption
	public String
	getValue() {
		return null;
	}

	//--------------------------------------------------------------------------

	public boolean
	isPublic() {
		return m_public;
	}

	//--------------------------------------------------------------------------

	public final void
	setShowInOther(boolean show_in_other) {
		m_show_in_other = show_in_other;
	}

	//--------------------------------------------------------------------------

	public final void
	write(boolean full_page, Request r) {
		if (!canView(r)) {
			r.w.write("Access denied for page ").write(r.request.getRequestURI()).write(". Contact the site administrator for more info.<br /><br />");
			r.w.a(Site.site.getDisplayName() + " Home Page", Site.context + "/" + Site.site.getHomePage());
			return;
		}
		if (!m_public && !Site.credentials.canAccessSite(r))
			return;
		if (full_page)
			Site.site.pageOpen(m_name, false, r);
		r.w.js("var _menus=[['#top_menu', " + JS.string(getURL().substring(1)) + "]]");
		r.setSessionAttribute("current page", m_name);
		NavBar nav_bar = null;
		if (m_has_menu) {
			if (m_module != null)
				r.w.setAttribute("data-url", m_module.apiURL("page_menu"));
			r.w.tagOpen("div");
			nav_bar = writeMenu(r);
			r.w.tagClose();
		}
		r.w.setId("main").mainOpen();
		if (nav_bar != null && !"main".equals(nav_bar.target()))
			r.w.setId(nav_bar.target())
				.addClass("container")
				.tagOpen("div");
		if (nav_bar == null || !nav_bar.checkClick(r)) {
			if (m_text != null)
				r.w.addClass("ck-content doc-view").tag("div", m_text);
			writeContent(r);
		}
		r.close();
	}

	//--------------------------------------------------------------------------

	protected void
	writeContent(Request r) {
		if (m_embed)
			r.w.write("<style>html,body,#main{height:100%}</style><div style=\"height:100%;display:flex;flex-direction:column;\"><iframe src=\"").write(m_url).write("\" style=\"border-width:0;flex-grow:1;\"></iframe></div>");
	}

	//--------------------------------------------------------------------------

	protected NavBar
	writeMenu(Request r) {
		if (m_module != null)
			return m_module.writePageMenu(r);
		return null;
	}
}
