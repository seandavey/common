package pages;

import java.text.Collator;
import java.util.Collection;
import java.util.Map;
import java.util.TreeMap;

import app.Module;
import app.Request;
import app.Roles;
import app.Site;
import db.DBConnection;
import db.DeleteHook;
import db.Form;
import db.InsertHook;
import db.NameValuePairs;
import db.Rows;
import db.SQL;
import db.Select;
import db.UpdateHook;
import db.View;
import db.ViewDef;
import db.access.AccessPolicy;
import db.column.Column;
import db.column.TagsColumn;
import db.column.TextAreaColumn;
import db.object.DBObject;
import db.object.JSONField;

public class Pages extends Module implements DeleteHook, InsertHook, UpdateHook {
	private final Map<String,Page>	m_pages;
	@JSONField
	private boolean					m_support_pages_table;

	//--------------------------------------------------------------------

	public
	Pages(Collator collator) {
		m_pages = new TreeMap<>(collator);
	}

	//--------------------------------------------------------------------

	public final void
	add(Page p, DBConnection db) {
		if (m_support_pages_table && db != null && !DBObject.load(p, "pages", "url=" + SQL.string(p.getURL()), db))
			DBObject.store(p, "pages", new NameValuePairs(), db);
		String name = p.getName();
		if (name != null)
			m_pages.put(name.toLowerCase(), p);
		else
			Site.site.log("page name is null", true);
	}

	//--------------------------------------------------------------------

	@Override
	public void
	afterInsert(int id, NameValuePairs nvp, Request r) {
		Page page = new Page();
		page.m_is_site_page = false;
		DBObject.load(page, "pages", id, r.db);
		m_pages.put(page.getName().toLowerCase(), page);
	}

	//--------------------------------------------------------------------

	@Override
	public void
	afterUpdate(int id, NameValuePairs nvp, Map<String,Object> previous_values, Request r) {
		Page page = (Page)previous_values.get("page");
		if (page != null) {
			DBObject.load(page, "pages", id, r.db);
			m_pages.put(page.getName().toLowerCase(), page);
		}
	}

	//--------------------------------------------------------------------

	@Override
	public String
	beforeDelete(String where, Request r) {
		String name = r.db.lookupString(new Select("name").from("pages").where(where));
		m_pages.remove(name.toLowerCase());
		return null;
	}

	//--------------------------------------------------------------------

	@Override
	public String
	beforeUpdate(int id, NameValuePairs nvp, Map<String,Object> previous_values, Request r) {
		String name = r.db.lookupString(new Select("name").from("pages").whereIdEquals(id));
		if (name != null) {
			name = name.toLowerCase();
			previous_values.put("page", m_pages.get(name));
			m_pages.remove(name);
		}
		return null;
	}

	//--------------------------------------------------------------------

	public final Page
	getPageByName(String name) {
		return m_pages.get(name.toLowerCase());
	}

	//--------------------------------------------------------------------

	public final Page
	getPageByURL(String url) {
		int i = url.indexOf('/', 1);
		if (i != -1)
			url = url.substring(0, i);
		for (Page page : m_pages.values())
			if (url.equals(page.getURL()))
				return page;
		return null;
	}

	//--------------------------------------------------------------------

	public final Collection<Page>
	getPages() {
		return m_pages.values();
	}

	//--------------------------------------------------------------------

	@Override
	public void
	init(boolean was_added, DBConnection db) {
		if (m_support_pages_table)
			DBObject.init("pages", Page.class, db);
	}

	//--------------------------------------------------------------------

	public final void
	loadPages(DBConnection db) {
		if (!m_support_pages_table)
			return;
		Rows rows = new Rows(new Select("*").from("pages"), db);
		while (rows.next()) {
			String name = rows.getString("name");
			if (name != null && getPageByName(name) != null)
				continue;
			Page page = new Page();
			DBObject.load(page, rows);
			page.m_is_site_page = false;
			name = page.getName();
			if (name != null)
				m_pages.put(name.toLowerCase(), page);
			else
				Site.site.log("page name null", true);
		}
		rows.close();
	}

	//--------------------------------------------------------------------

	@Override
	public ViewDef
	_newViewDef(String name) {
		if (name.equals("pages"))
			return new ViewDef(name)
				.addDeleteHook(this)
				.addInsertHook(this)
				.addUpdateHook(this)
				.setAccessPolicy(new AccessPolicy() {
					@Override
					public boolean
					showDeleteButtonForRow(Rows data, Request r) {
						Page p = getPageByName(data.getString("name"));
						return p != null && !p.m_is_site_page;
					}
				}.add().delete().edit())
				.setDefaultOrderBy("lower(name)")
				.setRecordName("Page", true)
				.setColumnNamesForm(new String[] { "name", "url", "embed", "enabled", "public", "roles", "text" })
				.setColumnNamesTable(new String[] { "name", "url", "enabled", "public", "roles" })
				.setColumn(new Column("embed").setHelpText("check to embed the page specified in the URL field, top menu and footer will be shown"))
				.setColumn(new Column("enabled").setDefaultValue(true))
				.setColumn(new Column("name").setHelpText("the names for the Groups and Other pages should not be changed; Groups can be renamed in Edit Site->Modules").setIsRequired(true))
				.setColumn(new Column("public").setHelpText("check to make the page viewable by anyone on the internet (no account on the site needed)"))
				.setColumn(new TagsColumn("roles", null){
					@Override
					public void
					writeInput(View v, Form f, boolean inline, Request r) {
						m_choices = Roles.getRoleNames();
						super.writeInput(v, f, inline, r);
					}
				}.setHelpText("role(s) needed to view page if not public"))
				.setColumn(new TextAreaColumn("text").setHelpText("text to display at the top of the page").setIsRichText(true, false))
				.setColumn(new Column("url"){
					@Override
					public void
					writeInput(View v, Form f, boolean inline, Request r) {
						if (v.data != null) {
							String url = v.data.getString("url");
							if (url != null) {
								Page p = getPageByURL(url);
								if (p != null && p.m_is_site_page) {
									r.w.ui.helpText(url);
									return;
								}
							}
						}
						super.writeInput(v, f, inline, r);
					}
				}.setIsRequired(true));
		return null;
	}

	//--------------------------------------------------------------------------

//	public void
//	remove(Page p) {
//		m_pages.remove(p.getName().toLowerCase());
//	}

	//--------------------------------------------------------------------

	public final Pages
	setSupportPagesTable(boolean support_pages_table) {
		m_support_pages_table = support_pages_table;
		return this;
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	writeAdminPane(String[] column_names, boolean show_tasks, boolean show_object, Request r) {
		super.writeAdminPane(column_names, show_tasks, show_object, r);
		r.w.h3("Pages");
		Site.site.newView("pages", r).writeComponent();
	}
}
