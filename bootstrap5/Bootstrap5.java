package bootstrap5;

import java.io.IOException;
import java.util.List;

import app.Request;
import util.Text;
import web.HTMLWriter;

public class Bootstrap5 {
	private static final String[]	s_months = new String[] { "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" };

	private final HTMLWriter		m_w;

	public final String				bg_secondary = "bg-secondary";
	public final String				section_head = "mosaic-section-head bg-primary-subtle text-emphasis-primary";

	//--------------------------------------------------------------------------

	public
	Bootstrap5(HTMLWriter w) {
		m_w = w;
	}

	//--------------------------------------------------------------------------

	public HTMLWriter
	aButton(String text, String href) {
		return aButton(text, href, "btn-sm");
	}

	//--------------------------------------------------------------------------

	public HTMLWriter
	aButton(String text, String href, String size_class) {
		m_w.addClass("btn btn-secondary");
		if (size_class != null)
			m_w.addClass(size_class);
		return m_w.a(text, href);
	}

	//--------------------------------------------------------------------------

	public HTMLWriter
	aIcon(String href, String icon, String text) {
		m_w.addClass("btn btn-secondary btn-sm")
			.setAttribute("href", href)
			.tagOpen("a");
		icon(icon);
		if (text != null)
			m_w.space().write(text);
		return m_w.tagClose();
	}

	//--------------------------------------------------------------------------

	public ui.Button
	button(String text) {
		return new Button(text, m_w);
	}

	//--------------------------------------------------------------------------

	public int
	buttonGroupOpen() {
		return m_w.addClass("btn-group").tagOpen("div");
	}

	//--------------------------------------------------------------------------

	public HTMLWriter
	buttonIcon(String icon) {
		return buttonIconOnClick(icon, null, null);
	}

	//--------------------------------------------------------------------------

	public HTMLWriter
	buttonIconOnClick(String icon, String on_click) {
		return buttonIconOnClick(icon, null, on_click);
	}

	//--------------------------------------------------------------------------

	public HTMLWriter
	buttonIconOnClick(String icon, String text, String on_click) {
		m_w.setOnClick(on_click);
		m_w.addClass("btn btn-outline-primary btn-sm")
			.addStyle("box-sizing:content-box;max-height:1.5em")
			.tagOpen("button");
		icon(icon);
		if (text != null)
			m_w.nbsp().write(text);
		return m_w.tagClose();
	}

	//--------------------------------------------------------------------------

	public HTMLWriter
	buttonOnClick(String text, String on_click) {
		return buttonOnClick(text, on_click, true, "secondary");
	}

	//--------------------------------------------------------------------------

	public HTMLWriter
	buttonOnClick(String text, String on_click, boolean small) {
		return buttonOnClick(text, on_click, small, "secondary");
	}

	//--------------------------------------------------------------------------

	public HTMLWriter
	buttonOnClick(String text, String on_click, boolean small, String variant) {
		m_w.setAttribute("type", "button").addClass("btn").setOnClick(on_click);
		if (small)
			m_w.addClass("btn-sm");
		if (variant != null)
			m_w.addClass("btn-" + variant);
		return m_w.tag("button", text);
	}

	//--------------------------------------------------------------------------

	public HTMLWriter
	buttonOutlineOnClick(String text, String on_click) {
		return m_w.setAttribute("type", "button").addClass("btn btn-outline-primary btn-sm").setOnClick(on_click).tag("button", text);
	}

	//--------------------------------------------------------------------------

	public ui.Card
	card() {
		return new Card(m_w);
	}

	//--------------------------------------------------------------------------

	public HTMLWriter
	checkbox(String name, String label, String title, boolean checked, boolean inline, boolean use_hidden) {
		return checkbox(name, "yes", label, title, checked, inline, use_hidden);
	}

	//--------------------------------------------------------------------------

	public HTMLWriter
	checkbox(String name, String value, String label, String title, boolean checked, boolean inline, boolean use_hidden) {
		if (inline)
			m_w.write("<div class=\"form-check form-check-inline\" style=\"margin-right:0;min-height:0;\">");
		else
			m_w.write("<div class=\"form-check\">");
		m_w.setAttribute("type", "checkbox");
		if (name != null) {
			m_w.setId(name + "_cb")
				.setAttribute("value", value);
			if (use_hidden)
				m_w.addToAttribute("onchange", "this.nextElementSibling.value=this.checked?'true':'false'");
			else
				m_w.setAttribute("name", name);
		}
		if (checked)
			m_w.setAttribute("checked", null);
		if (title != null)
			m_w.setAttribute("title", title);
		m_w.addClass("form-check-input")
			.tag("input");
		if (name != null && use_hidden)
			m_w.hiddenInput(name, checked ? "true" : "false");
		if (label != null) {
			if (title != null)
				m_w.setAttribute("title", title);
			m_w.addClass("form-check-label");
			if (name != null)
				m_w.setAttribute("for", name + "_cb");
			m_w.tag("label", label);
		}
		m_w.write("</div>");
		return m_w;
	}

	//--------------------------------------------------------------------------

	public int
	containerOpen() {
		return m_w.addClass("container").tagOpen("div");
	}

	//--------------------------------------------------------------------------

	public HTMLWriter
	display6(String head) {
		return m_w.addClass("display-6").tag("div", head);
	}

	//--------------------------------------------------------------------------

	public ui.Dropdown
	dropdown() {
		return new Dropdown(m_w);
	}

	//--------------------------------------------------------------------------

	public ui.Dropdown
	dropdown(String button_text, boolean small) {
		return new Dropdown(button_text, small, m_w);
	}

	//--------------------------------------------------------------------------

	public HTMLWriter
	figure(String src, String caption, String img_style) {
		m_w.addClass("figure").tagOpen("figure");
		if (src != null) {
			if (img_style != null)
				m_w.addStyle(img_style);
			m_w.addClass("figure-img img-fluid rounded").setAttribute("src", src).tag("img");
		}
		m_w.addClass("figure-caption").tag("figcaption", caption);
		m_w.tagClose();
		return m_w;
	}

	//--------------------------------------------------------------------------

	public HTMLWriter
	formLabel(String label) {
		return m_w.addClass("form-label").tag("label", label);
	}

	//--------------------------------------------------------------------------

	public HTMLWriter
	helpText(String text) {
		return m_w.addClass("form-text").tag("div", text);
	}

	//--------------------------------------------------------------------------

	public HTMLWriter
	icon(String icon) {
		m_w.setAttribute("width", "16").setAttribute("height", "16").setAttribute("viewBox", "0 0 16 16").setAttribute("fill", "currentColor").setAttribute("data-icon", icon).tagOpen("svg");
		m_w.write("<path fill-rule=\"evenodd\" d=\"");
		switch(icon) {
		case "archive":
			m_w.write("M0 2a1 1 0 0 1 1-1h14a1 1 0 0 1 1 1v2a1 1 0 0 1-1 1v7.5a2.5 2.5 0 0 1-2.5 2.5h-9A2.5 2.5 0 0 1 1 12.5V5a1 1 0 0 1-1-1V2zm2 3v7.5A1.5 1.5 0 0 0 3.5 14h9a1.5 1.5 0 0 0 1.5-1.5V5H2zm13-3H1v2h14V2zM5 7.5a.5.5 0 0 1 .5-.5h5a.5.5 0 0 1 0 1h-5a.5.5 0 0 1-.5-.5z");
			break;
		case "arrow-clockwise":
			m_w.write("M8 3a5 5 0 1 0 4.546 2.914.5.5 0 0 1 .908-.417A6 6 0 1 1 8 2z\"/><path d=\"M8 4.466V.534a.25.25 0 0 1 .41-.192l2.36 1.966c.12.1.12.284 0 .384L8.41 4.658A.25.25 0 0 1 8 4.466");
			break;
		case "arrow-down":
			m_w.write("M8 1a.5.5 0 0 1 .5.5v11.793l3.146-3.147a.5.5 0 0 1 .708.708l-4 4a.5.5 0 0 1-.708 0l-4-4a.5.5 0 0 1 .708-.708L7.5 13.293V1.5A.5.5 0 0 1 8 1z");
			break;
		case "arrow-left":
			m_w.write("M15 8a.5.5 0 0 0-.5-.5H2.707l3.147-3.146a.5.5 0 1 0-.708-.708l-4 4a.5.5 0 0 0 0 .708l4 4a.5.5 0 0 0 .708-.708L2.707 8.5H14.5A.5.5 0 0 0 15 8z");
			break;
		case "arrow-right":
			m_w.write("M1 8a.5.5 0 0 1 .5-.5h11.793l-3.147-3.146a.5.5 0 0 1 .708-.708l4 4a.5.5 0 0 1 0 .708l-4 4a.5.5 0 0 1-.708-.708L13.293 8.5H1.5A.5.5 0 0 1 1 8z");
			break;
		case "arrow-up":
			m_w.write("M8 15a.5.5 0 0 0 .5-.5V2.707l3.146 3.147a.5.5 0 0 0 .708-.708l-4-4a.5.5 0 0 0-.708 0l-4 4a.5.5 0 1 0 .708.708L7.5 2.707V14.5a.5.5 0 0 0 .5.5z");
			break;
		case "camera":
			m_w.write("M15 12a1 1 0 0 1-1 1H2a1 1 0 0 1-1-1V6a1 1 0 0 1 1-1h1.172a3 3 0 0 0 2.12-.879l.83-.828A1 1 0 0 1 6.827 3h2.344a1 1 0 0 1 .707.293l.828.828A3 3 0 0 0 12.828 5H14a1 1 0 0 1 1 1v6zM2 4a2 2 0 0 0-2 2v6a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V6a2 2 0 0 0-2-2h-1.172a2 2 0 0 1-1.414-.586l-.828-.828A2 2 0 0 0 9.172 2H6.828a2 2 0 0 0-1.414.586l-.828.828A2 2 0 0 1 3.172 4H2z\"/><path d=\"M8 11a2.5 2.5 0 1 1 0-5 2.5 2.5 0 0 1 0 5zm0 1a3.5 3.5 0 1 0 0-7 3.5 3.5 0 0 0 0 7zM3 6.5a.5.5 0 1 1-1 0 .5.5 0 0 1 1 0z");
			break;
		case "chat-text":
			m_w.write("M2.678 11.894a1 1 0 0 1 .287.801 10.97 10.97 0 0 1-.398 2c1.395-.323 2.247-.697 2.634-.893a1 1 0 0 1 .71-.074A8.06 8.06 0 0 0 8 14c3.996 0 7-2.807 7-6 0-3.192-3.004-6-7-6S1 4.808 1 8c0 1.468.617 2.83 1.678 3.894zm-.493 3.905a21.682 21.682 0 0 1-.713.129c-.2.032-.352-.176-.273-.362a9.68 9.68 0 0 0 .244-.637l.003-.01c.248-.72.45-1.548.524-2.319C.743 11.37 0 9.76 0 8c0-3.866 3.582-7 8-7s8 3.134 8 7-3.582 7-8 7a9.06 9.06 0 0 1-2.347-.306c-.52.263-1.639.742-3.468 1.105z\"/><path d=\"M4 5.5a.5.5 0 0 1 .5-.5h7a.5.5 0 0 1 0 1h-7a.5.5 0 0 1-.5-.5zM4 8a.5.5 0 0 1 .5-.5h7a.5.5 0 0 1 0 1h-7A.5.5 0 0 1 4 8zm0 2.5a.5.5 0 0 1 .5-.5h4a.5.5 0 0 1 0 1h-4a.5.5 0 0 1-.5-.5z");
			break;
//		case "check":
//			m_w.write("M10.97 4.97a.75.75 0 0 1 1.07 1.05l-3.99 4.99a.75.75 0 0 1-1.08.02L4.324 8.384a.75.75 0 1 1 1.06-1.06l2.094 2.093 3.473-4.425z");
//			break;
		case "chevron-down":
			m_w.write("M1.646 4.646a.5.5 0 0 1 .708 0L8 10.293l5.646-5.647a.5.5 0 0 1 .708.708l-6 6a.5.5 0 0 1-.708 0l-6-6a.5.5 0 0 1 0-.708z");
			break;
		case "chevron-right":
			m_w.write("M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z");
			break;
		case "cloud-download":
			m_w.write("M4.406 1.342A5.53 5.53 0 0 1 8 0c2.69 0 4.923 2 5.166 4.579C14.758 4.804 16 6.137 16 7.773 16 9.569 14.502 11 12.687 11H10a.5.5 0 0 1 0-1h2.688C13.979 10 15 8.988 15 7.773c0-1.216-1.02-2.228-2.313-2.228h-.5v-.5C12.188 2.825 10.328 1 8 1a4.53 4.53 0 0 0-2.941 1.1c-.757.652-1.153 1.438-1.153 2.055v.448l-.445.049C2.064 4.805 1 5.952 1 7.318 1 8.785 2.23 10 3.781 10H6a.5.5 0 0 1 0 1H3.781C1.708 11 0 9.366 0 7.318c0-1.763 1.266-3.223 2.942-3.593.143-.863.698-1.723 1.464-2.383\"/><path d=\"M7.646 15.854a.5.5 0 0 0 .708 0l3-3a.5.5 0 0 0-.708-.708L8.5 14.293V5.5a.5.5 0 0 0-1 0v8.793l-2.146-2.147a.5.5 0 0 0-.708.708z");
			break;
		case "envelope":
			m_w.write("M0 4a2 2 0 0 1 2-2h12a2 2 0 0 1 2 2v8a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V4zm2-1a1 1 0 0 0-1 1v.217l7 4.2 7-4.2V4a1 1 0 0 0-1-1H2zm13 2.383l-4.758 2.855L15 11.114v-5.73zm-.034 6.878L9.271 8.82 8 9.583 6.728 8.82l-5.694 3.44A1 1 0 0 0 2 13h12a1 1 0 0 0 .966-.739zM1 11.114l4.758-2.876L1 5.383v5.73z");
			break;
		case "folder":
			m_w.write("M.54 3.87.5 3a2 2 0 0 1 2-2h3.672a2 2 0 0 1 1.414.586l.828.828A2 2 0 0 0 9.828 3h3.982a2 2 0 0 1 1.992 2.181l-.637 7A2 2 0 0 1 13.174 14H2.826a2 2 0 0 1-1.991-1.819l-.637-7a1.99 1.99 0 0 1 .342-1.31zM2.19 4a1 1 0 0 0-.996 1.09l.637 7a1 1 0 0 0 .995.91h10.348a1 1 0 0 0 .995-.91l.637-7A1 1 0 0 0 13.81 4H2.19zm4.69-1.707A1 1 0 0 0 6.172 2H2.5a1 1 0 0 0-1 .981l.006.139C1.72 3.042 1.95 3 2.19 3h5.396l-.707-.707z");
			break;
		case "folder-open":
			m_w.write("M1 3.5A1.5 1.5 0 0 1 2.5 2h2.764c.958 0 1.76.56 2.311 1.184C7.985 3.648 8.48 4 9 4h4.5A1.5 1.5 0 0 1 15 5.5v.64c.57.265.94.876.856 1.546l-.64 5.124A2.5 2.5 0 0 1 12.733 15H3.266a2.5 2.5 0 0 1-2.481-2.19l-.64-5.124A1.5 1.5 0 0 1 1 6.14V3.5zM2 6h12v-.5a.5.5 0 0 0-.5-.5H9c-.964 0-1.71-.629-2.174-1.154C6.374 3.334 5.82 3 5.264 3H2.5a.5.5 0 0 0-.5.5V6zm-.367 1a.5.5 0 0 0-.496.562l.64 5.124A1.5 1.5 0 0 0 3.266 14h9.468a1.5 1.5 0 0 0 1.489-1.314l.64-5.124A.5.5 0 0 0 14.367 7H1.633z");
			break;
		case "file-ruled":
			m_w.write("M2 2a2 2 0 0 1 2-2h8a2 2 0 0 1 2 2v12a2 2 0 0 1-2 2H4a2 2 0 0 1-2-2V2zm2-1a1 1 0 0 0-1 1v4h10V2a1 1 0 0 0-1-1H4zm9 6H6v2h7V7zm0 3H6v2h7v-2zm0 3H6v2h6a1 1 0 0 0 1-1v-1zm-8 2v-2H3v1a1 1 0 0 0 1 1h1zm-2-3h2v-2H3v2zm0-3h2V7H3v2z");
			break;
		case "gear":
			m_w.write("M8 4.754a3.246 3.246 0 1 0 0 6.492 3.246 3.246 0 0 0 0-6.492M5.754 8a2.246 2.246 0 1 1 4.492 0 2.246 2.246 0 0 1-4.492 0\"/><path d=\"M9.796 1.343c-.527-1.79-3.065-1.79-3.592 0l-.094.319a.873.873 0 0 1-1.255.52l-.292-.16c-1.64-.892-3.433.902-2.54 2.541l.159.292a.873.873 0 0 1-.52 1.255l-.319.094c-1.79.527-1.79 3.065 0 3.592l.319.094a.873.873 0 0 1 .52 1.255l-.16.292c-.892 1.64.901 3.434 2.541 2.54l.292-.159a.873.873 0 0 1 1.255.52l.094.319c.527 1.79 3.065 1.79 3.592 0l.094-.319a.873.873 0 0 1 1.255-.52l.292.16c1.64.893 3.434-.902 2.54-2.541l-.159-.292a.873.873 0 0 1 .52-1.255l.319-.094c1.79-.527 1.79-3.065 0-3.592l-.319-.094a.873.873 0 0 1-.52-1.255l.16-.292c.893-1.64-.902-3.433-2.541-2.54l-.292.159a.873.873 0 0 1-1.255-.52zm-2.633.283c.246-.835 1.428-.835 1.674 0l.094.319a1.873 1.873 0 0 0 2.693 1.115l.291-.16c.764-.415 1.6.42 1.184 1.185l-.159.292a1.873 1.873 0 0 0 1.116 2.692l.318.094c.835.246.835 1.428 0 1.674l-.319.094a1.873 1.873 0 0 0-1.115 2.693l.16.291c.415.764-.42 1.6-1.185 1.184l-.291-.159a1.873 1.873 0 0 0-2.693 1.116l-.094.318c-.246.835-1.428.835-1.674 0l-.094-.319a1.873 1.873 0 0 0-2.692-1.115l-.292.16c-.764.415-1.6-.42-1.184-1.185l.159-.291A1.873 1.873 0 0 0 1.945 8.93l-.319-.094c-.835-.246-.835-1.428 0-1.674l.319-.094A1.873 1.873 0 0 0 3.06 4.377l-.16-.292c-.415-.764.42-1.6 1.185-1.184l.292.159a1.873 1.873 0 0 0 2.692-1.115z");
			break;
		case "house":
			m_w.write("M2 13.5V7h1v6.5a.5.5 0 0 0 .5.5h9a.5.5 0 0 0 .5-.5V7h1v6.5a1.5 1.5 0 0 1-1.5 1.5h-9A1.5 1.5 0 0 1 2 13.5zm11-11V6l-2-2V2.5a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5z\"/><path fill-rule=\"evenodd\" d=\"M7.293 1.5a1 1 0 0 1 1.414 0l6.647 6.646a.5.5 0 0 1-.708.708L8 2.207 1.354 8.854a.5.5 0 1 1-.708-.708L7.293 1.5z");
			break;
		case "journal-text":
			m_w.write("M5 10.5a.5.5 0 0 1 .5-.5h2a.5.5 0 0 1 0 1h-2a.5.5 0 0 1-.5-.5zm0-2a.5.5 0 0 1 .5-.5h5a.5.5 0 0 1 0 1h-5a.5.5 0 0 1-.5-.5zm0-2a.5.5 0 0 1 .5-.5h5a.5.5 0 0 1 0 1h-5a.5.5 0 0 1-.5-.5zm0-2a.5.5 0 0 1 .5-.5h5a.5.5 0 0 1 0 1h-5a.5.5 0 0 1-.5-.5z\"/><path d=\"M3 0h10a2 2 0 0 1 2 2v12a2 2 0 0 1-2 2H3a2 2 0 0 1-2-2v-1h1v1a1 1 0 0 0 1 1h10a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1H3a1 1 0 0 0-1 1v1H1V2a2 2 0 0 1 2-2z\"/><path d=\"M1 5v-.5a.5.5 0 0 1 1 0V5h.5a.5.5 0 0 1 0 1h-2a.5.5 0 0 1 0-1H1zm0 3v-.5a.5.5 0 0 1 1 0V8h.5a.5.5 0 0 1 0 1h-2a.5.5 0 0 1 0-1H1zm0 3v-.5a.5.5 0 0 1 1 0v.5h.5a.5.5 0 0 1 0 1h-2a.5.5 0 0 1 0-1H1z");
			break;
		case "layout-three-column":
			m_w.write("M0 1.5A1.5 1.5 0 0 1 1.5 0h13A1.5 1.5 0 0 1 16 1.5v13a1.5 1.5 0 0 1-1.5 1.5h-13A1.5 1.5 0 0 1 0 14.5v-13zM1.5 1a.5.5 0 0 0-.5.5v13a.5.5 0 0 0 .5.5H5V1H1.5zM10 15V1H6v14h4zm1 0h3.5a.5.5 0 0 0 .5-.5v-13a.5.5 0 0 0-.5-.5H11v14z");
			break;
		case "lock":
			m_w.write("M11.5 8h-7a1 1 0 0 0-1 1v5a1 1 0 0 0 1 1h7a1 1 0 0 0 1-1V9a1 1 0 0 0-1-1zm-7-1a2 2 0 0 0-2 2v5a2 2 0 0 0 2 2h7a2 2 0 0 0 2-2V9a2 2 0 0 0-2-2h-7zm0-3a3.5 3.5 0 1 1 7 0v3h-1V4a2.5 2.5 0 0 0-5 0v3h-1V4z");
			break;
		case "newspaper":
			m_w.write("M0 2.5A1.5 1.5 0 0 1 1.5 1h11A1.5 1.5 0 0 1 14 2.5v10.528c0 .3-.05.654-.238.972h.738a.5.5 0 0 0 .5-.5v-9a.5.5 0 0 1 1 0v9a1.5 1.5 0 0 1-1.5 1.5H1.497A1.497 1.497 0 0 1 0 13.5v-11zM12 14c.37 0 .654-.211.853-.441.092-.106.147-.279.147-.531V2.5a.5.5 0 0 0-.5-.5h-11a.5.5 0 0 0-.5.5v11c0 .278.223.5.497.5H12z\"/><path d=\"M2 3h10v2H2V3zm0 3h4v3H2V6zm0 4h4v1H2v-1zm0 2h4v1H2v-1zm5-6h2v1H7V6zm3 0h2v1h-2V6zM7 8h2v1H7V8zm3 0h2v1h-2V8zm-3 2h2v1H7v-1zm3 0h2v1h-2v-1zm-3 2h2v1H7v-1zm3 0h2v1h-2v-1z");
			break;
		case "pencil":
			m_w.write("M12.146.146a.5.5 0 0 1 .708 0l3 3a.5.5 0 0 1 0 .708l-10 10a.5.5 0 0 1-.168.11l-5 2a.5.5 0 0 1-.65-.65l2-5a.5.5 0 0 1 .11-.168l10-10zM11.207 2.5L13.5 4.793 14.793 3.5 12.5 1.207 11.207 2.5zm1.586 3L10.5 3.207 4 9.707V10h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.293l6.5-6.5zm-9.761 5.175l-.106.106-1.528 3.821 3.821-1.528.106-.106A.5.5 0 0 1 5 12.5V12h-.5a.5.5 0 0 1-.5-.5V11h-.5a.5.5 0 0 1-.468-.325z");
			break;
		case "people":
			m_w.write("M15 14s1 0 1-1-1-4-5-4-5 3-5 4 1 1 1 1h8zm-7.978-1A.261.261 0 0 1 7 12.996c.001-.264.167-1.03.76-1.72C8.312 10.629 9.282 10 11 10c1.717 0 2.687.63 3.24 1.276.593.69.758 1.457.76 1.72l-.008.002a.274.274 0 0 1-.014.002H7.022zM11 7a2 2 0 1 0 0-4 2 2 0 0 0 0 4zm3-2a3 3 0 1 1-6 0 3 3 0 0 1 6 0zM6.936 9.28a5.88 5.88 0 0 0-1.23-.247A7.35 7.35 0 0 0 5 9c-4 0-5 3-5 4 0 .667.333 1 1 1h4.216A2.238 2.238 0 0 1 5 13c0-1.01.377-2.042 1.09-2.904.243-.294.526-.569.846-.816zM4.92 10A5.493 5.493 0 0 0 4 13H1c0-.26.164-1.03.76-1.724.545-.636 1.492-1.256 3.16-1.275zM1.5 5.5a3 3 0 1 1 6 0 3 3 0 0 1-6 0zm3-2a2 2 0 1 0 0 4 2 2 0 0 0 0-4z");
			break;
		case "person":
			m_w.write("M10 5a2 2 0 1 1-4 0 2 2 0 0 1 4 0zM8 8a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm6 5c0 1-1 1-1 1H3s-1 0-1-1 1-4 6-4 6 3 6 4zm-1-.004c-.001-.246-.154-.986-.832-1.664C11.516 10.68 10.289 10 8 10c-2.29 0-3.516.68-4.168 1.332-.678.678-.83 1.418-.832 1.664h10z");
			break;
		case "phone":
			m_w.write("M11 1H5a1 1 0 0 0-1 1v12a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1zM5 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h6a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H5z\"/><path fill-rule=\"evenodd\" d=\"M8 14a1 1 0 1 0 0-2 1 1 0 0 0 0 2z");
			break;
		case "plus":
			m_w.write("M8 4a.5.5 0 0 1 .5.5v3h3a.5.5 0 0 1 0 1h-3v3a.5.5 0 0 1-1 0v-3h-3a.5.5 0 0 1 0-1h3v-3A.5.5 0 0 1 8 4");
			break;
		case "printer":
			m_w.write("M2.5 8a.5.5 0 1 0 0-1 .5.5 0 0 0 0 1z\"/><path d=\"M5 1a2 2 0 0 0-2 2v2H2a2 2 0 0 0-2 2v3a2 2 0 0 0 2 2h1v1a2 2 0 0 0 2 2h6a2 2 0 0 0 2-2v-1h1a2 2 0 0 0 2-2V7a2 2 0 0 0-2-2h-1V3a2 2 0 0 0-2-2H5zM4 3a1 1 0 0 1 1-1h6a1 1 0 0 1 1 1v2H4V3zm1 5a2 2 0 0 0-2 2v1H2a1 1 0 0 1-1-1V7a1 1 0 0 1 1-1h12a1 1 0 0 1 1 1v3a1 1 0 0 1-1 1h-1v-1a2 2 0 0 0-2-2H5zm7 2v3a1 1 0 0 1-1 1H5a1 1 0 0 1-1-1v-3a1 1 0 0 1 1-1h6a1 1 0 0 1 1 1z");
			break;
		case "search":
			m_w.write("M10.442 10.442a1 1 0 0 1 1.415 0l3.85 3.85a1 1 0 0 1-1.414 1.415l-3.85-3.85a1 1 0 0 1 0-1.415z\"/><path fill-rule=\"evenodd\" d=\"M6.5 12a5.5 5.5 0 1 0 0-11 5.5 5.5 0 0 0 0 11zM13 6.5a6.5 6.5 0 1 1-13 0 6.5 6.5 0 0 1 13 0z");
			break;
		case "skip-backward":
			m_w.write("M5.696 8L11.5 4.633v6.734L5.696 8zm-.792-.696a.802.802 0 0 0 0 1.392l6.363 3.692c.52.302 1.233-.043 1.233-.696V4.308c0-.653-.713-.998-1.233-.696L4.904 7.304z");
			break;
		case "skip-end":
			m_w.write("M12 3.5a.5.5 0 0 1 .5.5v8a.5.5 0 0 1-1 0V4a.5.5 0 0 1 .5-.5z\"/><path fill-rule=\"evenodd\" d=\"M10.804 8L5 4.633v6.734L10.804 8zm.792-.696a.802.802 0 0 1 0 1.392l-6.363 3.692C4.713 12.69 4 12.345 4 11.692V4.308c0-.653.713-.998 1.233-.696l6.363 3.692z");
			break;
		case "skip-forward":
			m_w.write("M10.804 8L5 4.633v6.734L10.804 8zm.792-.696a.802.802 0 0 1 0 1.392l-6.363 3.692C4.713 12.69 4 12.345 4 11.692V4.308c0-.653.713-.998 1.233-.696l6.363 3.692z");
			break;
		case "skip-start":
			m_w.write("M4.5 3.5A.5.5 0 0 0 4 4v8a.5.5 0 0 0 1 0V4a.5.5 0 0 0-.5-.5z\"/><path fill-rule=\"evenodd\" d=\"M5.696 8L11.5 4.633v6.734L5.696 8zm-.792-.696a.802.802 0 0 0 0 1.392l6.363 3.692c.52.302 1.233-.043 1.233-.696V4.308c0-.653-.713-.998-1.233-.696L4.904 7.304z");
			break;
		case "three-dots":
			m_w.write("M3 9.5a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3zm5 0a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3zm5 0a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3z");
			break;
		}
		m_w.write("\"/>").tagClose();
		return m_w;
	}

	//--------------------------------------------------------------------------

	public HTMLWriter
	iconText(String icon, String text) {
		m_w.write("<div class=\"mb-3\" style=\"align-items:center;display:flex;max-width:1024px\">")
			.ui.icon(icon)
			.addStyle("margin-left:10px")
			.tag("span", text)
			.write("</div>");
		return m_w;
	}

	//--------------------------------------------------------------------------

	public HTMLWriter
	iconToggle(String s, boolean open) {
		m_w.addStyle("text-decoration:none !important").aOnClickOpen("_.table.toggle_divider(this)");
		m_w.write(s).space();
		if (open)
			m_w.addStyle("transform:rotate(90deg)");
		icon("chevron-right");
		return m_w;
	}

	//--------------------------------------------------------------------------

	public ui.InlineForm
	inlineForm(String action) {
		return new InlineForm(action, m_w);
	}

	//--------------------------------------------------------------------------

	public InputGroup
	inputGroup() {
		return new InputGroup(m_w);
	}

	//--------------------------------------------------------------------------

	public HTMLWriter
	monthSelect(int selected_month) {
		selectOpen(null, true);
		for (int i=1; i<=12; i++) {
			m_w.write("<option");
			if (i == selected_month)
				m_w.write(" selected=\"selected\"");
			m_w.write(" value=\"").write(i).write("\">").write(s_months[i - 1]).write("</option>");
		}
		m_w.tagClose();
		return m_w;
	}

	//--------------------------------------------------------------------------

	public ui.NavBar
	navBar(String id, String target, int segment, Request r) {
		return new NavBar(id, target, segment, r.w);
	}

	//--------------------------------------------------------------------------

	public ui.NavList
	navList() {
		return new NavList(m_w);
	}

	//--------------------------------------------------------------------------

	public HTMLWriter
	radioButtonInline(String name, String id, String label, String value, boolean checked, String on_click) {
		if (id == null)
			id = Text.uuid();
		m_w.write("<span class=\"form-check form-check-inline\">")
			.addClass("form-check-input")
			.setAttribute("type", "radio")
			.setAttribute("name", name)
			.setId(id);
		if (value != null)
			m_w.setAttribute("value", value);
		if (checked)
			m_w.setAttribute("checked", null);
		if (on_click != null)
			m_w.setAttribute("onclick", on_click);
		m_w.tag("input")
			.addClass("form-check-label")
			.setAttribute("for", id)
			.tag("label", label)
			.write("</span>");
		return m_w;
	}

	//--------------------------------------------------------------------------
	/**
	 * writes the HTML code for a group of radio buttons.
	 * @param name the name of each radio input
	 * @param labels an array of labels, one for each radio button
	 * @param values an array of values, one for each radio button
	 * @param selected_value equal to the value for which button should be selected, may be null
	 * @throws IOException
	 */

	public HTMLWriter
	radioButtons(String name, String[] labels, String[] values, String selected_value) {
		for (int i=0; i<labels.length; i++)
			radioButtonInline(name, name + i, labels[i], values == null ? null : values[i], 0 == i && null == selected_value || values != null && values[i].equals(selected_value), null);
		return m_w;
	}

	//--------------------------------------------------------------------------

	public HTMLWriter
	select(String name, String[] options, String selected_option, String first_option, boolean inline) {
		selectOpen(name, inline);
		if (first_option != null)
			m_w.tag("option", first_option);
		if (options != null)
			for (String option : options) {
				if (selected_option != null && option.equals(selected_option))
					m_w.setAttribute("selected", null);
				m_w.tag("option", option);
			}
		m_w.tagClose();
		return m_w;
	}

	//--------------------------------------------------------------------------

	public HTMLWriter
	select(String name, List<String> options, String selected_option, String first_option, boolean inline) {
		selectOpen(name, inline);
		if (first_option != null)
			m_w.tag("option", first_option);
		if (options != null)
			for (String option : options) {
				if (selected_option != null && option.equals(selected_option))
					m_w.setAttribute("selected", null);
				m_w.tag("option", option);
			}
		m_w.tagClose();
		return m_w;
	}

	//--------------------------------------------------------------------------

	public HTMLWriter
	selectOpen(String name, boolean inline) {
		if (name != null)
			m_w.setAttribute("name", name);
		if (inline)
			m_w.addStyle("display:inline-block;width:initial");
		m_w.addClass("form-select")
			.tagOpen("select");
		return m_w;
	}

	//--------------------------------------------------------------------------

	public HTMLWriter
	subHead(String text) {
		return m_w.addClass("form-text").p(text);
	}

	//--------------------------------------------------------------------------

	public HTMLWriter
	submitButton(String text) {
		m_w.setAttribute("type", "submit").addClass("btn btn-secondary");
		return m_w.tag("button", text);
	}

	//--------------------------------------------------------------------------

	public ui.Table
	table() {
		return new Table(m_w);
	}

	//--------------------------------------------------------------------------

	public ui.Tabs
	tabs(String id) {
		return new Tabs(id, m_w);
	}

	//--------------------------------------------------------------------------

	public ui.Td
	td() {
		return new Td(m_w);
	}

	//--------------------------------------------------------------------------

	public HTMLWriter
	textInput(String name, int size, String value) {
		m_w.setAttribute("type", "text");
		if (name != null)
			m_w.setAttribute("name", name);
		if (size > 0) {
			m_w.setAttribute("size", size);
			m_w.setAttribute("maxlength", size);
		}
		if (value != null)
			m_w.setAttribute("value", value);
		m_w.setAttribute("autocomplete", "off")
			.addClass("form-control")
			.tag("input");
		return m_w;
	}

	// --------------------------------------------------------------------------

	public HTMLWriter
	yearSelect(int earliest_year, int latest_year, int selected_year, boolean all_option) {
		if (selected_year != 0)
			selected_year = Math.max(Math.min(selected_year, latest_year), earliest_year);
		selectOpen(null, true);
		if (all_option) {
			m_w.write("<option value=\"all\"");
			if (selected_year == 0)
				m_w.write(" selected=\"selected\"");
			m_w.write(">All</option>");
		}
		for (int i=earliest_year; i<=latest_year; i++) {
			m_w.write("<option");
			if (i == selected_year)
				m_w.write(" selected=\"selected\"");
			m_w.write(">").write(i).write("</option>");
		}
		m_w.tagClose();
		return m_w;
	}
}
