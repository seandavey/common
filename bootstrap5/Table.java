package bootstrap5;

import ui.Td;
import web.HTMLWriter;

class Table extends TagBase<Table> implements ui.Table {
//	private int					m_cell_padding = -1;
//	private int					m_column_num;
//	private Td					m_label_td;
	private int					m_num_columns;
	private int					m_tbody_mark = -1;
	private int					m_td_mark = -1;
	private int					m_thead_mark = -1;
	private int					m_tr_mark = -1;

	//--------------------------------------------------------------------------

	public
	Table(HTMLWriter w) {
		super("table", w);
	}

	//--------------------------------------------------------------------------

	@Override
	public Table
	addDefaultClasses() {
		addClass("table table-sm table-hover");
		return this;
	}

	//--------------------------------------------------------------------------

	@Override
	public Table
	borderSpacing(int border_spacing) {
		addStyle("border-spacing", border_spacing);
		if (border_spacing > 0)
			addStyle("border-collapse", "separate");
		return this;
	}

	//--------------------------------------------------------------------------

//	@Override
//	public Table
//	cellPadding(int cell_padding) {
//		m_cell_padding = cell_padding;
//		return this;
//	}

	//--------------------------------------------------------------------------

	@Override
	public void
	close() {
		if (m_mark != -1) {
			trClose();
			m_w.tagsCloseTo(m_mark);
			m_mark = -1;
		}
	}

	//--------------------------------------------------------------------------

	@Override
	public Table
	heads(String... heads) {
		for (String head : heads)
			th(head);
		return this;
	}

	//--------------------------------------------------------------------------

	@Override
	public int
	numColumns() {
		return m_num_columns;
	}

	//--------------------------------------------------------------------------

	@Override
	public Table
	open() {
		close();
		return super.open();
	}

	// --------------------------------------------------------------------------

//	@Override
//	public Table
//	rowOpen(String label) {
//		tr().td(m_label_td);
//		m_w.write(label);
//		td();
//		return this;
//	}

	//--------------------------------------------------------------------------

//	@Override
//	public Table
//	setLabelTd(Td label_td) {
//		m_label_td = label_td;
//		return this;
//	}

	//--------------------------------------------------------------------------

	private Table
	_td(String tag) {
		if (m_tr_mark == -1)
			tr();
		else
			tdClose();
//		if (m_cell_padding != -1)
//			m_w.addStyle("padding:" + m_cell_padding + "px");
		m_td_mark = m_w.tagOpen(tag);
//		++m_column_num;
		return this;
	}

	//--------------------------------------------------------------------------

	@Override
	public Table
	td() {
		return _td("td");
	}

	//--------------------------------------------------------------------------

	@Override
	public Table
	td(Td td) {
		if (m_tr_mark == -1)
			tr();
		else
			tdClose();
		m_td_mark = td.open().getMark();
//		++m_column_num;
		return this;
	}

	//--------------------------------------------------------------------------

	@Override
	public Table
	td(int colspan, String text) {
		m_w.setAttribute("colspan", colspan);
		td(text);
//		m_column_num += colspan - 1;
		return this;
	}

	//--------------------------------------------------------------------------

	@Override
	public Table
	td(String text) {
		_td("td");
		if (text != null)
			m_w.write(text);
		return this;
	}

	//--------------------------------------------------------------------------

//	@Override
//	public Table
//	tdCenter() {
//		m_w.addStyle("text-align:center");
//		_td("td");
//		return this;
//	}

	//--------------------------------------------------------------------------

	private Table
	tdClose() {
		if (m_td_mark != -1) {
			m_w.tagsCloseTo(m_td_mark);
			m_td_mark = -1;
		}
		return this;
	}

	//--------------------------------------------------------------------------

	@Override
	public Table
	tdCurrency(double d) {
		m_w.addStyle("text-align:right");
		_td("td");
		m_w.writeCurrency(d);
		return this;
	}

	//--------------------------------------------------------------------------

	@Override
	public Table
	tdRight() {
		m_w.addStyle("text-align:right");
		_td("td");
		return this;
	}

	//--------------------------------------------------------------------------

	@Override
	public Table
	tfoot() {
		if (m_tbody_mark != -1) {
			m_w.tagsCloseTo(m_tbody_mark);
			m_tbody_mark = -1;
			m_tr_mark = -1;
		}
		m_w.tagOpen("tfoot");
		return this;
	}

	//--------------------------------------------------------------------------

	@Override
	public Table
	th() {
		if (m_tr_mark == -1) {
			thead();
			tr();
		}
		_td("th");
		m_num_columns++;
		return this;
	}

	//--------------------------------------------------------------------------

	@Override
	public Table
	th(String text) {
		th();
		m_w.write(text);
		return this;
	}

	//--------------------------------------------------------------------------

	@Override
	public Table
	thCenter() {
		if (m_tr_mark == -1) {
			thead();
			tr();
		}
		m_w.addStyle("text-align:center");
		_td("th");
		m_num_columns++;
		return this;
	}

	//--------------------------------------------------------------------------

	@Override
	public Table
	thCenter(String text) {
		thCenter();
		m_w.write(text);
		return this;
	}

	//--------------------------------------------------------------------------

	@Override
	public Table
	thead() {
		if (m_mark == -1)
			open();
		m_thead_mark = m_w.addClass("table-primary").tagOpen("thead");
		return this;
	}

	//--------------------------------------------------------------------------

	@Override
	public Table
	tr() {
		if (m_mark == -1)
			open();
		else
			trClose();
		m_tr_mark = m_w.tagOpen("tr");
//		m_column_num = 0;
		return this;
	}

	//--------------------------------------------------------------------------

	private Table
	trClose() {
		if (m_tr_mark == -1)
			return this;
		tdClose();
		m_w.tagsCloseTo(m_tr_mark);
		m_tr_mark = -1;
		if (m_thead_mark != -1) {
			m_w.tagsCloseTo(m_thead_mark);
			m_thead_mark = -1;
			m_tbody_mark = m_w.tagOpen("tbody");
		}
		return this;
	}
}
