package bootstrap5;

import web.HTMLWriter;

class Button extends TagBase<Button> implements ui.Button {
	private String	m_icon;

	//--------------------------------------------------------------------------

	public
	Button(String text, HTMLWriter w) {
		super("button", w);
		m_text = text;
		addAttribute("type", "button");
	}

	//--------------------------------------------------------------------------

	@Override
	public Button
	setIcon(String icon) {
		m_icon = icon;
		return this;
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	write() {
		addClass("btn btn-secondary btn-sm");
		open();
		if (m_icon != null)
			m_w.ui.icon(m_icon);
		if (m_text != null)
			m_w.write(m_text);
		close();
	}
}
