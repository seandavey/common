package bootstrap5;

import web.HTMLWriter;

class Dropdown extends TagBase<Dropdown> implements ui.Dropdown {
	private final String	m_button_text;
	private String			m_div_class = "dropdown";
	private boolean			m_dropdown_menu_right;
	private final boolean	m_has_button;
	private boolean			m_in_nav_bar;
	private final boolean	m_small;

	//--------------------------------------------------------------------------

	public
	Dropdown(HTMLWriter w) {
		super("ul", w);
		addStyle("white-space", "nowrap");
		m_button_text = null;
		m_has_button = false;
		m_small = false;
	}

	//--------------------------------------------------------------------------

	public
	Dropdown(String button_text, boolean small, HTMLWriter w) {
		super("ul", w);
		m_button_text = button_text;
		m_small = small;
		m_has_button = true;
	}

	//--------------------------------------------------------------------------

	@Override
	public Dropdown
	a(String text, String href, boolean new_tab) {
		if (m_mark == -1)
			open();
		m_w.tagOpen("li");
		if (new_tab)
			m_w.aBlank(text, href);
		else
			m_w.addClass("dropdown-item")
				.setAttribute("data-text", text)
				.a(text, href);
		m_w.tagClose();
		return this;
	}

	//--------------------------------------------------------------------------

	@Override
	public Dropdown
	aOnClick(String text, String on_click) {
		if (m_mark == -1)
			open();
		m_w.tagOpen("li");
		m_w.addClass("dropdown-item")
			.setAttribute("data-text", text)
			.aOnClick(text, on_click)
			.tagClose();
		return this;
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	close() {
		super.close();
		if (m_has_button)
			m_w.tagClose();
	}

	//--------------------------------------------------------------------------

	@Override
	public Dropdown
	divider() {
		if (m_mark == -1)
			open();
		m_w.write("<li><hr class=\"dropdown-divider\"></li>");
		return this;
	}

	//--------------------------------------------------------------------------

	@Override
	public final String
	getMenuJS() {
		return "app.dropdown(this.querySelector('.dropdown-menu'))";
	}

	//--------------------------------------------------------------------------

	@Override
	public Dropdown
	open() {
		if (m_has_button) {
			if (m_div_class != null)
				m_w.addClass(m_div_class);
			m_w.tagOpen("div");
			m_w.addClass(m_small ? "btn btn-secondary dropdown-toggle btn-sm" : m_in_nav_bar ? "nav-link dropdown-toggle" : "btn btn-secondary dropdown-toggle")
				.aOnClick(m_button_text, "var e=this.nextSibling;_.dom.set_styles(e,{display:window.getComputedStyle(e).display==='none'?'block':'none',textAlign:'left'});app.p=e;");
		}
		addClass("dropdown-menu");
		if (m_dropdown_menu_right)
			addStyle("right", "0");
		return super.open();
	}

	//--------------------------------------------------------------------------

	@Override
	public Dropdown
	setDivClass(String div_class) {
		m_div_class = div_class;
		return this;
	}

	//--------------------------------------------------------------------------

	@Override
	public Dropdown
	setDropdownMenuRight(boolean dropdown_menu_right) {
		m_dropdown_menu_right = dropdown_menu_right;
		return this;
	}

	//--------------------------------------------------------------------------

	@Override
	public Dropdown
	setInNavBar(boolean in_nav_bar) {
		m_in_nav_bar = in_nav_bar;
		return this;
	}
}
