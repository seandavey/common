package ui;

public interface NavList extends Nav<NavList> {

	void heading(String section_head);

	NavList setPills(boolean pills);

	NavList setStacked(boolean stacked);

}