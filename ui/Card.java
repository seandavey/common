package ui;

public interface Card extends TagBase<Card> {

	Card bodyClose();

	Card bodyOpen();

	Card footer(String text);

	Card header(String text);

	Card headerClose();

	Card headerOpen();

	Card subtitle(String subtitle);

	Card text(String text);

	Card textOpen();

	Card title(String title);

}