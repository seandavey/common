package ui;

import java.util.Collection;

import web.HTMLWriter;

public class Tags {
	private boolean						m_allow_dragging;
	private String[]					m_choices;
	private String[]					m_items;
	private String						m_mode;
	private String						m_name;
	private String						m_on_add;
	private String						m_on_edit_updated;
	private Collection<SelectOption>	m_options;
	private boolean						m_restrict_to_choices;

	//--------------------------------------------------------------------------

	public
	Tags(String name, String[] items, String[] choices) {
		m_name = name;
		m_items = items;
		m_choices = choices;
	}

	//--------------------------------------------------------------------------

	public
	Tags(String name, String[] items, Collection<SelectOption> options) {
		m_name = name;
		m_items = items;
		m_options = options;
	}

	//--------------------------------------------------------------------------

	public Tags
	setAllowDragging(boolean allow_dragging) {
		m_allow_dragging = allow_dragging;
		return this;
	}

	//--------------------------------------------------------------------------

	public Tags
	setMode(String mode) {
		m_mode = mode;
		return this;
	}

	//--------------------------------------------------------------------------

	public Tags
	setOnAdd(String on_add) {
		m_on_add = on_add;
		return this;
	}

	//--------------------------------------------------------------------------

	public Tags
	setOnEditUpdated(String on_edit_updated) {
		m_on_edit_updated = on_edit_updated;
		return this;
	}

	//--------------------------------------------------------------------------

	public Tags
	setRestrictToChoices(boolean restrict_to_choices) {
		m_restrict_to_choices = restrict_to_choices;
		return this;
	}

	//--------------------------------------------------------------------------

	public final void
	write(HTMLWriter w) {
		w.addStyle("display:none").textArea(m_name, null);
		w.write("<style>.tagify__dropdown{text-wrap:nowrap}</style><script>JS.get('tags',function(){_.$('#")
			.write(m_name)
			.write("').tags = new Tagify(_.$('#")
			.write(m_name)
			.write("'),{delimiters:null,dropdown:{enabled:0,fuzzySearch:true,maxItems:1000}");
		if (m_choices != null && m_choices.length > 0) {
			w.write(",whitelist:").jsArray(m_choices);
			if (m_restrict_to_choices)
				w.write(",enforceWhitelist:true");
		} else if (m_options != null && m_options.size() > 0) {
			w.write(",whitelist:[");
			boolean first = true;
			for (SelectOption o : m_options) {
				if (first)
					first = false;
				else
					w.comma();
				w.write("{value:").jsString(o.getText()).write(",id:").jsString(o.getValue()).write("}");
			}
			w.write("]");
			if (m_restrict_to_choices)
				w.write(",enforceWhitelist:true");
		}
		if (m_mode != null)
			w.write(",mode:").jsString(m_mode);
		w.write("});");
		if (m_on_add != null)
			w.write("_.$('#").write(m_name).write("').tags.on('add', ").write(m_on_add).write(");");
		if (m_on_edit_updated != null && m_on_edit_updated.length() > 0)
			w.write("_.$('#").write(m_name).write("').tags.on('edit:beforeUpdate',").write(m_on_edit_updated).write(");");
		w.write("_.$('#").write(m_name).write("').style.display='none';");
		if (m_items != null) {
			w.write("_.$('#").write(m_name).write("').tags.addTags([");
			for (int i=0; i<m_items.length; i++) {
				if (i > 0)
					w.comma();
				w.jsString(m_items[i]);
			}
			w.write("]);");
		}
		if (m_allow_dragging)
			w.write("new DragSort(_.$('#")
				.write(m_name)
				.write("').tags.DOM.scope,{selector:'.'+_.$('#")
				.write(m_name)
				.write("').tags.settings.classNames.tag,callbacks:{dragEnd:function(){_.$('#")
				.write(m_name)
				.write("').tags.updateValueByDOMTags()}}});");
		w.write("})</script>");
	}
}
